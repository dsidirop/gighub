# Built With

![Rider](https://bitbucket.org/dsidirop/gighub/raw/3053c14bf8cf41ce78cec197828b66353422c914/Collateral/Badges/rider.png)
![Resharper](https://bitbucket.org/dsidirop/gighub/raw/3053c14bf8cf41ce78cec197828b66353422c914/Collateral/Badges/resharper.png)

![NodeJs](https://bitbucket.org/dsidirop/gighub/raw/3053c14bf8cf41ce78cec197828b66353422c914/Collateral/Badges/nodejs.png)
![Sass](https://bitbucket.org/dsidirop/gighub/raw/3053c14bf8cf41ce78cec197828b66353422c914/Collateral/Badges/sass.png)
![ES6](https://bitbucket.org/dsidirop/gighub/raw/3053c14bf8cf41ce78cec197828b66353422c914/Collateral/Badges/es6.png)

![ASPNET](https://bitbucket.org/dsidirop/gighub/raw/3a22265aecb7a0af8cd2c58f7ee445209cb15ec2/Collateral/Badges/AspNet.png)
![Bootstrap](https://bitbucket.org/dsidirop/gighub/raw/3a22265aecb7a0af8cd2c58f7ee445209cb15ec2/Collateral/Badges/Bootstrap.png)
![jQuery](https://bitbucket.org/dsidirop/gighub/raw/3a22265aecb7a0af8cd2c58f7ee445209cb15ec2/Collateral/Badges/jQuery.png)
![Gulp](https://bitbucket.org/dsidirop/gighub/raw/3053c14bf8cf41ce78cec197828b66353422c914/Collateral/Badges/gulp.png)

![RequireJS.Net](https://bitbucket.org/dsidirop/gighub/raw/3053c14bf8cf41ce78cec197828b66353422c914/Collateral/Badges/requirejsnet.png)
![Yarn](https://bitbucket.org/dsidirop/gighub/raw/1f29d7043e196b67599b0d61278b7262aaa4d6d4/Collateral/Badges/yarn.png)

# [GigHub](https://ci.appveyor.com/project/dsidirop/gighub)

[![Build status](https://ci.appveyor.com/api/projects/status/bitbucket/dsidirop/gighub?retina=true)](https://ci.appveyor.com/project/dsidirop/gighub)

[![Test status](http://teststatusbadge.azurewebsites.net/api/status/dsidirop/gighub)](https://ci.appveyor.com/project/dsidirop/gighub)

# [Jira](https://dsidirop.atlassian.net/jira/software/projects/GH/issues/)

- [Sprints](https://dsidirop.atlassian.net/jira/software/projects/GH/boards/1/backlog)
- [Roadmap](https://dsidirop.atlassian.net/jira/software/projects/GH/boards/1/roadmap)

# Intro

GigHub is a webcentric solution for managing concerts online. It supports advertising and managing gigs as well as declaring attendance to them (along with notifications about upcoming gigs and status updates.)

- Architecturally, it's a standard N-Tier solution which segregates controllers, from services, from repositories by means of design-by-contract and standard design patterns.
- It employs EF6 (in fluent mode) for DB persistence and DB migrations.
- It employs AutoMapper for mapping DTOs between layers.
- It employs NInject for dependency injection.
- It employs async/await mechanics through and through to ensure responsiveness under load.
- It enjoys a "level 2" ReST API that's fortified with proper model-validation.
- It uses standard Identity-Server authentication mechanism (cookie-based).
- There are two kinds of controllers: MVC Controllers for Razor-View rendering and WebAPI controllers for json-fueled ReST APIs.
- Package management is done by Nuget (backend) and Yarn (frontend).
- For its frontend needs it employs bootstrap along with jQuery (module-pattern + js-data-services), RequireJS (via RequireJS.NET) and Babel.
- Style wise it uses Sass.
- There is full integration with Gulp in order to facilitate auto-detection of changes made to .js and .sass files and thus proper auto-regeneration of .css and .js assets for a smooth development experience.
- The project adheres to TDD and it includes true unit-tests as well as integration and system tests (no end-to-end tests however). All tests are written using FluentAssertions for enhanced readability and maintainability.
- CI is taken care of by AppVeyor.

# Jumpstart 

Quickstart:

0. Make sure you have:

	- JetBrains Rider (or Visual Studio 2015+)
	- .Net SDK 4.8
	- Sql Server Express 2014 or above
	- The latest R# (for good measure), more so if you plan to run the unit tests

1. Open a command prompt and issue the following command:

          "C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\MSBuild\Current\Bin\amd64\MSBuild.exe"   "C:\VS\GigHub\Collateral\IDE Setup\GigHub.IDESetup.targets"                                  

1. Now open 'Code\GigHub.Core.sln' with VS or Rider
2. Go to Tools -> Options -> Nuget and DISABLE automatic package restore
4. Set the project GigHub.Core as your startup project
5. Build the solution
6. Open a command prompt within the GigHub.Core folder and type:

         ./lgulp   # in windows    or   .\lgulp   # in *nix

7. Run the solution

8. Open a browser over to:

    http://localhost:<port>

PS: By default this solution employs a local .mdf database meant for SqlServerExpress on IISExpress. If you want to host it in IIS using the same .mdf database then you will have to follow these instructions:

    https://codemegeek.com/2018/05/13/configure-iis-to-us-localdb/

# Resources

	- https://app.pluralsight.com/library/courses/full-stack-dot-net-developer/table-of-contents
    - https://app.pluralsight.com/library/courses/full-stack-dot-net-developer-fundamentals/table-of-contents
    - http://requirejsnet.veritech.io/setup.html
    - https://stackoverflow.com/a/21179031/863651