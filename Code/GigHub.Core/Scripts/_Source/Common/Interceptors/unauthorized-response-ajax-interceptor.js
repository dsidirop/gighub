﻿define(
    "unauthorized-response-ajax-interceptor",
    [
        "jquery",
        "jsonlib",
        "window",
        "document",
        "url-helper",
        "navigation-service"
    ],
    function (
        $,
        jsonlib,
        window,
        document,
        UrlHelper,
        NavigationService,
        undefined
    ) {
        "use strict";

        var helper = function() {
            var _$doc = $(document);
            var _urlHelper = new UrlHelper();
            var _navigationService = new NavigationService();

            var HTTP_UNAUTHORIZED_401 = 401;

            var ACCOUNT_LOGIN = "/Account/Login";
            var ACCOUNT_LOGIN_LOWERCASE = ACCOUNT_LOGIN.toLowerCase();

            this.start = function() {
                _$doc.on("ajaxError", $doc_ajaxError);
            };

            this.stop = function() {
                _$doc.off("ajaxError", $doc_ajaxError);
            };

            function $doc_ajaxError(event, jqXHR, settings, thrownError) {
                if (jqXHR.status !== HTTP_UNAUTHORIZED_401) {
                    return;
                }

                var currentpath = $.trim(_navigationService.getLocationPath());
                if (currentpath.toLowerCase().startsWith(ACCOUNT_LOGIN_LOWERCASE)) { //already on the login page
                    return;
                }

                var currentSearch = _navigationService.getLocationSearch();
                var returnUrlEncoded = _urlHelper.encodeURIComponent(currentpath + currentSearch);
                var loginUrlWithReturnUrl = ACCOUNT_LOGIN + "?ReturnUrl=" + returnUrlEncoded; //  /Account/Login?ReturnUrl=...

                _navigationService.goTo(loginUrlWithReturnUrl);
            }
        };

        return helper;
    }
);
