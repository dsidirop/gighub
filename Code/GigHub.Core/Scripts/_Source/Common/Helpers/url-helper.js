﻿define(
    "url-helper",
    [
        "window"
    ],
    function (
        window,
        undefined
    ) {
        "use strict";

        var helper = function() {
            this.encodeURI = function(url) {
                return window.encodeURI(url);
            };

            this.encodeURIComponent = function(uri) {
                return window.encodeURIComponent(uri);
            };
        };

        return helper;
    }
);
