define(
    "int-helper",
    [
        "lodash"
    ],
    function (
        _,
        undefined
    ) {
        "use strict";

        var intHelper = function() {

            this.parse = function(input) {
                return _.toNumber(_.trim(input));
            };

            this.parseExact = function(input) {
                input = _.trim(input);

                return /^\d+$/.test(input)
                    ? _.toNumber(input)
                    : NaN;
            };

        };

        return intHelper;
    }
);
