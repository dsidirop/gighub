﻿define("requirejs-website-options-service", ["window"], function (window, undefined) { // http://requirejsnet.veritech.io/setup.html
        "use strict";

        var service = function () {
            var _websiteOptions = null;

            (function autoinit(options) {
                var requireConfig = window["requireConfig"];
                if (!requireConfig)
                    throw new Error("[RJWOS.AI01] [BUG] window.requireConfig is nil!");

                _websiteOptions = requireConfig["websiteOptions"];
                if (!_websiteOptions)
                    throw new Error("[RJWOS.AI02] [BUG] window.requireConfig.websiteOptions is nil!");

                if (typeof(_websiteOptions.isDebugModeOn) === "undefined")
                    throw new Error("[RJWOS.AI03] [BUG] isDebugModeOn is undefined - something fishy is going on!");

                if (typeof(_websiteOptions.isAuthenticated) === "undefined")
                    throw new Error("[RJWOS.AI04] [BUG] isDebugModeOn is undefined - something fishy is going on!");
            })();

            this.isDebugModeOn = function () {
                return _websiteOptions.isDebugModeOn;
            };

            this.isAuthenticated = function () {
                return _websiteOptions.isAuthenticated;
            };
        };

        return service;
    }
);
