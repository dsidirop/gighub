﻿// http://requirejsnet.veritech.io/setup.html

define(
    "gig-create-or-edit-service",
    [
        "jquery",
        "jquery-validate",
        "bootstrap",
        "app-initializer-service",
        "requirejs-website-options-service",
        "notifications-controller",
        "gig-create-or-edit-controller"
    ],
    function (
        $,
        $validate,
        bootstrap,
        AppInitializerService,
        WebsiteOptionsService,
        NotificationsController,
        GigCreateOrEditController,
        undefined
    ) {
        "use strict";

        var gigCreateOrEditService = function () { //used by both credte.js and edit.js because they have the exact same needs in terms of initialization
            var _appInitializerService;
            var _websiteOptionsService;
            var _notificationsController;
            var _gigCreateOrEditController;

            this.Init = function () {
                _appInitializerService = new AppInitializerService();
                _websiteOptionsService = new WebsiteOptionsService();
                _notificationsController = new NotificationsController();
                _gigCreateOrEditController = new GigCreateOrEditController();

                _appInitializerService.setup(); //order

                _notificationsController.Init({isLoggedIn: _websiteOptionsService.isAuthenticated()}); //order
                _gigCreateOrEditController.Init();

                return this;
            };
        };

        return gigCreateOrEditService;
    }
);
