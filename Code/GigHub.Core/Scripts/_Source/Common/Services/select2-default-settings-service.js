﻿define(
    "select2-default-settings-service",
    [
        "jquery",
        "jquery.select2"
    ],
    function (
        $,
        $select2,
        undefined
    ) {
        "use strict";

        var service = function () {
            this.Init = function () {
                $.fn
                    .select2
                    .defaults
                    .set("theme", "bootstrap");

                return this;
            };
        };

        return service;
    }
);
