﻿define(
    "notifications-service",
    [
        "jquery",
        "jsonlib"
    ],
    function (
        $,
        jsonlib,
        undefined
    ) {
        "use strict";

        var notificationsService = function () {
            var _dudfunc = function () {
            };

            this.markNotificationsRead = function (ajaxConfig) { //data {notificationsids} done fail always
                ajaxConfig.done = ajaxConfig.done || _dudfunc;
                ajaxConfig.fail = ajaxConfig.fail || _dudfunc;
                ajaxConfig.always = ajaxConfig.always || _dudfunc;

                var extraCallbackDataSnapshot = ajaxConfig.extraCallbackData; //bestpractice
                $.ajax({
                    url: "/api/notifications/markasread",
                    data: jsonlib.stringify({ "ids": ajaxConfig.data.notificationsIds }),
                    method: "POST",
                    mimeType: "text/html", //1
                    contentType: "application/json"
                })
                    .done(function (response) { ajaxConfig.done(response, extraCallbackDataSnapshot); })
                    .fail(function (jqXHR, textStatus, errorThrown) {
                        if (jqXHR.status === 401) return; //handled globally

                        ajaxConfig.fail(extraCallbackDataSnapshot);
                    })
                    .always(function () { ajaxConfig.always(extraCallbackDataSnapshot); });
            };

            this.getNotifications = function (ajaxConfig) { //done fail always
                ajaxConfig.done = ajaxConfig.done || _dudfunc;
                ajaxConfig.fail = ajaxConfig.fail || _dudfunc;
                ajaxConfig.always = ajaxConfig.always || _dudfunc;

                var extraCallbackDataSnapshot = ajaxConfig.extraCallbackData; //bestpractice
                $.ajax({
                    url: "/api/notifications",
                    method: "GET",
                    dataType: "json",
                    contentType: "application/json"
                })
                    .done(function (response) { ajaxConfig.done(response, extraCallbackDataSnapshot); })
                    .fail(function (jqXHR, textStatus, errorThrown) {
                        if (jqXHR.status === 401) return; //handled globally

                        ajaxConfig.fail(extraCallbackDataSnapshot);
                    })
                    .always(function () { ajaxConfig.always(extraCallbackDataSnapshot); });
            };
        };

        return notificationsService;
    }
);
