﻿define(
    "artists-followings-service",
    [
        "jquery",
        "jsonlib",
    ],
    function (
        $,
        jsonlib,
        undefined
    ) {
        "use strict";

        var artistsFollowingsService = function () {
            var _dudfunc = function () {
            };

            this.toggleFollowing = function (ajaxConfig) { //data {artistid} done fail always extracallbackdata
                ajaxConfig.done = ajaxConfig.done || _dudfunc;
                ajaxConfig.fail = ajaxConfig.fail || _dudfunc;
                ajaxConfig.always = ajaxConfig.always || _dudfunc;

                var extraCallbackDataSnapshot = ajaxConfig.extraCallbackData; //bestpractice
                $.ajax({
                    url: "/api/artistsfollowings", //[togglefollowing]
                    data: jsonlib.stringify({ "ArtistId": ajaxConfig.data.artistId }),
                    method: "POST",
                    dataType: "json",
                    contentType: "application/json"
                })
                    .done(function (response) { ajaxConfig.done(response, extraCallbackDataSnapshot); })
                    .fail(function(jqXHR, textStatus, errorThrown) {
                        if (jqXHR.status === 401) return; //handled globally

                        ajaxConfig.fail(extraCallbackDataSnapshot);
                    })
                    .always(function () { ajaxConfig.always(extraCallbackDataSnapshot); });
            };
        };

        return artistsFollowingsService;
    }
);
