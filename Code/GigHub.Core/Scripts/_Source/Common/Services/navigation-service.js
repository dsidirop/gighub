﻿define(
    "navigation-service",
    [
        "jquery",
        "jquery-navigate",
        "window",
        "jsonlib",
    ],
    function (
        $,
        $jqueryNavigate,
        window,
        jsonlib,
        undefined
    ) {
        "use strict";

        var navigationService = function () {

            this.getLocation = function () {
                return window.location;
            };

            this.getLocationPath = function () {
                return window.location.pathname;
            };

            this.getLocationSearch = function () {
                return window.location.search;
            };

            this.goTo = function (url) {
                return $.navigate.goTo(url);
            };

        };

        return navigationService;
    }
);
