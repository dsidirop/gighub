﻿define(
    "attendances-service",
    [
        "jquery",
        "jsonlib",
    ],
    function (
        $,
        jsonlib,
        undefined
    ) {
        "use strict";

        var attendancesService = function () {
            var _dudfunc = function () {
            };

            this.toggleAttendance = function (ajaxConfig) { //data {gigid} done fail always extracallbackdata
                ajaxConfig.done = ajaxConfig.done || _dudfunc;
                ajaxConfig.fail = ajaxConfig.fail || _dudfunc;
                ajaxConfig.always = ajaxConfig.always || _dudfunc;

                var extraCallbackDataSnapshot = ajaxConfig.extraCallbackData; //bestpractice
                $.ajax({
                    url: "/api/attendances", //[toggleattendance]
                    data: jsonlib.stringify({ "GigId": ajaxConfig.data.gigId }),
                    method: "POST",
                    dataType: "json",
                    contentType: "application/json"
                })
                    .done(function(response) { ajaxConfig.done(response, extraCallbackDataSnapshot); })
                    .fail(function (jqXHR, textStatus, errorThrown) {
                        if (jqXHR.status === 401) return; //handled globally

                        ajaxConfig.fail(extraCallbackDataSnapshot);
                    })
                    .always(function () { ajaxConfig.always(extraCallbackDataSnapshot); });
            };
        };

        return attendancesService;
    }
);
