﻿define(
    "gigs-service",
    [
        "jquery",
        "jsonlib"
    ],
    function (
        $,
        jsonlib,
        undefined
    ) {
        "use strict";

        var gigsService = function () {
            var _dudfunc = function () {
            };

            this.setCancelStatus = function (ajaxConfig) { //data {gigid cancelnotuncancel} done fail always extracallbackdata
                ajaxConfig.done = ajaxConfig.done || _dudfunc;
                ajaxConfig.fail = ajaxConfig.fail || _dudfunc;
                ajaxConfig.always = ajaxConfig.always || _dudfunc;

                var extraCallbackDataSnapshot = ajaxConfig.extraCallbackData; //bestpractice
                $.ajax({
                    url: "/api/gigs/setcancelstatus",
                    data: jsonlib.stringify({ "GigId": ajaxConfig.data.gigId, "CancelNotUncancel": ajaxConfig.data.cancelNotUncancel }),
                    method: "POST",
                    mimeType: "text/html", //1
                    contentType: "application/json"
                })
                    .done(function (response) { ajaxConfig.done(response, extraCallbackDataSnapshot); })
                    .fail(function (jqXHR, textStatus, errorThrown) {
                        if (jqXHR.status === 401) return; //handled globally

                        ajaxConfig.fail(extraCallbackDataSnapshot);
                    })
                    .always(function () { ajaxConfig.always(extraCallbackDataSnapshot); });
            };
        };

        return gigsService;
    }
);
