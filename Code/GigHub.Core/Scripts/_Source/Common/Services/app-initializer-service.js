﻿define(
    "app-initializer-service",
    [
        "unauthorized-response-ajax-interceptor"
    ],
    function (
        UnauthorizedResponseAjaxInterceptor,
        undefined
    ) {
        "use strict";

        var appInitializerService = function() {

            var _unauthorizedResponseAjaxInterceptor = new UnauthorizedResponseAjaxInterceptor();

            this.setup = function() {
                _unauthorizedResponseAjaxInterceptor.start();
            };

        };

        return appInitializerService;
    }
);