﻿define(
    "notifications-template-service",
    [
        "jquery",
        "moment"
    ],
    function(
        $,
        moment,
        undefined
    ) {
        "use strict";

        var notificationsTemplateService = function () {

            var htmlEscaper = (function () { //miniclosure
                var dummydiv = $("<div>");
                return (function (text) {
                    return dummydiv.text(text).html();
                });
            })();

            this.iterateOverNotifications = function (data, callbackFunc) { //notificationscshtml public
                var dateFormat = "D MMM YYYY HH:mm";
                var artistOpen = "<span class='gh-notification-artist'>";
                var artistClose = "</span>";

                _.each(
                    data.notifications,
                    function (notif) {
                        var gig = notif.gig;
                        var msg = "[Internal Error - Report this]";
                        switch (notif.type) {
                            case "GigUpdated":
                                var changes = [];
                                if (notif.originalVenue) changes.push("venue to " + htmlEscaper(gig.venue) + " (moved from: " + htmlEscaper(notif.originalVenue) + ")");
                                if (notif.originalGenre) changes.push("genre to " + htmlEscaper(gig.genre.name) + " (old was: " + htmlEscaper(notif.originalGenre.name) + ")");
                                if (notif.originalDateTime) changes.push("show-date to <time>" + htmlEscaper(moment(gig.dateTime).format(dateFormat)) + "</time> (old was: <time>" + htmlEscaper(moment(notif.originalDateTime).format(dateFormat)) + "</time>)"); //todo  fixdates utc
                                msg = artistOpen + htmlEscaper(gig.artist.name) + artistClose + " updated " + changes.join(" and ");
                                break;
                            case "GigCreated":
                                msg = artistOpen + htmlEscaper(gig.artist.name) + artistClose + " is staging a new Gig at " + htmlEscaper(gig.venue);
                                break;
                            case "GigCanceled":
                                msg = artistOpen + htmlEscaper(gig.artist.name) + artistClose + " cancelled a Gig staged at " + htmlEscaper(gig.venue);
                                break;
                            case "GigUncanceled":
                                msg = artistOpen + htmlEscaper(gig.artist.name) + artistClose + " un-cancelled Gig staged at " + htmlEscaper(gig.venue);
                                break;
                        }

                        console.log(notif);

                        callbackFunc({message: msg, notificationId: notif.id});
                    }
                );
            };
        };

        return notificationsTemplateService;
});
