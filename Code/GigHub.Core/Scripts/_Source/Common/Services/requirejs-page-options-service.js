﻿define("requirejs-website-options-service", [ "window" ], function (window, undefined) { // http://requirejsnet.veritech.io/setup.html
        "use strict";

        var _requireConfig = window.requireConfig || {};
        var _pageOptions = requireConfig.pageOptions || {};

        var service = function () {

            this.get = function () {
                return _pageOptions;
            };

        };

        return service;
    }
);
