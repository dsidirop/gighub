﻿// requirejsnet.veritech.io/setup.html

require(
    [
        "requirejs-website-options-service",
        "jquery",
        "jquery-validate",
        "bootstrap",
        "app-initializer-service",
        "notifications-controller",
        "gigs-controller"
    ],
    function (
        WebsiteOptionsService,
        $,
        $validate,
        bootstrap,
        AppInitializerService,
        NotificationsController,
        GigsController,
        undefined
    ) {
        "use strict";

        var _controller;
        var _appInitializerService;
        var _websiteOptionsService;
        var _notificationsController;

        $(function () {
            _websiteOptionsService = new WebsiteOptionsService();

            _appInitializerService = new AppInitializerService();
            _appInitializerService.setup();
            
            _controller = new GigsController();
            _controller.Init();

            _notificationsController = new NotificationsController();
            _notificationsController.Init({isLoggedIn: _websiteOptionsService.isAuthenticated()});
        });
    }
);
