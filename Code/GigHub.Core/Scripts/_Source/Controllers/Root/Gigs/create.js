﻿// requirejsnet.veritech.io/setup.html

require(
    [
        "gig-create-or-edit-service", //create.js and edit.js are exactly the same   this is why we moved their init logic inside a seperate service
    ],
    function (
        GigCreateOrEditService,
        undefined
    ) {
        "use strict";

        var _gigCreateOrEditService;

        $(function () {
            _gigCreateOrEditService = new GigCreateOrEditService();

            _gigCreateOrEditService.Init();
        });
    }
);
