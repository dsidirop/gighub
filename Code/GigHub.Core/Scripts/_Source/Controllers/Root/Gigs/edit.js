﻿// requirejsnet.veritech.io/setup.html

require(
    [
        "gig-create-or-edit-service"
    ],
    function (
        GigCreateOrEditService,
        undefined
    ) {
        "use strict";

        var _gigCreateOrEditService;

        $(function () {
            _gigCreateOrEditService = new GigCreateOrEditService();

            _gigCreateOrEditService.Init();
        });
    }
);
