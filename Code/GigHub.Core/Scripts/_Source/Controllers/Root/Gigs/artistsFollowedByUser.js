﻿// requirejsnet.veritech.io/setup.html

require(
    [
        "requirejs-website-options-service",
        "jquery",
        "jquery-validate",
        "bootstrap",
        "app-initializer-service",
        "notifications-controller",
        "artists-followed-by-user-controller"
    ],
    function (
        WebsiteOptionsService,
        $,
        $validate,
        bootstrap,
        AppInitializerService,
        NotificationsController,
        ArtistsFollowedByUserController,
        undefined
    ) {
        "use strict";

        var _controller;
        var _appInitializerService;
        var _websiteOptionsService;
        var _notificationsController;

        $(function () {
            _websiteOptionsService = new WebsiteOptionsService();

            _appInitializerService = new AppInitializerService();
            _appInitializerService.setup();

            _notificationsController = new NotificationsController();
            _notificationsController.Init({isLoggedIn: _websiteOptionsService.isAuthenticated()});

            _controller = new ArtistsFollowedByUserController();
            _controller.Init();
        });
    }
);
