﻿// bare in mind that this controller is being used by both the index view of the home page and the gigsiamattending view

define(
    "gigs-controller",
    [
        "jquery",
        "document",
        "bootbox",
        "bootbox-locales",
        "attendances-service",
        "artists-followings-service"
    ],
    function (
        $,
        document,
        bootbox,
        bootboxLocales,
        attendancesService,
        artistsFollowingsService,
        undefined
    ) {
        "use strict";

        var gigsController = function () {
            var _attendancesService = null;
            var _artistsFollowingsService = null;

            this.Init = function() {
                _attendancesService = new attendancesService();
                _artistsFollowingsService = new artistsFollowingsService();

                var $doc = $(document);
                $doc.on("click", ".js-toggle-attendance", btnToggleAttendance_click);
                $doc.on("click", ".js-toggle-followartist", btnToggleFollowArtist_click);

                return this;
            };

            var btnToggleAttendance_click = function(ea) {
                var $button = $(ea.currentTarget);
                _attendancesService.toggleAttendance({
                    data: {
                        gigId: $button.attr("data-gig-id")
                    },
                    done: attendancesService_toggleAttendanceDone,
                    fail: anyServiceCall_genericFailHandler,
                    extraCallbackData: {
                        $button: $button //1
                    }
                });

                //1 to be a hundred percent on the safe side we want the donehandler to be bound to the button that initiated the event  we dont want to have the button
                //  to be placed as a class wide variable because it is conceivable that the user might click on two different buttons back to back while the first ajax
                //  call is still underway thus changing the button that was supposed to be used by the donehandler of the first ajax call  such caveats are very sneaky
            };

            var attendancesService_toggleAttendanceDone = function(response, extraCallbackData) { //1
                extraCallbackData.$button
                    .removeClass(response.going ? "btn-default" : "btn-info")
                    .addClass(response.going ? "btn-info" : "btn-default")
                    .text(response.going ? "Going !" : "Going ?");
            };

            var btnToggleFollowArtist_click = function(ea) {
                var $button = $(ea.currentTarget);
                _artistsFollowingsService.toggleFollowing({
                    data: {
                        artistId: $button.attr("data-artist-id")
                    },
                    done: artistsFollowingsService_toggleFollowingDone,
                    fail: anyServiceCall_genericFailHandler,
                    extraCallbackData: {
                        $button: $button //1
                    }
                });

                //1 to be a hundred percent on the safe side we want the donehandler to be bound to the button that initiated the event  we dont want to have the button
                //  to be placed as a class wide variable because it is conceivable that the user might click on two different buttons back to back while the first ajax
                //  call is still underway thus changing the button that was supposed to be used by the donehandler of the first ajax call  such caveats are very sneaky
            };

            var artistsFollowingsService_toggleFollowingDone = function(response, xd) { //1
                xd.$button
                    .removeClass(response.following ? "btn-default" : "btn-info")
                    .addClass(response.following ? "btn-info" : "btn-default")
                    .text(response.following ? "Following !" : "Follow ?");
            };

            var anyServiceCall_genericFailHandler = function(xd) {
                bootbox.dialog({title: "Error", message: "Operation failed!"});
            };
        };

        return gigsController;
    }
);
