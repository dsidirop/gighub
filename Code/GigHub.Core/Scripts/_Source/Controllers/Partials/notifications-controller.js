﻿define(
    "notifications-controller",
    [
        "jquery",
        "math",
        "jsonlib",
        "lodash",
        "bootbox",
        "bootbox-locales",
        "document",
        "int-helper",
        "notifications-service",
        "notifications-template-service" //used inside notifications.cshtml   so it must be loaded
    ],
    function (
        $,
        math,
        jsonlib,
        _,
        bootbox,
        bootboxLocales,
        document,
        IntHelper,
        NotificationsService,
        NotificationsTemplateService, //used inside notifications.cshtml   so it must be loaded
        undefined
    ) {
        "use strict";

        var AutoRefreshInterval = 15 * 1000;
        var NotificationsPopoverTagclass = "popover-notifications";

        var notificationsController = function () {
            var _$document = null;
            var _$notifsButton = null;
            var _$notificsBadge = null;

            var _int = null;
            var _notificationsService = null;
            var _notificationsCompiledTemplate = null;

            this.Init = function(config) {
                _int = new IntHelper();
                _notificationsService = new NotificationsService();

                _$document = $(document);
                _$notifsButton = $("#notificationsPopoverButton");
                _$notificsBadge = _$notifsButton.find(".js-unread-notifications-counter");
                _notificationsCompiledTemplate = _.template($("#tmpl-notifications").html(), { variable: "_templateConfig_" }); //Notifications.cshtml

                _$notifsButton.on("show.bs.popover", popover_shown);
                _$notifsButton.on("hidden.bs.popover", popover_hidden);

                _$document.on("click", "#notificationsPopoverButton", notificationsPopoverButton_click);
                _$document.on("mouseenter", "ul.notifications > li", anyNotificationListItem_mouseEnter);
                _$document.on("mouseleave", "ul.notifications > li", anyNotificationListItem_mouseLeave);
                _$document.on("click", "ul.notifications  li  .gh-notif-dismiss", anyNotificationListItemXbutton_click);

                if (config.isLoggedIn) {
                    refreshNotifications({ animateBadge: true, repeatOnComplete: true });
                }

                return this;
            };

            var notificationsPopoverButton_click = function(ea) {
                if ($("." + NotificationsPopoverTagclass).length !== 0) return;

                refreshNotifications({ animateBadge: false, repeatOnComplete: false });
            };

            var anyNotificationListItem_mouseEnter = function(ea) {
                $(ea.currentTarget).find(".gh-notif-dismiss").removeClass("hide");
            };

            var anyNotificationListItem_mouseLeave = function(ea) {
                $(ea.currentTarget).find(".gh-notif-dismiss").addClass("hide");
            };

            var anyNotificationListItemXbutton_click = function(ea) {
                var id = _int.parseExact($(ea.currentTarget).find(".js-notification-id").text());
                if (!id)
                    throw new Error("[NC.ANLIXBC01] [BUG] Failed to get the notification-id of the notification-item!");

                var notificationsIds = [id];
                _notificationsService.markNotificationsRead({
                    data: { notificationsIds: notificationsIds },
                    done: notificationsService_markNotificationsReadDone,
                    fail: anyService_genericFailHandler,
                    extraCallbackData: { notificationsIds: notificationsIds }
                });
            };

            var refreshNotifications = function(config) {
                config = config || {};
                config.animateBadge = config.animateBadge !== undefined ? config.animateBadge : false;
                config.repeatOnComplete = config.repeatOnComplete !== undefined ? config.repeatOnComplete : false;

                _notificationsService.getNotifications({
                    data: {},
                    done: notificationsService_getNotificationsDone,
                    always: notificationsService_getNotificationsAlways,
                    extraCallbackData: { config: config }
                });
            };

            var popover_shown = function() {
                _$notificsBadge.addClass("hide");
            };

            var popover_hidden = function() {
                if (_int.parseExact(_$notificsBadge.text()) === 0) return;

                _$notificsBadge.removeClass("hide");
            };

            var notificationsService_markNotificationsReadDone = function(responseignored, xd) {
                $("ul.notifications  li")
                    .each(function() {
                        var $el = $(this);
                        var notifId = _int.parseExact($el.find(".gh-notif-dismiss").find(".js-notification-id").text());
                        if ($.inArray(notifId, xd.notificationsIds) !== -1) {
                            if ($("ul.notifications  li").length > 1) {
                                $el.remove();
                            } else {
                                _$notifsButton.popover("destroy");
                            }

                            _$notificsBadge.text(math.max(_int.parseExact(_$notificsBadge.text()) - 1, 0));
                            if (_int.parseExact(_$notificsBadge.text()) === 0) {
                                _$notificsBadge.addClass("hide");
                            }
                        }
                    });
            };

            var notificationsService_getNotificationsAlways = function(xd) {
                if (!xd.config.repeatOnComplete) return;

                setTimeout(function () { refreshNotifications({ animateBadge: true, repeatOnComplete: true }); }, AutoRefreshInterval); //2 dontmovethis
            };

            var notificationsService_getNotificationsDone = function(notifications /*response*/, xd) {
                var notifsCount = notifications.length;
                var invokedFromPollingNotClick = xd.config.repeatOnComplete;
                var dialogAlreadyOpenNotClosed = $("." + NotificationsPopoverTagclass).length !== 0;

                if (dialogAlreadyOpenNotClosed && notifsCount === _int.parseExact(_$notificsBadge.text()))
                    return; //todo  we could improve this to have it compare timestamps instead

                _$notificsBadge.text(notifsCount);
                if (notifsCount === 0) {
                    _$notificsBadge.addClass("hide");
                    if (!invokedFromPollingNotClick) {
                        return;
                    }
                }

                if (notifsCount > 0 && !dialogAlreadyOpenNotClosed && xd.config.animateBadge) {
                    _$notificsBadge.removeClass("hide animated bounceInDown");
                    _$notificsBadge = resetElement(_$notificsBadge);
                    _$notificsBadge.addClass("animated bounceInDown");
                }

                var templateConfig = {
                    notificationsTemplateService: new NotificationsTemplateService(),
                    data: {
                        notifications: notifications
                    }
                };

                var html = _notificationsCompiledTemplate(templateConfig);
                if (dialogAlreadyOpenNotClosed) {
                    _$notifsButton.off("show.bs.popover", popover_shown);
                    _$notifsButton.off("hidden.bs.popover", popover_hidden);
                    _$notifsButton.data("bs.popover").options.content = html; //whenpolling
                    _$notifsButton.popover("show"); //crucial
                    _$notifsButton.on("show.bs.popover", popover_shown);
                    _$notifsButton.on("hidden.bs.popover", popover_hidden);
                } else if (invokedFromPollingNotClick) { //polling and dialogclosed
                    if (notifsCount > 0) {
                        _$notificsBadge.removeClass("hide");
                    }
                } else { //clicked and dialogclosed
                    _$notifsButton
                        .popover({
                            html: true,
                            title: "Notifications",
                            content: html,
                            placement: "bottom"
                        })
                        .popover("show")
                        .data("bs.popover")
                        .tip()
                        .addClass(NotificationsPopoverTagclass); //2
                }
            };

            var resetElement = function($elem) {
                $elem.before($elem.clone(true));
                var $newElem = $elem.prev();
                $elem.remove();
                return $newElem;
            };

            var anyService_genericFailHandler = function(xd) {
                bootbox.dialog({ title: "Error", message: "Operation failed!" });
            };
        };

        return notificationsController;
    }
);
