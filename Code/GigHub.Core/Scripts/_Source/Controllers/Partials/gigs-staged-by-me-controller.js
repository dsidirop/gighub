﻿define(
    "gigs-staged-by-me-controller",
    [
        "jquery",
        "jsonlib",
        "bootbox",
        "bootbox-locales",
        "document",
        "gigs-service"
    ],
    function (
        $,
        jsonlib,
        bootbox,
        bootboxLocales,
        document,
        GigsService,
        undefined
    ) {
        "use strict";

        var gigsStagedByMeController = function () {
            var _gigsService = null;

            this.Init = function() {
                _gigsService = new GigsService();

                $(document).on("click", ".js-togglecancel-gig", btnToggleCancelGig_click);
            };

            var btnToggleCancelGig_click = function(ea) {
                var $link = $(ea.currentTarget);
                var cancelNotUncancel = !$link.hasClass("gh-undo-cancel");

                bootbox.dialog({
                    title: "Confirmation Needed",
                    message: "Are you sure you want to " + (cancelNotUncancel ? "" : "un-") + "cancel this Gig?",
                    buttons: {
                        no: { label: "No", className: "btn-success" },
                        yes: { label: "Yes", className: "btn-danger", callback: closureBootboxDialog_yes({ $link: $link, cancelNotUncancel: cancelNotUncancel }) }
                    }
                });
            };

            var closureBootboxDialog_yes = function(data) { // { $link cancelNotUncancel }
                return function () {
                    _gigsService.setCancelStatus({
                        data: {
                            gigId: data.$link.attr("data-gig-id"),
                            cancelNotUncancel: data.cancelNotUncancel
                        },
                        done: gigService_done,
                        fail: anyService_genericFailHandler,
                        extraCallbackData: {
                            $link: data.$link,
                            cancelNotUncancel: data.cancelNotUncancel
                        }
                    });
                };
            };

            var gigService_done = function(response, xd) {
                xd
                    .$link
                    .toggleClass("gh-undo-cancel", xd.cancelNotUncancel) //           order
                    .text(xd.cancelNotUncancel ? "Undo Cancel" : "Cancel") //         order
                    .closest(".gh-gig-result-entry") //                               order
                    .toggleClass("gh--tag--gig-cancelled", xd.cancelNotUncancel); //  order
            };

            var anyService_genericFailHandler = function(xd) {
                bootbox.dialog({ title: "Error", message: "Operation failed!" });
            };
        };

        return gigsStagedByMeController;
    }
);

