﻿// requirejsnet.veritech.io/setup.html
//
// bare in mind that this controller is being used by both the index view of the home page and the gigsiamattending view
//

define(
    "gig-create-or-edit-controller",
    [
        "jquery",
        "jsonlib",
        "document",
        "jquery.select2",
        "select2-default-settings-service",
        undefined
    ],
    function (
        $,
        jsonlib,
        document,
        select2,
        Select2DefaultSettingsService,
        undefined
    ) {
        "use strict";

        var _select2DefaultSettingsService;

        var controller = function () {

            var _$document;
            var _$els = {
                $rootContainer: null,

                $ddlGenres: null
            };

            this.Init = function() {
                initServices();
                grabElements();
                setupGenresDropDownList();

                return this;
            };

            function initServices() {
                _select2DefaultSettingsService = new Select2DefaultSettingsService();
                _select2DefaultSettingsService.Init();
            };

            function grabElements() {
                _$document = $(document);

                _$els.$rootContainer = _$document.find("#GigCreationForm");
                if (_$els.$rootContainer.length !== 1)
                    throw new Error("[GCOEC.IE01] [BUG] Expected exactly one element but found " + _$els.$rootContainer.length);

                // gh-venue
                _$els.$txtVenue = _$els.$rootContainer.find(".gh-venue");
                if (_$els.$txtVenue.length !== 1)
                    throw new Error("[GCOEC.IE02] [BUG] Expected exactly one element but found " + _$els.$txtVenue.length);

                _$els.$ddlGenres = _$els.$rootContainer.find(".gh-genre");
                if (_$els.$ddlGenres.length !== 1)
                    throw new Error("[GCOEC.IE03] [BUG] Expected exactly one element but found " + _$els.$ddlGenres.length);
            };

            function setupGenresDropDownList() {
                _$els
                    .$ddlGenres
                    .select2();

                _$els
                    .$ddlGenres
                    .siblings(".select2")
                    .find(".select2-selection")
                    .css("height", _$els.$txtVenue.css("height")); //for good measure
            };
        };

        return controller;
    }
);
