﻿// requirejsnet.veritech.io/setup.html
//
// bare in mind that this controller is being used by both the index view of the home page and the gigsiamattending view
//

define(
    "artists-followed-by-user-controller",
    [
        "jquery",
        "jsonlib",
        "document",
        "bootbox",
        "bootbox-locales",
        "artists-followings-service"
    ],
    function (
        $,
        jsonlib,
        document,
        bootbox,
        bootboxLocales,
        artistsFollowingsService,
        undefined
    ) {
        "use strict";

        var singleGigController = function () {

            var _artistsFollowingsService = null;

            this.Init = function() {
                _artistsFollowingsService = new artistsFollowingsService();

                $(document).on("click", ".js-toggle-followartist", btnFollowArtistToggle_toggleFollowArtist);

                return this;
            };

            var btnFollowArtistToggle_toggleFollowArtist = function(ea) {
                var $button = $(ea.currentTarget);
                _artistsFollowingsService.toggleFollowing({
                    data: {
                        artistId: $button.attr("data-artist-id")
                    },
                    done: artistsFollowingsService_toggleFollowingDone,
                    fail: anyServiceCall_genericFailHandler,
                    extraCallbackData: {
                        $button: $button
                    }
                });
            };

            var artistsFollowingsService_toggleFollowingDone = function(response, xd) {
                xd.$button
                    .removeClass(response.following ? "btn-default" : "btn-info") //todo  replace this with toggleclass
                    .addClass(response.following ? "btn-info" : "btn-default")
                    .text(response.following ? "(Unfollow ?)" : "(Refollow ?)");
            };

            var anyServiceCall_genericFailHandler = function() {
                bootbox.dialog({title: "Error", message: "Operation failed!"});
            };
        };

        return singleGigController;
    }
);
