﻿Healthcheck Tip

To make sure our sitecss is sane and that firefox hasnt cached an outdated version of it it makes sense to rename it from time to time
to site.foobar.css while tweaking the bundles appropriately and then relaunch the project which will in turn force firefox to load the
latest version of our sitecss   You might be surprised with the amount of glitches that will forced to reveal themselves this way

todo   use scss instead of less