﻿using GigHub.Core.Foundation.Persistence;
using GigHub.Core.Persistence.Repositories;
using System.Threading.Tasks;

namespace GigHub.Core.Persistence
{
    public class UnitOfWork : IUnitOfWork
    {
        public readonly IApplicationDbContext _context;

        public IGigsRepository GigsRepo { get; }
        public IGenresRepository GenresRepo { get; }
        public IAttendancesRepository AttendancesRepo { get; }
        public IArtistsFollowingsRepository ArtistsFollowingsRepo { get; }
        public IUserHasReadNotificationsRepository UserHasReadNotificationsRepo { get; }

        public UnitOfWork(IApplicationDbContext context)
        {
            _context = context;

            GigsRepo = new GigsRepository(context);
            GenresRepo = new GenresRepository(context);
            AttendancesRepo = new AttendancesRepository(context);
            ArtistsFollowingsRepo = new ArtistsFollowingsRepository(context);
            UserHasReadNotificationsRepo = new UserHasReadNotificationsRepository(context);
        }

        public void CommitChanges() => _context.SaveChanges();
        public async Task CommitChangesAsync() => await _context.SaveChangesAsync().ConfigureAwait(continueOnCapturedContext: false);
    }
}