﻿using GigHub.Core.Persistence.Contexts;
using System.Data.Entity.Migrations;

//forwardwarning   http://stackoverflow.com/a/15832184/863651  if the need arises to delete and recreate the database then apart from deleting the mdf file
//forwardwarning   under the app_data folder one also needs to open the sqlserverexplorer in visualstudio and delete it from (localdb)\MSSQLLocalDB as well
//forwardwarning   it goes without saying that after performing the above steps you can finally recreate the database using   update-database   in nuget
namespace GigHub.Core.Persistence.Migrations
{
    sealed public class ApplicationDbContextConfiguration : DbMigrationsConfiguration<ApplicationDbContext> //0 public
    {
        public ApplicationDbContextConfiguration()
        {
            MigrationsDirectory = @"Persistence\Migrations";
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(ApplicationDbContext context) //called after migrating to the latest version
        {
        }

        //0 the class needs to be public because its being used inside the integration testing project
    }
}

