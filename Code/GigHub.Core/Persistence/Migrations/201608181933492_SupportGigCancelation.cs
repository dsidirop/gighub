using System.Data.Entity.Migrations;

namespace GigHub.Core.Persistence.Migrations
{
    public partial class SupportGigCancelation : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Gigs", "GotCancelled", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Gigs", "GotCancelled");
        }
    }
}
