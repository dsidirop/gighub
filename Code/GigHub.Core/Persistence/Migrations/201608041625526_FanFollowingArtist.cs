﻿using System.Data.Entity.Migrations;

namespace GigHub.Core.Persistence.Migrations
{
    public partial class FanFollowingArtist : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FanFollowingArtists",
                c => new
                    {
                        FanId = c.String(nullable: false, maxLength: 128),
                        ArtistId = c.String(nullable: false, maxLength: 128)
                    })
                .PrimaryKey(t => new { t.FanId, t.ArtistId })
                .ForeignKey("dbo.AspNetUsers", t => t.FanId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.ArtistId, cascadeDelete: false)
                .Index(t => t.FanId)
                .Index(t => t.ArtistId);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FanFollowingArtists", "ArtistId", "dbo.AspNetUsers");
            DropForeignKey("dbo.FanFollowingArtists", "FanId", "dbo.AspNetUsers");
            DropIndex("dbo.FanFollowingArtists", new[] { "ArtistId" });
            DropIndex("dbo.FanFollowingArtists", new[] { "FanId" });
            DropTable("dbo.FanFollowingArtists");
        }
    }
}
