namespace GigHub.Core.Persistence.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class ConvertDataAnnotationsToFluentAPI : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Gigs", "DateTime", c => c.DateTime(nullable: false, precision: 0, storeType: "datetime2"));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Gigs", "DateTime", c => c.DateTime(nullable: false));
        }
    }
}
