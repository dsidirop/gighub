﻿using System.Data.Entity.Migrations;

namespace GigHub.Core.Persistence.Migrations
{
    public partial class AddNamePropertyToAppUser : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "Name", c => c.String(nullable: false, maxLength: 256));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "Name");
        }
    }
}
