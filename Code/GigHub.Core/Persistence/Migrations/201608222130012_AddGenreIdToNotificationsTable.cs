using System.Data.Entity.Migrations;

namespace GigHub.Core.Persistence.Migrations
{
    public partial class AddGenreIdToNotificationsTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Notifications", "OriginalGenreId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Notifications", "OriginalGenreId");
        }
    }
}
