﻿- Note that since we moved the Migrations folder from the root of the project we had to tweak Configuration.cs to include << MigrationsDirectory = @"Persistence\Migrations"; >>

- In general we shouldnt be moving around these classes in various folders lightheartedly because when doing so we are prone to change the namespaces these classes live under
  and this change will in turn clog the migrationshistory in the database that already exists to break

- If you make up your mind to change the parent folder/namespaces of these classes then you will probably need to delete the database and recreate it from scratch via update-database
  to have the database reinitialized correctly from the getgo

  Finally do not be tempted to move these classes to a different folder and keep the namespaces the same as it is very easy for someone down the road to fix namespaces globally using
  resharper which will autoadjust the namespaces in the very same classes that we desire to remain with the old namespace  This is bound to break the database migrations causing alot
  of frustrating headscratching in terms what was the culprit behind the breakage
