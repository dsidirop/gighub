namespace GigHub.Core.Persistence.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NotificationModelIntroduceExplicitFluentConfiguration : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Notifications", "DateTime", c => c.DateTime(nullable: false, precision: 0, storeType: "datetime2"));
            AlterColumn("dbo.Notifications", "OriginalDateTime", c => c.DateTime(precision: 0, storeType: "datetime2"));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Notifications", "OriginalDateTime", c => c.DateTime());
            AlterColumn("dbo.Notifications", "DateTime", c => c.DateTime(nullable: false));
        }
    }
}
