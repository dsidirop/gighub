using System.Data.Entity.Migrations;

namespace GigHub.Core.Persistence.Migrations
{
    public partial class EnrichNotificationWithGenreColumn : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Notifications", "OriginalGenre_Id", c => c.Byte());
            CreateIndex("dbo.Notifications", "OriginalGenre_Id");
            AddForeignKey("dbo.Notifications", "OriginalGenre_Id", "dbo.Genres", "Id");
            DropColumn("dbo.Notifications", "OriginalGenreId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Notifications", "OriginalGenreId", c => c.Int(nullable: false));
            DropForeignKey("dbo.Notifications", "OriginalGenre_Id", "dbo.Genres");
            DropIndex("dbo.Notifications", new[] { "OriginalGenre_Id" });
            DropColumn("dbo.Notifications", "OriginalGenre_Id");
        }
    }
}
