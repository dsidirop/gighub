﻿using System.Data.Entity.Migrations;

namespace GigHub.Core.Persistence.Migrations
{
    public partial class SeedGenres : DbMigration
    {
        public override void Up()
        {
            Sql(@"
                INSERT INTO Genres (Id, Name) VALUES (1, 'Jazz');
                INSERT INTO Genres (Id, Name) VALUES (2, 'Rock');
                INSERT INTO Genres (Id, Name) VALUES (3, 'Blues');
                INSERT INTO Genres (Id, Name) VALUES (4, 'Classical');
            ");
        }

        public override void Down()
        {
            Sql(@"DELETE FROM Genres WHERE Id IN (1, 2, 3, 4)");
        }
    }
}
