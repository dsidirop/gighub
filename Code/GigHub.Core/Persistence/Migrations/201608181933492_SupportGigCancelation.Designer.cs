// <auto-generated />

using System.CodeDom.Compiler;
using System.Data.Entity.Migrations.Infrastructure;
using System.Resources;

namespace GigHub.Core.Persistence.Migrations
{
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class SupportGigCancelation : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(SupportGigCancelation));
        
        string IMigrationMetadata.Id
        {
            get { return "201608181933492_SupportGigCancelation"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
