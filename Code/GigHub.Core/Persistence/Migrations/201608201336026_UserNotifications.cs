using System.Data.Entity.Migrations;

namespace GigHub.Core.Persistence.Migrations
{
    public partial class UserNotifications : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Notifications",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DateTime = c.DateTime(nullable: false),
                        Type = c.Int(nullable: false),
                        OriginalVenue = c.String(),
                        OriginalDateTime = c.DateTime(),
                        Gig_Id = c.Int(nullable: false)
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Gigs", t => t.Gig_Id, cascadeDelete: true)
                .Index(t => t.Gig_Id);
            
            CreateTable(
                "dbo.UserHasReadNotifications",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        NotificationId = c.Int(nullable: false),
                        IsRead = c.Boolean(nullable: false)
                    })
                .PrimaryKey(t => new { t.UserId, t.NotificationId })
                .ForeignKey("dbo.Notifications", t => t.NotificationId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: false)
                .Index(t => t.UserId)
                .Index(t => t.NotificationId);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserHasReadNotifications", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.UserHasReadNotifications", "NotificationId", "dbo.Notifications");
            DropForeignKey("dbo.Notifications", "Gig_Id", "dbo.Gigs");
            DropIndex("dbo.UserHasReadNotifications", new[] { "NotificationId" });
            DropIndex("dbo.UserHasReadNotifications", new[] { "UserId" });
            DropIndex("dbo.Notifications", new[] { "Gig_Id" });
            DropTable("dbo.UserHasReadNotifications");
            DropTable("dbo.Notifications");
        }
    }
}
