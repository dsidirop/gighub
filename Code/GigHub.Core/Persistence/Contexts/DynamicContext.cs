﻿using Microsoft.CodeDom.Providers.DotNetCompilerPlatform;
using System;
using System.CodeDom.Compiler;
using System.Collections.Concurrent;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Linq;

namespace GigHub.Core.Persistence.Contexts
{
    // not used by this project yet   this class is kept here for future reference   have a look at the following article
    // http://nodogmablog.bryanhogan.net/2013/08/entity-framework-in-an-dynamics-nav-navision-environment/
    // note that this approach is not optimal or even recommended by microsoft   the approach recommended by microsoft is
    // shown in hotmappedcontext
    sealed public class DynamicContext
    {
        static private readonly object LockObject = new object();
        static private readonly ConcurrentDictionary<string, Type> CompiledContexts = new ConcurrentDictionary<string, Type>();
        private const string Template = @"
            using System.Data.Entity;
            namespace Implementation.Context
            {{
                public class {0}_Context : CommonDBContext
                {{
                  public {0}_Context(string connectionString, string tablePrefix)
                        : base(connectionString, tablePrefix)
                    {{
                        Database.SetInitializer<{0}_Context>(null); //0
                    }}
                    //0 notice the type inside <>    its vital to have this tweak here instead of in the base class because we need
                    //  setinitializer to affect this class and not just the parent class
                }}
            }}
        ";

        private readonly string _sourceCode;
        private readonly string _classNamePrefix;

        #region Constructors

        public DynamicContext(string classNamePrefix)
        {
            _sourceCode = string.Format(Template, classNamePrefix);
            _classNamePrefix = classNamePrefix;
        }

        #endregion

        #region Public Methods

        static private readonly Type[] ConstructorTypes = { typeof(string), typeof(string) };
        public CommonDBContext GetContext(string connectionString, string tablePrefix)
        {
            var compiledType = (Type)null; // ReSharper disable once InconsistentlySynchronizedField
            if (CompiledContexts.TryGetValue(_classNamePrefix, out compiledType))
                return GetInstanceOfCompiledContext(connectionString, tablePrefix, compiledType, ConstructorTypes);

            lock (LockObject)
            {
                if (CompiledContexts.TryGetValue(_classNamePrefix, out compiledType))
                    return GetInstanceOfCompiledContext(connectionString, tablePrefix, compiledType, ConstructorTypes);

                compiledType = CompileContextAndExtractCompiledType(_classNamePrefix);
                CompiledContexts[_classNamePrefix] = compiledType;
            }

            return GetInstanceOfCompiledContext(connectionString, tablePrefix, compiledType, ConstructorTypes);
        }

        #endregion

        #region Private Methods

        static private CommonDBContext GetInstanceOfCompiledContext(string connectionString, string tablePrefix, Type compiledContext, Type[] constructorTypes)
        { // ReSharper disable once PossibleNullReferenceException
            return compiledContext.GetConstructor(constructorTypes).Invoke(new object[] { connectionString, tablePrefix }) as CommonDBContext;
        }

        private Type CompileContextAndExtractCompiledType(string classNamePrefix)
        {
            var compilerParameters = new CompilerParameters { GenerateInMemory = true };
            compilerParameters.ReferencedAssemblies.AddRange(GetAssembliesLocations(new[] { "Implementation.dll", "EntityFramework.dll" }));

            var compiledType = new CSharpCodeProvider()
                .CompileAssemblyFromSource(compilerParameters, _sourceCode)
                .CompiledAssembly
                .GetType($"Implementation.Context.{classNamePrefix}_Context");

            return compiledType;
        }

        static private string[] GetAssembliesLocations(string[] assemblyNames)
        {
            var locations = new string[assemblyNames.Length];
            for (var loop = 0; loop <= assemblyNames.Length - 1; loop++)
            {
                locations[loop] = AppDomain.CurrentDomain
                    .GetAssemblies()
                    .Where(a => !a.IsDynamic && a.ManifestModule.Name == assemblyNames[loop])
                    .Select(a => a.Location)
                    .First();
            }
            return locations;
        }

        #endregion
    }

    public abstract class CommonDBContext : DbContext
    {
        private readonly string tableName;

        protected CommonDBContext(string connectionString, string tableName)
            : base(connectionString)
        {
            this.tableName = tableName;
        }

        public DbSet Customer { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new CustomerMap(tableName));
        }
    }

    public class CustomerMap : EntityTypeConfiguration<Customer>
    {
        public CustomerMap(string tableName)
        {
            HasKey(t => t.CustomerID); // Primary Key

            ToTable(tableName); // Map the table name
        }
    }

    public class Customer
    {
        public string CustomerID { get; set; }
    }
}