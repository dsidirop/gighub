﻿using GigHub.Core.Foundation.Models;
using GigHub.Core.Foundation.Persistence;
using GigHub.Core.Persistence.EntityConfigurations;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;

namespace GigHub.Core.Persistence.Contexts
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>, IApplicationDbContext
    {
        public DbSet<Gig> Gigs { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<Attendance> Attendances { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<FanFollowingArtist> ArtistsFollowings { get; set; }
        public DbSet<UserHasReadNotification> UserHasReadNotifications { get; set; }

        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        static public ApplicationDbContext Create() => new ApplicationDbContext();

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new GigFluentConfiguration());
            modelBuilder.Configurations.Add(new GenreFluentConfiguration());
            modelBuilder.Configurations.Add(new AttendanceFluentConfiguration());
            modelBuilder.Configurations.Add(new NotificationFluentConfiguration());
            modelBuilder.Configurations.Add(new ApplicationUserFluentConfiguration());
            modelBuilder.Configurations.Add(new UserHasReadNotificationFluentConfiguration());

            base.OnModelCreating(modelBuilder);
        }
    }
}
