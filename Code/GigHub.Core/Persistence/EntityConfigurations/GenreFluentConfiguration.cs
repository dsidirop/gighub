using GigHub.Core.Foundation.Models;
using System.Data.Entity.ModelConfiguration;

namespace GigHub.Core.Persistence.EntityConfigurations
{
    sealed public class GenreFluentConfiguration : EntityTypeConfiguration<Genre>
    {
        public GenreFluentConfiguration()
        {
            HasKey(g => g.Id);
            Property(g => g.Id).IsRequired().HasColumnOrder(1);
            Property(x => x.Name).IsRequired().HasColumnType("nvarchar").HasMaxLength(128);
        }
    }
}