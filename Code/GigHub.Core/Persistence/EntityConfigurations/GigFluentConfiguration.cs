﻿using GigHub.Core.Foundation.Models;
using System.Data.Entity.ModelConfiguration;

namespace GigHub.Core.Persistence.EntityConfigurations
{
    sealed public class GigFluentConfiguration : EntityTypeConfiguration<Gig>
    {
        public GigFluentConfiguration()
        {
            HasKey(g => g.Id); //0
            Property(g => g.Id).IsRequired().HasColumnOrder(1);

            Property(g => g.Venue).IsRequired().HasColumnType("nvarchar").HasMaxLength(128).HasColumnOrder(4); //1 hasminlength(3) notsupported
            Property(g => g.GenreId).IsRequired().HasColumnOrder(3); //2 displayname("genre")
            Property(g => g.ArtistId).IsRequired().HasColumnOrder(2);
            Property(g => g.DateTime).IsRequired().HasColumnType("datetime2").HasPrecision(0).HasColumnOrder(5); //3 hasprecision
            Property(g => g.GotCancelled).IsRequired().HasColumnOrder(6);

            HasMany(g => g.Attendances).WithRequired(a => a.Gig).WillCascadeOnDelete(false); //4 5
            HasRequired(g => g.Genre).WithMany(/*no navback property yet*/).HasForeignKey(g => g.GenreId); //foreignkey wireup
            HasRequired(g => g.Artist).WithMany(/*no navback property yet*/).HasForeignKey(g => g.ArtistId); //foreignkey wireup
        }
    }
}
//0 Declaring the primary key thusly is not strictly needed but at the time of this writing I liked fluent config to be as explicit as possible
//1 MinLength is the only configuration that can be achieved using only dataannotation but has no counterpart in the fluentapi configurations
//2 Displayattribute is not used by ef hence theres no equivalent
//3 Hasprecision can be found only via fluentapi and there is no equivalent with data annotations   On a different note always make sure to use datetime2
//  as a matter of best practice   http://stackoverflow.com/a/14246660/863651    entity framework should have been doing this by default but it doesnt
//4 Entity framework needs to be informed that a many to many relationship indeed exists between attendance and gig
//5 We need to disable cascade delete on deleted gigs because mssqlserver complains when detecting multiple cascade delete paths on the same table from the same source

