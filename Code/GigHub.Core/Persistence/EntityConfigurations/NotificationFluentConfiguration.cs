using GigHub.Core.Foundation.Models;
using System.Data.Entity.ModelConfiguration;

namespace GigHub.Core.Persistence.EntityConfigurations
{
    sealed public class NotificationFluentConfiguration : EntityTypeConfiguration<Notification>
    {
        public NotificationFluentConfiguration()
        {
            HasKey(n => n.Id); //0
            Property(n => n.Id).IsRequired().HasColumnOrder(1);

            Property(n => n.Type).IsRequired().HasColumnOrder(2);
            Property(n => n.DateTime).IsRequired().HasColumnType("datetime2").HasPrecision(0).HasColumnOrder(3);

            Property(n => n.OriginalVenue).HasColumnOrder(4);
            Property(n => n.OriginalDateTime).HasColumnType("datetime2").HasPrecision(0).HasColumnOrder(5);

            HasRequired(n => n.Gig).WithMany();
            HasOptional(n => n.OriginalGenre).WithMany();
        }
    }
}