using GigHub.Core.Foundation.Models;
using System.Data.Entity.ModelConfiguration;

namespace GigHub.Core.Persistence.EntityConfigurations
{
    sealed public class ApplicationUserFluentConfiguration : EntityTypeConfiguration<ApplicationUser>
    {
        public ApplicationUserFluentConfiguration()
        {
            Property(u => u.Name).IsRequired().HasColumnType("nvarchar").HasMaxLength(256);

            HasMany(navigationPropertyExpression: u => u.Followees).WithRequired(navigationPropertyExpression: f => f.Fan).WillCascadeOnDelete(value: false);
            HasMany(navigationPropertyExpression: u => u.Followers).WithRequired(navigationPropertyExpression: f => f.Artist).WillCascadeOnDelete(value: false);
            HasMany(navigationPropertyExpression: u => u.UserNotifications).WithRequired(navigationPropertyExpression: uhrn => uhrn.User).WillCascadeOnDelete(value: false);
        }
    }
}