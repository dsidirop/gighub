using GigHub.Core.Foundation.Models;
using System.Data.Entity.ModelConfiguration;

namespace GigHub.Core.Persistence.EntityConfigurations
{
    sealed public class UserHasReadNotificationFluentConfiguration : EntityTypeConfiguration<UserHasReadNotification>
    {
        public UserHasReadNotificationFluentConfiguration()
        {
            HasKey(uhrn => new {uhrn.UserId, uhrn.NotificationId}); //0

            Property(uhrn => uhrn.UserId).IsRequired().HasColumnOrder(1);
            Property(uhrn => uhrn.NotificationId).IsRequired().HasColumnOrder(2);

            Property(uhrn => uhrn.IsRead).IsRequired().HasColumnOrder(0);

            HasRequired(uhrn => uhrn.User).WithMany(au => au.UserNotifications).HasForeignKey(uhrn => uhrn.UserId); //keepseparate   foreignkeywireup
            HasRequired(uhrn => uhrn.Notification).WithMany(/*no navback prop*/).HasForeignKey(uhrn => uhrn.NotificationId); //foreignkeywireup
        }
    }
}
//0 Declaring the primary key thusly is not strictly needed but at the time of this writing I liked fluent config to be as explicit as possible