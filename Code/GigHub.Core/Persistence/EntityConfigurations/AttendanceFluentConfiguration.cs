using GigHub.Core.Foundation.Models;
using System.Data.Entity.ModelConfiguration;

namespace GigHub.Core.Persistence.EntityConfigurations
{
    sealed public class AttendanceFluentConfiguration : EntityTypeConfiguration<Attendance>
    {
        public AttendanceFluentConfiguration()
        {
            HasKey(a => new {a.GigId, a.AttendeeId}); //0
            Property(a => a.GigId).IsRequired().HasColumnOrder(1);
            Property(a => a.AttendeeId).IsRequired().HasColumnOrder(2);

            HasRequired(a => a.Gig).WithMany(g => g.Attendances).HasForeignKey(g => g.GigId); //foreignkey wireup
            HasRequired(a => a.Attendee).WithMany(/*no navback prop*/).HasForeignKey(a => a.AttendeeId); //foreignkey wireup
        }
    }
}
//0 Declaring the primary key thusly is not strictly needed but at the time of this writing I liked fluent config to be as explicit as possible