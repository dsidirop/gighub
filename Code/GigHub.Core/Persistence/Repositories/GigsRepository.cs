using GigHub.Core.Foundation.Models;
using GigHub.Core.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace GigHub.Core.Persistence.Repositories
{
    //reminder1   a repository is a collection of domain and datamapping layers using a collectionlike interface
    //reminder1   for accessing domain objects and can only be hosted in memory
    //reminder1
    //reminder1   given the above definition it goes without saying that the repository does not and should not
    //reminder1   ever host dboperations like savetodb or similar
    //reminder1
    //reminder1   savetodb related operations must be hosted and performed only at the unitofwork level
    //reminder1   if a dboperations ala savetodb is found to be performed outside the unitofwork level then we are
    //reminder1   doing something wrong
    //reminder
    //reminder2   always bare in mind that as a matter of good craftsmanship repositories should never return an
    //reminder2   iqueryable in any of their methods of their interfaces   leaking iqueryable is considered an anti
    //reminder2   pattern as explained in this blogpost   https://weblogs.asp.net/dotnetstories/repository-iqueryable
    //reminder2   so far I have come across only one extremely rare exception which concerns returning iqueryable for
    //reminder2   use with odatacontrollers  but even in this scenario there are ways implement repositories properly
    sealed public class GigsRepository : IGigsRepository
    {
        private IApplicationDbContext _context;

        public IApplicationDbContext Context
        {
            get { return _context; }
            set { _context = value; }
        }

        public GigsRepository()
        {
        }

        public GigsRepository(IApplicationDbContext context)
        {
            _context = context;
        }

        public void Add(Gig gig) => _context.Gigs.Add(gig);

        public async Task<Gig> GetSingleAsync(int gigId, bool includeGenre = false, bool includeAttendances = false, bool includeAttendancesAttendees = false, bool includeArtist = false, bool includeArtistFollowers = false)
        {
            var gigs = _context.Gigs as IQueryable<Gig>;
            if (includeGenre)
            {
                gigs = gigs.Include(x => x.Genre);
            }

            if (includeAttendances)
            {
                gigs = gigs.Include(x => x.Attendances);
            }

            if (includeAttendancesAttendees)
            {
                gigs = gigs.Include(x => x.Attendances.Select(y => y.Attendee));
            }

            if (includeArtist)
            {
                gigs = gigs.Include(x => x.Artist);
            }

            if (includeArtistFollowers)
            {
                gigs = gigs.Include(x => x.Artist.Followers);
            }

            return await gigs.FirstOrDefaultAsync(x => x.Id == gigId).ConfigureAwait(continueOnCapturedContext: false);
        }

        public async Task<IEnumerable<Gig>> GetMultipleAsync(string artistId = null, string searchTerm = null, bool onlyFutureOnesNotAll = false, bool includeCancelled = true)
        {
            searchTerm = searchTerm?.Trim().ToLower();

            var gigs = _context.Gigs.Where(x => true); //0 unittesting stupidhack
            if (!string.IsNullOrEmpty(artistId))
            {
                gigs = gigs.Where(x => x.ArtistId == artistId);
            }

            if (!string.IsNullOrEmpty(searchTerm))
            {
                gigs = gigs.Where(x => x.Venue.ToLower().Contains(searchTerm) || x.Genre.Name.ToLower().Contains(searchTerm) || x.Artist.Name.ToLower().Contains(searchTerm)); //1 case
            }

            if (onlyFutureOnesNotAll)
            {
                var now = DateTime.Now;
                gigs = gigs.Where(x => x.DateTime >= now);
            }

            if (!includeCancelled)
            {
                gigs = gigs.Where(x => !x.GotCancelled);
            }

            return await gigs
                .Include(x => x.Genre) //2 eagerload
                .Include(x => x.Artist)
                .OrderBy(x => x.DateTime)
                .ToListAsync()
                .ConfigureAwait(continueOnCapturedContext: false);
        }
        //0 For the unittest getmultiple_get_shouldbereturned to succeed we had to resort to calling the where clause at least once otherwise we would end up with
        //  a null reference exception in our hands   must make a stackoveflow question about this
        //1 Performing a case insensitive search here is harder than one would imagine  http://stackoverflow.com/questions/3360772/linq-contains-case-insensitive
        //2 Its crucial to explicitly eagerload the genre property of the gig otherwise the resulting properties will come out nullified
    }
}