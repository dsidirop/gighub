using GigHub.Core.Foundation.Models;
using GigHub.Core.Foundation.Persistence;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace GigHub.Core.Persistence.Repositories
{
    public class AttendancesRepository : IAttendancesRepository
    {
        private IApplicationDbContext _context;

        public IApplicationDbContext Context
        {
            get { return _context; }
            set { _context = value; }
        }

        public AttendancesRepository()
        {
        }

        public AttendancesRepository(IApplicationDbContext context)
        {
            _context = context;
        }

        public void Add(string userId, int gigId)
            => _context.Attendances.Add(new Attendance { AttendeeId = userId, GigId = gigId });

        public void Delete(Attendance attendance)
        => _context.Attendances.Remove(attendance);

        public async Task<Attendance> GetSingleAsync(string userId, int gigId)
            => await _context.Attendances.FirstOrDefaultAsync(x => x.AttendeeId == userId && x.GigId == gigId).ConfigureAwait(continueOnCapturedContext: false);

        public async Task<IEnumerable<Attendance>> GetAllMatchingAsync(string userId, string searchTerm = null, IEnumerable<int> gigIds = null)
        {
            var skipGigIdsCheck = gigIds == null; //order
            var searchTermLower = searchTerm?.Trim().ToLower();
            var shouldIncludeAll = string.IsNullOrEmpty(searchTerm);

            gigIds = gigIds ?? new List<int>(0); //order vital

            return await _context.Attendances
                .Where(x => x.AttendeeId == userId)
                .Where(x => shouldIncludeAll
                            || x.Gig.Venue.ToLower().Contains(searchTermLower) //0 case
                            || x.Gig.Genre.Name.ToLower().Contains(searchTermLower)
                            || x.Gig.Artist.Name.ToLower().Contains(searchTermLower)) //1
                .Where(x => skipGigIdsCheck || gigIds.Contains(x.GigId))
                .Include(x => x.Gig) //2 deadlast
                .Include(x => x.Gig.Genre) //2 deadlast
                .Include(x => x.Gig.Artist) //2 deadlast
                .ToListAsync() //3 unittest issue
                .ConfigureAwait(continueOnCapturedContext: false);
        }
        //0 using lowercase is just about the only practical way we have to achieve caseinsensitive matching
        //1 its crucial to explicitly eagerload the genre and artist properties of the gig otherwise the resulting properties will come out nullified
        //2 its crucial to use eagerloading deadlast after all where constraints have been placed   if the order of the two is reversed the mechanism wont work
        //3 note that tolistasync is very troublesome when it comes to unittesting it against inmemory mocks of db and dbsets   such unittests throw an exception
        //  upon invoking tolistasync about the operation being unsupported and stuff
    }
}