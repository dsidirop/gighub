using GigHub.Core.Foundation.Models;
using GigHub.Core.Foundation.Persistence;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;

namespace GigHub.Core.Persistence.Repositories
{
    public class GenresRepository : IGenresRepository
    {
        private readonly IApplicationDbContext _context;

        public GenresRepository(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Genre>> GetAllAsync() => await _context.Genres.ToListAsync().ConfigureAwait(continueOnCapturedContext: false);
        public async Task<Genre> GetAsync(byte genreId) => await _context.Genres.SingleAsync(x => x.Id == genreId).ConfigureAwait(continueOnCapturedContext: false);
    }
}