﻿using GigHub.Core.Foundation.Models;
using GigHub.Core.Foundation.Persistence;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace GigHub.Core.Persistence.Repositories
{
    public class ArtistsFollowingsRepository : IArtistsFollowingsRepository
    {
        private readonly IApplicationDbContext _context;

        public ArtistsFollowingsRepository(IApplicationDbContext context)
        {
            _context = context;
        }

        public void Add(string fanId, string artistId)
            => _context.ArtistsFollowings.Add(new FanFollowingArtist { FanId = fanId, ArtistId = artistId });

        public void Delete(FanFollowingArtist fanFollowingArtist)
            => _context.ArtistsFollowings.Remove(fanFollowingArtist);

        public async Task<FanFollowingArtist> GetSingleAsync(string fanId, string artistId)
            => await _context.ArtistsFollowings
                .FirstOrDefaultAsync(x => x.FanId == fanId && x.ArtistId == artistId)
                .ConfigureAwait(continueOnCapturedContext: false);

        public async Task<IEnumerable<FanFollowingArtist>> GetMultipleAsync(string fanId, IEnumerable<string> artistsId = null) //0 caution tolistasync
        {
            var ignoreArtistsIds = artistsId == null; //order
            artistsId = artistsId ?? new List<string>(0); //order vital

            return await _context.ArtistsFollowings
                .Where(x => x.FanId == fanId)
                .Where(x => ignoreArtistsIds || artistsId.Contains(x.ArtistId))
                .Include(x => x.Artist) //1
                .ToListAsync()
                .ConfigureAwait(continueOnCapturedContext: false);
        }
        //0 use tolistasync with caution given the fact that at least as far as entityfr6 there is a performance issue when querying
        //  varbinary(max), varchar(max), nvarchar(max) or xml columns ala async   the overhead induced is at an order of magnitude
        //  of 8 or so compared to the classic straightforward nonasync approach
        //1 including artist is needed only by artistsfollowedbyuser
    }
}