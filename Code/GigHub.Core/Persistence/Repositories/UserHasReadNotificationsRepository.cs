using GigHub.Core.Foundation.Models;
using GigHub.Core.Foundation.Persistence;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace GigHub.Core.Persistence.Repositories
{
    public class UserHasReadNotificationsRepository : IUserHasReadNotificationsRepository
    {
        private IApplicationDbContext _context;

        public IApplicationDbContext Context
        {
            get { return _context; }
            set { _context = value; }
        }

        public UserHasReadNotificationsRepository()
        {
        }

        public UserHasReadNotificationsRepository(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<UserHasReadNotification>> GetUnreadNotificationsAsync(string userId, IEnumerable<int> ids = null)
        {
            var alwaysGreenlightIdsCheck = ids == null; //order
            ids = ids ?? new List<int>(0); //order vital

            return await _context
                .UserHasReadNotifications
                .Where(x => x.UserId == userId)
                .Where(x => !x.IsRead)
                .Where(x => alwaysGreenlightIdsCheck || ids.Any(y => y == x.NotificationId))
                .Include(x => x.Notification)
                .Include(x => x.Notification.Gig)
                .Include(x => x.Notification.Gig.Genre)
                .Include(x => x.Notification.Gig.Artist)
                .Include(x => x.Notification.OriginalGenre)
                .OrderBy(x => x.Notification.DateTime)
                .ToListAsync()
                .ConfigureAwait(continueOnCapturedContext: false);
        }
    }
}