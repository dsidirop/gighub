﻿using System.Web.Mvc;
using System.Web.SessionState;

namespace GigHub.Core.Controllers.BaseControllers.Mvc
{
    [SessionState(SessionStateBehavior.ReadOnly)]
    public abstract class MvcBaseController : Controller
    {
    }
}