﻿using System.Web.Http;
using GigHub.Core.Utilities.DataAnnotationAttributes;

namespace GigHub.Core.Controllers.BaseControllers.Api
{
    [AjaxOnlyController_Webapi]
    public abstract class WebApiBaseController : ApiController
    {
    }
}