﻿using GigHub.Core.Controllers.BaseControllers.Api;
using GigHub.Core.Foundation.Dtos;
using GigHub.Core.Foundation.Models;
using GigHub.Core.Foundation.ServicesX.Notifications;
using GigHub.Core.Foundation.ServicesX.Notifications.Exceptions;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using AutoMapper;
using GigHub.Core.Foundation.Mapper;

//0 when using [authorize] on a webapi controller in the context of mvc5 we also need to tweak startupauthcs as described in
//
//               http://kevin-junghans.blogspot.gr/2013/12/returning-401-http-status-code-on.html
//
//  if startupauthcs is not tweaked then unauthorized ajax calls will be responded with 200ok response which is utterly crazy
namespace GigHub.Core.Controllers.Api
{
    [Authorize] //0 read startupauthcs
    public class NotificationsController : WebApiBaseController
    {
        private readonly IMappingService _mapper;
        private readonly INotificationsService _notificationsService;

        public NotificationsController(
            IMappingService mapper,
            INotificationsService notificationsService
        )
        {
            _mapper = mapper;
            _notificationsService = notificationsService;
        }

        [HttpGet]
        [ResponseType(typeof(IEnumerable<NotificationDto>))]
        public async Task<IHttpActionResult> Get() //todo   show only first 5 or so notifications and send over their total number as well
        {
            var userId = User.Identity.GetUserId();
            var unreadNotifications = await _notificationsService.GetUnreadNotificationsAsync(userId).ConfigureAwait(continueOnCapturedContext: false);

            var results = unreadNotifications.Select(x => _mapper.Map<Notification, NotificationDto>(x.Notification));
            return Ok(results);
        }

        [HttpPost]
        public async Task<IHttpActionResult> MarkAsRead([Required, FromBody] MarkAsReadOptions options) //0
        {
            if (options?.Ids != null && !options.Ids.Any())
                return Ok();

            try
            {
                await _notificationsService.MarkNotificationsAsReadAsync(
                    userId: User.Identity.GetUserId(),
                    notificationsIds: options?.Ids
                ).ConfigureAwait(continueOnCapturedContext: false);
            }
            catch (NotificationBelongsToDifferentArtistException)
            {
                return StatusCode(HttpStatusCode.Forbidden);
            }

            return Ok();

            //0 we prefer using post rather than delete since we are simply modifying data rather than deleting them
        }
    }
}