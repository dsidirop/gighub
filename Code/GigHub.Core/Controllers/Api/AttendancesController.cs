﻿using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using GigHub.Core.Controllers.BaseControllers.Api;
using GigHub.Core.Foundation.Dtos;
using GigHub.Core.Foundation.ServicesX.Gigs;
using GigHub.Core.Foundation.ServicesX.Gigs.Exceptions;
using Microsoft.AspNet.Identity;

//0 when using [authorize] on a webapi controller in the context of mvc5 we also need to tweak startupauthcs as described in
//
//               http://kevin-junghans.blogspot.gr/2013/12/returning-401-http-status-code-on.html
//
//  if authsetupcs is not tweaked then unauthorized ajax calls will be responded with 200ok response which is utterly crazy

namespace GigHub.Core.Controllers.Api
{
    [Authorize] //0 read startupauthcs
    public class AttendancesController : WebApiBaseController
    {
        private readonly IGigsService _gigsService;

        public AttendancesController(IGigsService gigsService)
        {
            _gigsService = gigsService;
        }

        [HttpPost]
        public async Task<IHttpActionResult> ToggleAttendance([Required] [FromBody] AttendanceDto dto) //gigstreamjs
        {
            //if (!ModelState.IsValid) return BadRequest(@"Invalid options"); //no need due to global filter

            var userId = User.Identity.GetUserId();

            var goingVerdict = false;
            try
            {
                goingVerdict = await _gigsService.ToggleGigAttendanceForUserAsync(userId, dto.GigId);
            }
            catch (GigNotFoundException)
            {
                return BadRequest(@"Gig doesn't exist");
            }
            catch (GigCannotBeAttendedByOwnerArtistHimselfException)
            {
                return StatusCode(HttpStatusCode.Forbidden);
            }
            catch (GigAttendanceTweakingFailedException) //unique constraint violation
            {
                goingVerdict = true; //user already going to gig
            }

            return Ok(content: new AttendanceVerdictDto { Going = goingVerdict });
        }
    }
}
