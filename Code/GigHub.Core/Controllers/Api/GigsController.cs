﻿using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using GigHub.Core.Controllers.BaseControllers.Api;
using GigHub.Core.Foundation.Attributes.WebAPI;
using GigHub.Core.Foundation.Dtos;
using GigHub.Core.Foundation.ServicesX.Gigs;
using GigHub.Core.Foundation.ServicesX.Gigs.Exceptions;
using Microsoft.AspNet.Identity;

//0 when using [authorize] on a webapi controller in the context of mvc5 we also need to tweak startupauthcs as described in
//
//               http://kevin-junghans.blogspot.gr/2013/12/returning-401-http-status-code-on.html
//
//  if startupauthcs is not tweaked then unauthorized ajax calls will be responded with 200ok response which is utterly crazy
namespace GigHub.Core.Controllers.Api
{
    [Authorize] //0 read startupauthcs
    public class GigsController : WebApiBaseController
    {
        [HttpPost]
        public async Task<IHttpActionResult> SetCancelStatus([Required] GigToToggleCancelStateDto dto) // FromBody
        {
            //if (!ModelState.IsValid) return BadRequest(@"Invalid options"); //no need due to global filter

            try
            {
                await _gigsService.SetGigCancelStatusAsync(User.Identity.GetUserId(), dto.GigId, dto.CancelNotUncancel);
            }
            catch (GigNotFoundException)
            {
                return StatusCode(HttpStatusCode.NotFound);
            }
            catch (GigBelongsToDifferentArtistException)
            {
                return StatusCode(HttpStatusCode.Forbidden);
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        private readonly IGigsService _gigsService;

        public GigsController(IGigsService gigsService)
        {
            _gigsService = gigsService;
        }
    }
}
