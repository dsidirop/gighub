﻿using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using System.Web.Http;
using GigHub.Core.Controllers.BaseControllers.Api;
using GigHub.Core.Foundation.Dtos;
using GigHub.Core.Foundation.ServicesX.Artists;
using GigHub.Core.Foundation.ServicesX.Artists.Exceptions;
using Microsoft.AspNet.Identity;

//0 when using [authorize] on a webapi controller in the context of mvc5 we also need to tweak startupauthcs as described in
//
//               http://kevin-junghans.blogspot.gr/2013/12/returning-401-http-status-code-on.html
//
//  if startupauthcs is not tweaked then unauthorized ajax calls will be responded with 200ok response which is utterly crazy
namespace GigHub.Core.Controllers.Api
{
    [Authorize] //0 read startupauthcs
    public class ArtistsFollowingsController : WebApiBaseController
    {
        private readonly IArtistsService _artistsService;

        public ArtistsFollowingsController(IArtistsService artistsService)
        {
            _artistsService = artistsService;
        }

        [HttpPost]
        public async Task<IHttpActionResult> ToggleFollowing([Required] [FromBody] FanWantsToFollowArtistDto dto) //artistsfollowingsservicejs
        {
            //if (!ModelState.IsValid) return BadRequest(@"Invalid options"); //no need due to global filter

            var fanId = User.Identity.GetUserId();
            if (fanId == dto.ArtistId) return BadRequest(@"Artist can't follow his/hers own self.");

            var followingVerdict = false;
            try
            {
                followingVerdict = await _artistsService.ToggleFollowingAsync(fanId, dto.ArtistId).ConfigureAwait(continueOnCapturedContext: false);
            }
            catch (ArtistFollowingTweakingFailedException) //unique key violation
            {
                followingVerdict = true;
            }

            return Ok(new FanFollowsArtistVerdictDto {Following = followingVerdict});
        }
    }
}
