﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using GigHub.Core.Foundation.ViewModels;
using GigHub.Core.ViaGlobalAsax.MVCSpecific.Identity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace GigHub.Core.Controllers
{
    [Authorize]
    public class ManageController : Controller //MvcBaseController <- better not
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public ApplicationSignInManager SignInManager
        {
            get => _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            private set => _signInManager = value;
        }

        public ApplicationUserManager UserManager
        {
            get => _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            private set => _userManager = value;
        }

        public ManageController()
        {
        }

        public ManageController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        [HttpGet]
        public async Task<ActionResult> Index(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
                : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
                : message == ManageMessageId.SetTwoFactorSuccess ? "Your two-factor authentication provider has been set."
                : message == ManageMessageId.Error ? "An error has occurred."
                : message == ManageMessageId.AddPhoneSuccess ? "Your phone number was added."
                : message == ManageMessageId.RemovePhoneSuccess ? "Your phone number was removed."
                : "";

            var userId = User.Identity.GetUserId();
            var model = new IndexViewModel
            {
                HasPassword = HasPassword(),
                PhoneNumber = await UserManager.GetPhoneNumberAsync(userId).ConfigureAwait(continueOnCapturedContext: false),
                TwoFactor = await UserManager.GetTwoFactorEnabledAsync(userId).ConfigureAwait(continueOnCapturedContext: false),
                Logins = await UserManager.GetLoginsAsync(userId).ConfigureAwait(continueOnCapturedContext: false),
                BrowserRemembered = await AuthenticationManager.TwoFactorBrowserRememberedAsync(userId).ConfigureAwait(continueOnCapturedContext: false)
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RemoveLogin(string loginProvider, string providerKey)
        {
            var result = await UserManager.RemoveLoginAsync(
                userId: User.Identity.GetUserId(),
                login: new UserLoginInfo(loginProvider: loginProvider, providerKey: providerKey)
            ).ConfigureAwait(continueOnCapturedContext: false);


            if (!result.Succeeded)
                return RedirectToAction("ManageLogins", new {Message = ManageMessageId.Error});

            var user = await UserManager
                .FindByIdAsync(User.Identity.GetUserId())
                .ConfigureAwait(continueOnCapturedContext: false);

            if (user != null)
            {
                await SignInManager.SignInAsync(
                    user: user,
                    isPersistent: false,
                    rememberBrowser: false
                ).ConfigureAwait(continueOnCapturedContext: false);
            }

            return RedirectToAction("ManageLogins", new {Message = ManageMessageId.RemoveLoginSuccess});
        }

        [HttpGet]
        public ActionResult AddPhoneNumber()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddPhoneNumber([Required] AddPhoneNumberViewModel model)
        {
            var code = await UserManager.GenerateChangePhoneNumberTokenAsync(User.Identity.GetUserId(), model.Number).ConfigureAwait(continueOnCapturedContext: false); // Generate the token and send it
            if (UserManager.SmsService == null)
                return RedirectToAction("VerifyPhoneNumber", new {PhoneNumber = model.Number});

            var message = new IdentityMessage
            {
                Destination = model.Number,
                Body = "Your security code is: " + code
            };

            await UserManager.SmsService.SendAsync(message).ConfigureAwait(continueOnCapturedContext: false);
            return RedirectToAction("VerifyPhoneNumber", new {PhoneNumber = model.Number});
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EnableTwoFactorAuthentication()
        {
            await UserManager.SetTwoFactorEnabledAsync(User.Identity.GetUserId(), true).ConfigureAwait(continueOnCapturedContext: false);

            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId()).ConfigureAwait(continueOnCapturedContext: false);
            if (user == null)
                return RedirectToAction("Index", "Manage");

            await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false).ConfigureAwait(continueOnCapturedContext: false);

            return RedirectToAction("Index", "Manage");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DisableTwoFactorAuthentication()
        {
            await UserManager.SetTwoFactorEnabledAsync(User.Identity.GetUserId(), false).ConfigureAwait(continueOnCapturedContext: false);

            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId()).ConfigureAwait(continueOnCapturedContext: false);
            if (user == null)
                return RedirectToAction("Index", "Manage");

            await SignInManager.SignInAsync(
                user: user,
                isPersistent: false,
                rememberBrowser: false
            ).ConfigureAwait(continueOnCapturedContext: false);

            return RedirectToAction("Index", "Manage");
        }

        [HttpGet]
        public async Task<ActionResult> VerifyPhoneNumber(string phoneNumber) // ReSharper disable once UnusedVariable
        {
            var code = await UserManager.GenerateChangePhoneNumberTokenAsync(User.Identity.GetUserId(), phoneNumber).ConfigureAwait(continueOnCapturedContext: false);

            return phoneNumber == null
                ? View("Error")
                : View(new VerifyPhoneNumberViewModel {PhoneNumber = phoneNumber});

            // Send an SMS through the SMS provider to verify the phone number
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyPhoneNumber([Required] VerifyPhoneNumberViewModel model)
        {
            var result = await UserManager.ChangePhoneNumberAsync(User.Identity.GetUserId(), model.PhoneNumber, model.Code).ConfigureAwait(continueOnCapturedContext: false);
            if (!result.Succeeded)
            {
                ModelState.AddModelError("", @"Failed to verify phone"); // If we got this far, something failed, redisplay form
                return View(model);
            }

            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId()).ConfigureAwait(continueOnCapturedContext: false);
            if (user == null)
                return RedirectToAction("Index", new {Message = ManageMessageId.AddPhoneSuccess});

            await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false).ConfigureAwait(continueOnCapturedContext: false);

            return RedirectToAction("Index", new {Message = ManageMessageId.AddPhoneSuccess});
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RemovePhoneNumber()
        {
            var result = await UserManager.SetPhoneNumberAsync(User.Identity.GetUserId(), null).ConfigureAwait(continueOnCapturedContext: false);
            if (!result.Succeeded)
                return RedirectToAction("Index", new {Message = ManageMessageId.Error});

            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId()).ConfigureAwait(continueOnCapturedContext: false);
            if (user == null)
                return RedirectToAction("Index", new {Message = ManageMessageId.RemovePhoneSuccess});

            await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false).ConfigureAwait(continueOnCapturedContext: false);

            return RedirectToAction("Index", new {Message = ManageMessageId.RemovePhoneSuccess});
        }

        [HttpGet]
        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword([Required] ChangePasswordViewModel model)
        {
            var result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword).ConfigureAwait(continueOnCapturedContext: false);
            if (!result.Succeeded)
            {
                AddErrors(result);
                return View(model);
            }

            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId()).ConfigureAwait(continueOnCapturedContext: false);
            if (user == null)
                return RedirectToAction("Index", new {Message = ManageMessageId.ChangePasswordSuccess});

            await SignInManager.SignInAsync(
                user,
                isPersistent: false,
                rememberBrowser: false
            ).ConfigureAwait(continueOnCapturedContext: false);

            return RedirectToAction("Index", new {Message = ManageMessageId.ChangePasswordSuccess});
        }

        [HttpGet]
        public ActionResult SetPassword()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SetPassword([Required] SetPasswordViewModel model)
        {
            var result = await UserManager.AddPasswordAsync(User.Identity.GetUserId(), model.NewPassword).ConfigureAwait(continueOnCapturedContext: false);
            if (!result.Succeeded)
            {
                AddErrors(result);
                return View(model);
            }

            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId()).ConfigureAwait(continueOnCapturedContext: false);
            if (user == null)
                return RedirectToAction("Index", new {Message = ManageMessageId.SetPasswordSuccess});

            await SignInManager.SignInAsync(
                user: user,
                isPersistent: false,
                rememberBrowser: false
            ).ConfigureAwait(continueOnCapturedContext: false);

            return RedirectToAction("Index", new {Message = ManageMessageId.SetPasswordSuccess});
        }

        public async Task<ActionResult> ManageLogins(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
                : message == ManageMessageId.Error ? "An error has occurred."
                : "";

            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId()).ConfigureAwait(continueOnCapturedContext: false);
            if (user == null)
                return View("Error");

            var userLogins = await UserManager.GetLoginsAsync(User.Identity.GetUserId()).ConfigureAwait(continueOnCapturedContext: false);
            var otherLogins = AuthenticationManager.GetExternalAuthenticationTypes().Where(auth => userLogins.All(ul => auth.AuthenticationType != ul.LoginProvider)).ToList();
            ViewBag.ShowRemoveButton = user.PasswordHash != null || userLogins.Count > 1;

            return View(new ManageLoginsViewModel
            {
                CurrentLogins = userLogins,
                OtherLogins = otherLogins
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LinkLogin(string provider) // post manage linklogin
        {
            return new AccountController.ChallengeResult( //0
                provider,
                Url.Action("LinkLoginCallback", "Manage"),
                User.Identity.GetUserId()
            );

            //0 request a redirect to the external login provider to link a login for the current user
        }

        public async Task<ActionResult> LinkLoginCallback() //get manage linklogincallback
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync(XsrfKey, User.Identity.GetUserId()).ConfigureAwait(continueOnCapturedContext: false);
            if (loginInfo == null)
            {
                return RedirectToAction("ManageLogins", new {Message = ManageMessageId.Error});
            }

            var result = await UserManager.AddLoginAsync(User.Identity.GetUserId(), loginInfo.Login).ConfigureAwait(continueOnCapturedContext: false);
            return result.Succeeded ? RedirectToAction("ManageLogins") : RedirectToAction("ManageLogins", new {Message = ManageMessageId.Error});
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }

            base.Dispose(disposing);
        }

        #region Helpers

        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager => HttpContext.GetOwinContext().Authentication;

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private bool HasPassword()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());

            return user?.PasswordHash != null;
        }

        //private bool HasPhoneNumber()
        //{
        //    var user = UserManager.FindById(User.Identity.GetUserId());
        //    if (user != null)
        //    {
        //        return user.PhoneNumber != null;
        //    }
        //    return false;
        //}

        public enum ManageMessageId
        {
            AddPhoneSuccess,
            ChangePasswordSuccess,
            SetTwoFactorSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            RemovePhoneSuccess,
            Error
        }

        #endregion
    }
}