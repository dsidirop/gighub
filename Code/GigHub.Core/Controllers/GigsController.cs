﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using GigHub.Core.Controllers.BaseControllers.Mvc;
using GigHub.Core.Foundation.ServicesX.Artists;
using GigHub.Core.Foundation.ServicesX.Gigs;
using GigHub.Core.Foundation.ServicesX.Gigs.Exceptions;
using GigHub.Core.Foundation.ViewModels;
using GigHub.Core.Utilities;
using Microsoft.AspNet.Identity;

namespace GigHub.Core.Controllers
{
    // http://stackoverflow.com/a/9495189/863651   notice that mvc controllers derive from the controller base class because they deal in views
    // while webapi2 controllers that live under the api folder derive from apicontroller base class because they deal in serialized json data
    public class GigsController : MvcBaseController
    {
        private readonly IGigsService _gigsService;
        private readonly IArtistsService _artistsService;

        public GigsController(
            IGigsService gigsService,
            IArtistsService artistsService
        )
        {
            _gigsService = gigsService;
            _artistsService = artistsService;
        }

        [HttpGet]
        public async Task<ActionResult> SingleGig(int gigId)
        {
            var userId = User.Identity.GetUserId();
            var isAuthenticated = User.Identity.IsAuthenticated;

            var gig = await _gigsService.GetGigAsync(
                gigId,
                includeGenre: true,
                includeArtist: true,
                includeAttendances: isAuthenticated,
                includeArtistFollowers: isAuthenticated
            ).ConfigureAwait(continueOnCapturedContext: false);
            if (gig == null)
                return new HttpStatusCodeResult(HttpStatusCode.NotFound, @"Gig not found"); //0

            return View("SingleGig", new SingleGigViewModel
            {
                Gig = gig,
                Attending = isAuthenticated && gig.Attendances.FirstOrDefault(x => x.AttendeeId == userId) != null,
                Following = isAuthenticated && gig.Artist.Followers.FirstOrDefault(x => x.FanId == userId) != null
            });

            //0 custom 404 pages should be easy in aspnet huh  noooope  http://benfoster.io/blog/aspnet-mvc-custom-error-pages   todo gh0020 
        }

        [HttpGet]
        [Authorize]
        public async Task<ActionResult> Attending(AttendingGigsListOptions options = null) //defaultview
        {
            var userId = User.Identity.GetUserId();
            var attendancesOfUser = await _gigsService.GetAttendancesOfUserAsync(userId, searchTerm: options?.Query).ConfigureAwait(continueOnCapturedContext: false);

            var gigsUserIsAttending = attendancesOfUser.Select(x => x.Gig);
            var idsOfArtistsHostingGigsUserIsAttending = gigsUserIsAttending.Select(x => x.ArtistId); //crucial

            var artistsFollowedByUser = (
                await _artistsService
                    .GetFollowingsAsync(userId, idsOfArtistsHostingGigsUserIsAttending)
                    .ConfigureAwait(continueOnCapturedContext: false)
            ).ToLookup(a => a.ArtistId);

            return View(UViews.GigsViewName, new AttendingViewModel
            {
                UserId = User.Identity.GetUserId(),
                Heading = "Gigs I'm Attending",
                UpcomingGigs = gigsUserIsAttending,
                AttendancesOfUser = attendancesOfUser.ToLookup(x => x.GigId),
                ArtistsFollowedByUser = artistsFollowedByUser,

                FilteringOptions = new AttendingViewModel.FilteringViaGigsListOptions
                {
                    SearchTerm = options?.Query
                }
            });
        }

        [Authorize] //defaultview
        public async Task<ActionResult> GigsStagedByMe(GigsStagedByMeOptions options = null)
        {
            var gigs = await _gigsService.GetGigsAsync(User.Identity.GetUserId(), options?.Query);

            return View(nameof(GigsStagedByMe), new GigsStagedByMeViewModel
            {
                Gigs = gigs,

                Filtering = new GigsStagedByMeViewModel.FilteringForGigsStagedByMeOptions
                {
                    SearchTerm = options?.Query
                }
            });
        }

        [Authorize]
        public async Task<ActionResult> ArtistsFollowedByUser()
        {
            var artists = await _artistsService.GetFollowingsAsync(User.Identity.GetUserId()).ConfigureAwait(continueOnCapturedContext: false);

            return View(nameof(ArtistsFollowedByUser), new FollowingsViewModel
            {
                Artists = artists.Select(x => x.Artist).ToList()
            });
        }

        [Authorize]
        public async Task<ActionResult> Edit(int gigId)
        {
            var gig = await _gigsService
                .GetGigAsync(gigId, includeGenre: true)
                .ConfigureAwait(continueOnCapturedContext: true); //0 must be true

            if (gig == null) return new HttpStatusCodeResult(HttpStatusCode.NotFound, "Gig not found");
            if (gig.ArtistId != User.Identity.GetUserId()) return new HttpUnauthorizedResult();

            var genres = await _gigsService
                .GetAllGenresAsync()
                .ConfigureAwait(continueOnCapturedContext: true); //0 must be true

            return View(GigFormCreateOrEditViewName, new GigFormViewModel
            {
                Id = gig.Id,
                Date = gig.DateTime.ToString(UDates.GigDateFormat),
                Time = gig.DateTime.ToString(UDates.GigTimeOfDayFormat),
                Venue = gig.Venue,
                Genres = genres,
                GenreId = gig.GenreId
            });

            //0 continueoncapturedcontext must be set to true otherwise the thread will probably not return into the captured context of the http
            //  call thus causing RequireJsOptions.Add to break completely
        }

        [Authorize] //0 mvc
        public async Task<ActionResult> Create() => View(GigFormCreateOrEditViewName, new GigFormViewModel
        {
            Genres = await _gigsService.GetAllGenresAsync().ConfigureAwait(continueOnCapturedContext: false)

            //0 if the user is not logged in while trying to access http://localhost:54666/gigs/create then he will automagically get redirected to http://localhost:54666/Account/Login?ReturnUrl=%2FGigs%2FCreate
        });

        [Authorize]
        [HttpPost] //notice this is a webapi-ish method so dont confuse this method with the mvc flavor of create above
        [ValidateAntiForgeryToken] //this will crosscheck the validation token embedded in the webpage in the input[type=hidden] textbox
        public async Task<ActionResult> Upsert([Required] GigFormViewModel model) //invoked called by gigformcreateoredit.cshtml
        {
            try
            {
                await _gigsService.UpsertGig(
                    gigId: model.Id,
                    venue: model.Venue,
                    genreId: model.GenreId,
                    artistId: User.Identity.GetUserId(),
                    dateTime: model.GetDateTime()
                );
            }
            catch (GigNotFoundException)
            {
                model.Genres = await _gigsService.GetAllGenresAsync().ConfigureAwait(continueOnCapturedContext: false);
                model.PossibleError = @"Gig no longer exists (did it get deleted behind your back?)";
                return View(GigFormCreateOrEditViewName, model);
            }
            catch (GigBelongsToDifferentArtistException)
            {
                model.Genres = await _gigsService.GetAllGenresAsync().ConfigureAwait(continueOnCapturedContext: false);
                model.PossibleError = @"You aren't the owner of the Gig you are trying to update";
                return View(GigFormCreateOrEditViewName, model);
            }
            
            return RedirectToAction(nameof(GigsStagedByMe), UMvc.Ctrler<GigsController>());

            //0 in case any of the values supplied are found to be invalid we need to supply the genres list as we do in the simple create method because
            //  we will be redirecting the user back to the create gig webpage
        }

        static private string _gigFormCreateOrEditViewName;
        private string GigFormCreateOrEditViewName =>
            _gigFormCreateOrEditViewName = _gigFormCreateOrEditViewName ?? UrlHelper.GenerateContentUrl(@"~/Views/Gigs/GigFormCreateOrEdit.cshtml", ControllerContext.HttpContext); //0
        //0 always prefer controllercontext.httpcontext over request.requestcontext.httpcontext   using the later is considered to be bad practice and
        //  it also makes it hard for this controller to get unit tested properly
    }
}