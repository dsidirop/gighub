﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using GigHub.Core.Controllers.BaseControllers.Mvc;
using GigHub.Core.Foundation.ServicesX.Artists;
using GigHub.Core.Foundation.ServicesX.Gigs;
using GigHub.Core.Foundation.ViewModels;
using GigHub.Core.Utilities;
using Microsoft.AspNet.Identity;

namespace GigHub.Core.Controllers
{
    public class HomeController : MvcBaseController //default controller targeted by routeconfig
    {
        private readonly IGigsService _gigsService;
        private readonly IArtistsService _artistsService;

        public HomeController(IGigsService gigsService, IArtistsService artistsService)
        {
            _gigsService = gigsService;
            _artistsService = artistsService;
        }

        [Route("")] // [Authorize] //0
        public async Task<ActionResult> GigsList(HomeGigsListOptions options = null) //defaultview
        {
            // ViewBag.Message = "Your application description page."; //0 avoid viewbags

            var userId = User.Identity.GetUserId();

            var upcomingGigs = await _gigsService.GetGigsAsync(
                searchTerm: options?.Query,
                onlyFutureOnesNotAll: true
            ).ConfigureAwait(continueOnCapturedContext: false);

            var attendancesOfUser = (
                await _gigsService.GetAttendancesOfUserAsync(
                    userId: userId,
                    gigIds: upcomingGigs.Select(x => x.Id)
                ).ConfigureAwait(continueOnCapturedContext: false)
            ).ToLookup(a => a.GigId);

            var artistsFollowedByUser = (
                await _artistsService.GetFollowingsAsync(
                    userId: userId,
                    idsOfArtistsHostingGigsUserIsAttending: upcomingGigs.Select(x => x.ArtistId)
                ).ConfigureAwait(continueOnCapturedContext: false)
            ).ToLookup(a => a.ArtistId);

            return View(UViews.GigsViewName, new HomeViewModel
            {
                UserId = User.Identity.GetUserId(),
                Heading = "Upcoming Gigs",
                UpcomingGigs = upcomingGigs,
                AttendancesOfUser = attendancesOfUser,
                ArtistsFollowedByUser = artistsFollowedByUser,

                FilteringOptions = new HomeViewModel.FilteringViaGigsListOptions
                {
                    SearchTerm = options?.Query
                }

                //0 ViewBags are just dictionaries of dynamically typed objects so we lose compile time checking and the ability to refactor with the helper of r# and so on
                //  For bonus points making extensive use of the viewbag also misses the point of using the mvc pattern  I get the impression viewbags were created to solve
                //  an edgecase problem in aspnet and people use them instead of creating view models as was originally intended in the design of the platform to the detriment
                //  of their work
                //1 We want upcoming gigs to be visible just fine in the landing page even if the user is not logged in
                //2 Performing a case insensitive search here is harder than one would imagine  http://stackoverflow.com/questions/3360772/linq-contains-case-insensitive
            });
        }
    }
}
