﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GigHub.Core.Foundation.Models;
using GigHub.Core.Foundation.Persistence;
using GigHub.Core.Foundation.ServicesX.Artists;
using GigHub.Core.Foundation.ServicesX.Artists.Exceptions;
using GigHub.Core.Foundation.ServicesX.Gigs.Exceptions;

namespace GigHub.Core.Services
{
    public class ArtistsService : IArtistsService
    {
        private readonly IUnitOfWork _unitOfWork;

        public ArtistsService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<bool> ToggleFollowingAsync(string fanId, string artistId)
        {
            var followingVerdict = false;
            var fanFollowingArtist = await _unitOfWork.ArtistsFollowingsRepo.GetSingleAsync(fanId, artistId).ConfigureAwait(continueOnCapturedContext: false);
            try
            {
                if (fanFollowingArtist == null)
                {
                    _unitOfWork.ArtistsFollowingsRepo.Add(fanId, artistId); //order
                    followingVerdict = true; //order
                }
                else
                {
                    _unitOfWork.ArtistsFollowingsRepo.Delete(fanFollowingArtist);
                }

                await _unitOfWork.CommitChangesAsync().ConfigureAwait(continueOnCapturedContext: false);
            }
            catch
            {
                throw new ArtistFollowingTweakingFailedException();
            }

            return followingVerdict;
        }

        public async Task<IEnumerable<FanFollowingArtist>> GetFollowingsAsync(string userId, IEnumerable<string> idsOfArtistsHostingGigsUserIsAttending = null)
        {
            return await _unitOfWork.ArtistsFollowingsRepo.GetMultipleAsync(userId, idsOfArtistsHostingGigsUserIsAttending).ConfigureAwait(continueOnCapturedContext: false);
        }
    }
}