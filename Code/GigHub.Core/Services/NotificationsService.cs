﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GigHub.Core.Foundation.Models;
using GigHub.Core.Foundation.Persistence;
using GigHub.Core.Foundation.ServicesX.Gigs;
using GigHub.Core.Foundation.ServicesX.Gigs.Exceptions;
using GigHub.Core.Foundation.ServicesX.Notifications;
using GigHub.Core.Foundation.ServicesX.Notifications.Exceptions;
using GigHub.Core.Utilities;

namespace GigHub.Core.Services
{
    public class NotificationsService : INotificationsService
    {
        private readonly IUnitOfWork _unitOfWork;

        public NotificationsService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task MarkNotificationsAsReadAsync(string userId, IEnumerable<int> notificationsIds = null)
        {
            var notifs = await GetUnreadNotificationsAsync(userId, notificationsIds).ConfigureAwait(continueOnCapturedContext: false);
            if (notifs.Any(n => n.UserId != userId)) throw new NotificationBelongsToDifferentArtistException();

            notifs.Pipe(x => x.IsRead = true).Run();
            await _unitOfWork.CommitChangesAsync().ConfigureAwait(continueOnCapturedContext: false);
        }

        public async Task<IEnumerable<UserHasReadNotification>> GetUnreadNotificationsAsync(string userId, IEnumerable<int> notificationsIds = null)
        {
            return await _unitOfWork.UserHasReadNotificationsRepo.GetUnreadNotificationsAsync(userId, notificationsIds).ConfigureAwait(continueOnCapturedContext: false);
        }
    }
}