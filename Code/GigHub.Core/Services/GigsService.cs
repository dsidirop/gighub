﻿using GigHub.Core.Foundation.Models;
using GigHub.Core.Foundation.Persistence;
using GigHub.Core.Foundation.ServicesX.Gigs;
using GigHub.Core.Foundation.ServicesX.Gigs.Exceptions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GigHub.Core.Services
{
    public class GigsService : IGigsService
    {
        private readonly IUnitOfWork _unitOfWork;

        public GigsService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task SetGigCancelStatusAsync(string userId, int gigId, bool cancelNotUncancel)
        {
            var gig = await _unitOfWork
                .GigsRepo
                .GetSingleAsync(
                    gigId: gigId,
                    includeAttendances: true,
                    includeAttendancesAttendees: true
                )
                .ConfigureAwait(continueOnCapturedContext: false);

            if (gig == null) throw new GigNotFoundException();
            if (gig.ArtistId != userId) throw new GigBelongsToDifferentArtistException();

            if (gig.GotCancelled == cancelNotUncancel)
                return;

            gig.SetCancelStatus(cancelNotUncancel);
            await _unitOfWork.CommitChangesAsync().ConfigureAwait(continueOnCapturedContext: false);
        }

        public async Task<bool> ToggleGigAttendanceForUserAsync(string userId, int gigId)
        {
            var gig = await _unitOfWork.GigsRepo.GetSingleAsync(gigId, includeAttendances: true, includeAttendancesAttendees: true).ConfigureAwait(continueOnCapturedContext: false);
            if (gig == null) throw new GigNotFoundException();
            if (userId == gig.ArtistId) throw new GigCannotBeAttendedByOwnerArtistHimselfException();

            // if (gig == null || artistId == gig.ArtistId) return BadRequest(gig == null ? "Gig doesn't exist" : "An Artist cannot attend his own Gig as a fan");

            var attendance = await _unitOfWork.AttendancesRepo.GetSingleAsync(userId, gigId).ConfigureAwait(continueOnCapturedContext: false);
            var goingVerdict = false;
            try
            {
                if (attendance == null) //toggle
                {
                    _unitOfWork.AttendancesRepo.Add(userId, gigId);
                    goingVerdict = true;
                }
                else
                {
                    _unitOfWork.AttendancesRepo.Delete(attendance);
                }

                await _unitOfWork.CommitChangesAsync().ConfigureAwait(continueOnCapturedContext: false);
            }
            catch
            {
                throw new GigAttendanceTweakingFailedException();
            }

            return goingVerdict;
        }

        public async Task<IEnumerable<Genre>> GetAllGenresAsync()
        {
            return await _unitOfWork.GenresRepo.GetAllAsync().ConfigureAwait(continueOnCapturedContext: false);
        }

        public async Task<IEnumerable<Attendance>> GetAttendancesOfUserAsync(string userId, string searchTerm = null, IEnumerable<int> gigIds = null)
        {
            return await _unitOfWork.AttendancesRepo.GetAllMatchingAsync(userId, searchTerm: searchTerm).ConfigureAwait(continueOnCapturedContext: false);
        }

        public async Task<bool> UpsertGig(int gigId, string venue, byte genreId, string artistId, DateTime dateTime)
        {
            if (gigId == 0) //0 purecreate
            {
                _unitOfWork.GigsRepo.Add(new Gig { Venue = venue, GenreId = genreId, ArtistId = artistId, DateTime = dateTime });
                await _unitOfWork.CommitChangesAsync().ConfigureAwait(continueOnCapturedContext: false);
                return true;
            }

            var preexistingGig = await GetGigAsync(gigId, includeGenre: true, includeAttendances: true, includeAttendancesAttendees: true).ConfigureAwait(continueOnCapturedContext: false);
            if (preexistingGig == null)
                throw new GigNotFoundException();

            if (preexistingGig.ArtistId != artistId)
                throw new GigBelongsToDifferentArtistException();
            
            var newGenre = await _unitOfWork.GenresRepo.GetAsync(genreId).ConfigureAwait(continueOnCapturedContext: false);
            if (!preexistingGig.TryUpdate(venue, dateTime, newGenre))
                return false;

            // _unitOfWork.GigsRepo.AddOrUpdate(preexistingGig); //noneed

            await _unitOfWork.CommitChangesAsync().ConfigureAwait(continueOnCapturedContext: false);
            return true;

            //0 the autoincremented gigids start from one   we can thus use zero to differentiate between insert and update operations
        }

        public async Task<Gig> GetGigAsync(int gigId, bool includeArtist = false, bool includeGenre = false, bool includeAttendances = false, bool includeArtistFollowers = false, bool includeAttendancesAttendees = false)
        {
            return await _unitOfWork.GigsRepo.GetSingleAsync(
                gigId,
                includeGenre: includeGenre,
                includeArtist: includeArtist,
                includeAttendances: includeAttendances,
                includeArtistFollowers: includeArtistFollowers,
                includeAttendancesAttendees: includeAttendancesAttendees
            ).ConfigureAwait(continueOnCapturedContext: false);
        }

        public async Task<IEnumerable<Gig>> GetGigsAsync(string artistId = null, string searchTerm = null, bool onlyFutureOnesNotAll = false, bool includeCancelled = true)
        {
            return await _unitOfWork.GigsRepo.GetMultipleAsync(
                artistId: artistId,
                searchTerm: searchTerm,
                includeCancelled: includeCancelled,
                onlyFutureOnesNotAll: onlyFutureOnesNotAll
            ).ConfigureAwait(continueOnCapturedContext: false);
        }
    }
}