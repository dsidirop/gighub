@ECHO off

REM   npm-to-yarn   cheatsheet  https://gist.github.com/jonlabelle/c082700c1c249d986faecbd5abf7d65b

SETLOCAL
CALL :find_dp0
SET "_prog=%dp0%\..\packages\Nodejs.Redist.x64.12.16.1\tools\node.exe"
"%_prog%"  "%dp0%\..\packages\Yarnpkg.Yarn.1.22.4\content\bin\yarn.js" %*
ENDLOCAL



EXIT /b %errorlevel%
:find_dp0
SET dp0=%~dp0
EXIT /b
