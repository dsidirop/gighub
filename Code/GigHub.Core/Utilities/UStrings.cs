﻿using System;
using System.Text.RegularExpressions;

//warning to future maintainers    the class u and any of its methods used in cshtml files and the like need
//warning to future maintainers    to be public so that they will be usable by the aspnet infrastructure
namespace GigHub.Core.Utilities
{
    static public class UStrings
    {
        static public bool ContainsCI(this string source, string toCheck) => source.ContainsX(toCheck, StringComparison.InvariantCultureIgnoreCase);
        static public bool ContainsX(this string source, string toCheck, StringComparison comp) => source.IndexOf(toCheck, comp) >= 0;
        static public string ReplaceCI(this string source, string toReplace, string replacement) => new Regex(Regex.Escape(toReplace), RegexOptions.IgnoreCase).Replace(source, replacement);
    }
}