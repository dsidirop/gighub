﻿using System.Net;

namespace GigHub.Core.Utilities.DataAnnotationAttributes
{
    //note to self   aspnetcore1 introduced a completely new authorization system thusly doing away with the mess of
    //note to self   system.web.http.authorizeattribute vs system.web.mvc.authorize  in aspnetcore1 the new consolidated
    //note to self   authorize attribute can be found under microsoft.aspnetcore.authorization 
    sealed public class DemandAuthorizedWithHotfixForUnauthorizedAjaxCalls_Mvc : System.Web.Mvc.AuthorizeAttribute //reserved for future usage
    {
        protected override void HandleUnauthorizedRequest(System.Web.Mvc.AuthorizationContext filterContext)
        {
            var isAjaxRequest = System.Web.Mvc.AjaxRequestExtensions.IsAjaxRequest(filterContext.RequestContext.HttpContext.Request);
            if (isAjaxRequest) //0 we handle unauthorized ajaxcalls in a special manner
            {
                filterContext.Result = new HttpUnauthorizedResult();
                return;
            }
            
            base.HandleUnauthorizedRequest(filterContext);
        }
        //0 by default unauthorized ajax requests dont return a 403forbidden error and it is up to the ajax error handler to
        //  behave appropriately    we prefer enforcing a more clearcut 401unauthorized response over 403forbidden because 403
        //  is used for other purposes   no idea why redmond devs went with 403 in the first place

        sealed private class HttpUnauthorizedResult : System.Web.Mvc.ActionResult
        {
            public override void ExecuteResult(System.Web.Mvc.ControllerContext context)
            {
                context.HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                context.HttpContext.Response.Write("Unauthorized");
                context.HttpContext.Response.End(); //0 vital
            }
        }
        //0 ending the response here and now is vital in order for the receiving end to actually receive an unauthorized response   if this statement is
        //  omitted then th receiving end gets an ok response which is weird
    }
}