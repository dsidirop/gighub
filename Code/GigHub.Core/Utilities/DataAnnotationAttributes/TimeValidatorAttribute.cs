﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;

namespace GigHub.Core.Utilities.DataAnnotationAttributes
{
    //todo   [TimeValidator]   is supposed to be used gigformviewmodel however this attribute
    //todo   doesnt work as intended so in due time we need to address this via ticket gh0003
    sealed public class TimeValidatorAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            var result = DateTime.MinValue;
            return DateTime.TryParseExact(value as string, "HH:MM", CultureInfo.InvariantCulture, DateTimeStyles.None, out result);
        }
    }
}