﻿namespace GigHub.Core.Utilities.DataAnnotationAttributes
{
    sealed public class AjaxOnlyMethod_Mvc : System.Web.Mvc.ActionMethodSelectorAttribute //0
    {
        public override bool IsValidForRequest(System.Web.Mvc.ControllerContext controllerContext, System.Reflection.MethodInfo methodInfo)
        {
            return System.Web.Mvc.AjaxRequestExtensions.IsAjaxRequest(controllerContext.RequestContext.HttpContext.Request);
        }
    }
    //0 currently unused but reserved for future reference
}