﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;

namespace GigHub.Core.Utilities.DataAnnotationAttributes
{
    public class FutureDateValidatorAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            var result = DateTime.MinValue;
            return DateTime.TryParseExact(value as string, UDates.GigDateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out result) && result > DateTime.Now;
        }
    }
}