﻿using System;

namespace GigHub.Core.Utilities.DataAnnotationAttributes
{
    sealed public class AjaxOnlyController_Webapi : System.Web.Http.Filters.AuthorizationFilterAttribute
    {
        public override void OnAuthorization(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            if (!IsAjaxRequest())
            {
                actionContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.NotFound);
            }

            base.OnAuthorization(actionContext);
        }

        static private bool IsAjaxRequest()
        {
            var request = System.Web.HttpContext.Current.Request;
            if (request == null) throw new ArgumentNullException(nameof(request));
            if (request["X-Requested-With"] == "XMLHttpRequest") return true;

            // ReSharper disable once ConditionIsAlwaysTrueOrFalse
            return request.Headers != null && request.Headers["X-Requested-With"] == "XMLHttpRequest";
        }
    }
}