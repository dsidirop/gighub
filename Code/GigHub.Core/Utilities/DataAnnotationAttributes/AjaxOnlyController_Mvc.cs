﻿using System;

namespace GigHub.Core.Utilities.DataAnnotationAttributes
{
    sealed public class AjaxOnlyController_Mvc : System.Web.Mvc.AuthorizeAttribute // ReSharper disable InvokeAsExtensionMethod
    {
        public override void OnAuthorization(System.Web.Mvc.AuthorizationContext filterContext) //0
        {
            if (filterContext == null) throw new ArgumentNullException(nameof(filterContext));

            var isAjaxRequest = System.Web.Mvc.AjaxRequestExtensions.IsAjaxRequest(filterContext.RequestContext.HttpContext.Request);
            if (!isAjaxRequest)
            {
                filterContext.Result = new System.Web.Mvc.HttpNotFoundResult();
                return;
            }

            base.OnAuthorization(filterContext);
        }
        //0 currently unused but reserved for future reference
    }
}