﻿// ReSharper disable InconsistentNaming
// ReSharper disable ConvertToConstant.Global

using System;
using System.Reflection;

//warning to future maintainers    the class u and any of its methods used in cshtml files and the like need
//warning to future maintainers    to be public so that they will be usable by the aspnet infrastructure
namespace GigHub.Core.Utilities
{
    static public class UEnv
    {
        static public readonly string nl = Environment.NewLine;
        static public readonly string nl2 = nl + nl;

        static public readonly string PlatformVersion = Assembly
            .GetExecutingAssembly()
            .GetName()
            .Version
            .ToString();

#if DEBUG
        static public readonly bool IsDebugModeOn = true; //keep static and turn this to const     if we use const the ide will nag us about the value always being true
#else
        public const bool IsDebugModeOn = false; //keep const as an optimization
#endif
    }
}