﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace GigHub.Core.Utilities
{
    static public class UMvcModelState
    {
        static public ModelStateDictionary AddModelErrorX(this ModelStateDictionary modelState, string key, string errorMessage)
        {
            modelState.AddModelError(key, errorMessage);
            return modelState;
        }

        static public string Stringify(this System.Web.Http.ModelBinding.ModelStateDictionary modelState)
        {
            var errorsListPerModelField = modelState.ToErrorsListPerModelField();
            return string.Join(UEnv.nl2, errorsListPerModelField.Select(x => $"- {x.Key}:{UEnv.nl}{string.Join(UEnv.nl, x.Value.Select(y => $"   - {y}"))}").ToArray());
        }

        static public IEnumerable<KeyValuePair<string, string[]>> ToErrorsListPerModelField(this System.Web.Http.ModelBinding.ModelStateDictionary modelState)
        {
            return modelState.ToDictionary(
                kvp => kvp.Key,
                kvp => kvp.Value.Errors.Select(err => !string.IsNullOrWhiteSpace(err.ErrorMessage) ? err.ErrorMessage : err.Exception.ToString()).ToArray()
            ).Where(m => m.Value.Any());
        }

        static public string Stringify(this ModelStateDictionary modelState, bool applyComplexFormatting = true)
        {
            var errorsListPerModelField = modelState.ToErrorsListPerModelField();

            return applyComplexFormatting
                ? string.Join(UEnv.nl2, errorsListPerModelField.Select(x => $"- {x.Key}:{UEnv.nl}{string.Join(UEnv.nl, x.Value.Select(y => $"   - {y}"))}").ToArray())
                : string.Join(UEnv.nl, errorsListPerModelField.SelectMany(x => x.Value).ToArray());
        }

        static public IEnumerable<KeyValuePair<string, string[]>> ToErrorsListPerModelField(this ModelStateDictionary modelState)
        {
            return modelState.ToDictionary(
                kvp => kvp.Key,
                kvp => kvp.Value.Errors.Select(err => !string.IsNullOrWhiteSpace(err.ErrorMessage) ? err.ErrorMessage : err.Exception.ToString()).ToArray()
            ).Where(m => m.Value.Any());
        }
    }
}