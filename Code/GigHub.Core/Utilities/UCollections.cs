﻿using System;
using System.Collections.Generic;

//warning to future maintainers    the class u and any of its methods used in cshtml files and the like need
//warning to future maintainers    to be public so that they will be usable by the aspnet infrastructure
namespace GigHub.Core.Utilities
{
    static public class UCollections
    {
        static public IEnumerable<T> Pipe<T>(this IEnumerable<T> itemsStream, Action<T> action)
        {
            foreach (var obj in itemsStream)
            {
                action(obj);
                yield return obj;
            }
        }

        static public void Run<T>(this IEnumerable<T> itemsStream)
        {
            foreach (var _ in itemsStream)
            {
                //dont use yield return here   it will break things
            }
        }
    }
}