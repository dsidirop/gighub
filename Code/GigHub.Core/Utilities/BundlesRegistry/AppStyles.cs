﻿namespace GigHub.Core.Utilities.BundlesRegistry
{
    static public class AppStyles
    {
        public const string Rockbed = @"~/content/rockbedcss";
        public const string CreateOrEdit = @"~/content/createoredit";
    }
}