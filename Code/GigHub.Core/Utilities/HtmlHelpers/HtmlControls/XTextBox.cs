﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using GigHub.Core.Utilities.HtmlHelpers.Attributes;

namespace GigHub.Core.Utilities.HtmlHelpers.HtmlControls
{
    static public class TextBoxExtensions
    {
        static public MvcHtmlString XTextBoxFor<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            object value = null,
            string format = null,
            object htmlAttributes = null)
        {
            var htmlAttributesRefined = HtmlHelper
                .AnonymousObjectToHtmlAttributes(htmlAttributes)
                .AddIfMissing("id", expression.GrabMemberInfo().Name)
                .AddIfMissing("type", "text");

            var modelMetadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);

            var name = htmlAttributesRefined.GetValueOrDefault("name") as string //0 order  todo  enrich cascade scanning for the attribute
                       ??
                       modelMetadata //order
                           ?.GetRealModelType()
                           .GetCustomAttributes(typeof(WithHtmlNameAttribute), inherit: true)
                           .Cast<WithHtmlNameAttribute>()
                           .FirstOrDefault()
                           ?.Name
                       ??
                       expression //order
                           .TryGrabAttribute<TModel, TProperty, WithHtmlNameAttribute>()
                           ?.Name;

            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentException($"[TBFE.XTBFF01] [BUG] No explicit '{nameof(name)}' specified and the property '{expression.Body}' of '{htmlHelper.ViewData.Model.GetType().Name}' isn't decorated with the {nameof(WithHtmlNameAttribute)} attribute!");

            return htmlHelper.TextBox(
                name: name,
                value: value ?? modelMetadata?.Model,
                format: format,
                htmlAttributes: htmlAttributesRefined
            );
        }

        //0 TODO   IMPLEMENT CASCADE SCANNING FOR TATTRIBUTE LIKE SHOWN BELOW
        //
        //  https://stackoverflow.com/a/4602535/863651     convert '(IFoo x) => x.y.z.t' into '(ConcreteFoo x) => x.y.z.t' without the t in the end
        //
        //  alternative approach
        //
        //  https://stackoverflow.com/a/22135756/863651    get number of components in the expression!
        //  https://stackoverflow.com/a/11153937/863651    grab the property/method infos + values in a chain and then analyze the last two parts of it!
        //
        //
        //
        //
        // public class ConcreteFoo : AFoo<ConcreteBar>
        // {
        //       [PingAttribute("1")]
        //       public IBar(3) Bar { get; set; } = new MoreConcreteBar(2)();
        // }
        //
        // [PingAttribute("2")]
        // public class MoreConcreteBar(2) : ConcreteBar
        // {
        // }
        //
        // [PingAttribute("3")]
        // public class ConcreteBar(3) : ABar
        // {
        // }
        //
        //
        // ------------------
        //
        //
        // public abstract AFoo<T> : IFoo   where T : ABar
        // {
        //       [PingAttribute("4")]
        //       public T Bar { get; set; }
        // }
        //
        // [PingAttribute("5")]
        // abstract public class ABar : IBar
        // {
        // }
        //
        //
        // ------------------
        //
        //
        // public abstract IFoo
        // {
        //       public IBar Bar { get; set; }
        // }
        //
        // public abstract IBar
        // {
        // }
    }

    static public class FooX
    {
        static public Type GetRealModelType(this ModelMetadata modelMetadata) //inspired by the internal method ModelMetadata.RealModelType
        {
            return modelMetadata.Model == null || IsNullableValueType(modelMetadata.ModelType)
                ? modelMetadata.ModelType
                : modelMetadata.Model.GetType();
        }

        static public bool IsNullableValueType(Type type) => Nullable.GetUnderlyingType(type) != null;
    }
}