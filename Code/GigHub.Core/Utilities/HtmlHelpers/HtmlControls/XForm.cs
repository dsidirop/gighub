﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using GigHub.Core.Utilities.HtmlHelpers.Attributes;

namespace GigHub.Core.Utilities.HtmlHelpers.HtmlControls
{
    static public class FormExtensions
    {
        static public MvcForm XBeginForm<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            object htmlAttributes = null
        )
        {
            var options = ModelMetadata
                .FromLambdaExpression(expression, htmlHelper.ViewData)
                ?.Model;

            if (options == null)
                throw new ArgumentException($"[FE.XBF00] [BUG] '{expression.Body}' is null!");

            var optionsType = options.GetType();

            var method = optionsType //order
                             .GetCustomAttributes(typeof(FormSubmitMethodAttribute), inherit: true)
                             .Cast<FormSubmitMethodAttribute>()
                             .FirstOrDefault()
                             ?.Method
                         ?? expression //order
                             .TryGrabAttribute<TModel, TProperty, FormSubmitMethodAttribute>()
                             ?.Method
                         ?? FormMethod.Post;

            var action = (
                optionsType
                    .GetCustomAttributes(typeof(FormSubmitActionAttribute), inherit: true)
                    .Cast<FormSubmitActionAttribute>()
                    .FirstOrDefault()
                ??
                expression
                    .TryGrabAttribute<TModel, TProperty, FormSubmitActionAttribute>()
            )?.Action;

            var controller = (
                optionsType
                    .GetCustomAttributes(typeof(FormSubmitControllerAttribute), inherit: true)
                    .Cast<FormSubmitControllerAttribute>()
                    .FirstOrDefault()
                ??
                expression
                    .TryGrabAttribute<TModel, TProperty, FormSubmitControllerAttribute>()
            )?.Controller;

            //if (method == null) throw new ArgumentException(); //dont  noneed

            if (string.IsNullOrWhiteSpace(action))
                throw new ArgumentException($"[FE.XBF01] [BUG] '{expression.Body}' isn't decorated with the {nameof(FormSubmitActionAttribute)} attribute like it's supposed to!");

            if (string.IsNullOrWhiteSpace(controller))
                throw new ArgumentException($"[FE.XBF02] [BUG] '{expression.Body}' isn't decorated with the {nameof(FormSubmitControllerAttribute)} attribute like it's supposed to!");

            var htmlAttributesRefined = HtmlHelper
                .AnonymousObjectToHtmlAttributes(htmlAttributes)
                .AddIfMissing("rel", "noopener")
                .AddIfMissing("role", "form");

            return htmlHelper.BeginForm(
                method: method,
                actionName: action,
                controllerName: controller,
                htmlAttributes: htmlAttributesRefined
            );
        }
    }
}