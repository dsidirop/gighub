﻿using System;
using System.Net;

// ReSharper disable UnreachableCode
// ReSharper disable ConditionIsAlwaysTrueOrFalse

namespace GigHub.Core.Utilities.HtmlHelpers
{
    static public class RenderHelper //0
    {
        static private readonly string RenderAppVersionString = $"v={WebUtility.HtmlEncode(UEnv.PlatformVersion)}";

        static public string GetRenderAppVersion()
        {
            return UEnv.IsDebugModeOn
                ? $"v={WebUtility.HtmlEncode(Guid.NewGuid().ToString())}"
                : RenderAppVersionString;
        }

        //0 http://requirejsnet.veritech.io/setup.html
    }
}