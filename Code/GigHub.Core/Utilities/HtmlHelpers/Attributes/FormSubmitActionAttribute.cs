﻿using System;

namespace GigHub.Core.Utilities.HtmlHelpers.Attributes
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Class)]
    public class FormSubmitActionAttribute : Attribute //employed by XBeginForm and friends
    {
        public string Action { get; }

        public FormSubmitActionAttribute(string action)
        {
            if (string.IsNullOrWhiteSpace(action))
                throw new ArgumentException(action);

            Action = action.Trim();
        }
    }
}