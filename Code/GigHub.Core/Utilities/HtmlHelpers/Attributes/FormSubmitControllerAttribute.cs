﻿using System;

namespace GigHub.Core.Utilities.HtmlHelpers.Attributes
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Class)]
    public class FormSubmitControllerAttribute : Attribute //employed by XBeginForm and friends
    {
        public string Controller { get; }

        public FormSubmitControllerAttribute(string controller)
        {
            if (string.IsNullOrWhiteSpace(controller))
                throw new ArgumentException(controller);

            Controller = controller
                .Trim()
                .RemoveControllerPostfix();
        }
    }
}