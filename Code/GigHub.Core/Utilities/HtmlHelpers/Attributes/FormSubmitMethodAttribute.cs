﻿using System;
using System.Web.Mvc;

namespace GigHub.Core.Utilities.HtmlHelpers.Attributes
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Class)]
    public class FormSubmitMethodAttribute : Attribute //employed by XBeginForm and friends
    {
        public FormMethod Method { get; }

        public FormSubmitMethodAttribute(FormMethod method)
        {
            Method = method;
        }
    }
}