﻿using System;

namespace GigHub.Core.Utilities.HtmlHelpers.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class WithHtmlNameAttribute : Attribute //employed by XTextBoxFor and friends
    {
        public string Name { get; }

        public WithHtmlNameAttribute(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentException(name);

            Name = name.Trim();
        }
    }
}