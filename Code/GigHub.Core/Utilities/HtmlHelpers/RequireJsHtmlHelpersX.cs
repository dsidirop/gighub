﻿using System.Linq;
using System.Threading;
using System.Web.Mvc;
using RequireJsNet;
using RequireJsNet.Configuration;
using RequireJsNet.Models;

namespace GigHub.Core.Utilities.HtmlHelpers
{
    static public class RequireJsHtmlHelpersX
    {
        static public string GetRequireJsStandardSetupHtml(this HtmlHelper htmlHelper, UrlHelper urlHelper)
            => htmlHelper.RenderRequireJsSetup(new RequireRendererConfiguration // http://requirejsnet.veritech.io/setup.html
                {
                    Logger = null, //1
                    UrlArgs = RenderHelper.GetRenderAppVersion(), //2
                    BaseUrl = urlHelper.Content(@"~/Scripts/"), //3 dont turn this to static readonly
                    RequireJsUrl = urlHelper.Content(@"~/Scripts/ThirdParty/requirejs/require.js"), //4 dont turn this to static readonly
                    LoadOverrides = false, //5  DONT ENABLE THIS IN PRODUCTION   TOO BUGGY!
                    ProcessConfig = ProcessConfigCallback,
                    EntryPointRoot = EntryPointRoot, //6
                    LocaleSelector = html => Thread.CurrentThread?.CurrentUICulture?.Name?.Split('-').FirstOrDefault() ?? @"en", //7
                    ProcessOptions = ProcessOptionsCallback, //8
                    CacheConfigObject = true, //10
                    ConfigurationFiles = ConfigurationFiles, //9
                    ConfigCachingPolicy = ConfigCachingPolicy.Permanent //10

                    // 1   instance of irequirejslogger
                    // 2   elegant cache breaking
                    // 3   root folder for your js controllers will be used for composing paths to entrypoint
                    // 4   baseurl to be passed to requirejs will be used when composing urls for scripts
                    // 5   whether we should load overrides or not used for autobundles disabled on debug mode
                    // 6   extensability point for the options object
                    // 7   compute the value you want locale to have used for i18n
                    // 8   value for urlargs to be passed to requirejs   used for versioning
                    // 9   a list of all the configuration files you want to load
                    //10   store config in aspnet cache to avoid IO operations at each request
                })
                .ToString();

        static private void ProcessConfigCallback(JsonRequireOutput config)
        {
            //nothing to do yet    if we ever employ this we should store the
            //config in the aspnet cache to avoid io operations at each request
        }

        static private void ProcessOptionsCallback(JsonRequireOptions options)
        {
            //nothing to do yet   these urlargs will be passed to requirejs
            //this is useful for versioning and cache breaking
        }

        private const string EntryPointRoot = @"~/Scripts/";
        static private readonly string[] ConfigurationFiles = {"~/Scripts/RequireJS.json"};
    }
}