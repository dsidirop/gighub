﻿using System.Collections.Generic;
using System.IO.Abstractions;

//warning to future maintainers    the class u and any of its methods used in cshtml files and the like need
//warning to future maintainers    to be public so that they will be usable by the aspnet infrastructure
namespace GigHub.Core.Utilities
{
    static public class UFilepaths
    {
        static private readonly FileSystem FileSystem = new FileSystem();
        static public IEnumerable<string> GetAllParentDirectories(this string path)
        {
            for (; !string.IsNullOrWhiteSpace(path); path = FileSystem.Directory.GetParent(path)?.FullName)
            {
                yield return path;
            }
        }
    }
}