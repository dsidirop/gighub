﻿using System;
using System.Collections;
using System.Collections.Generic;

//warning to future maintainers    the class u and any of its methods used in cshtml files and the like need
//warning to future maintainers    to be public so that they will be usable by the aspnet infrastructure
namespace GigHub.Core.Utilities
{
    static public class UEnumerableCountT
    {
        static public bool HasLessThan<T>(this IEnumerable<T> sequence, int count) => !sequence.HasMoreThan(count - 1);
        static public bool HasLessOrEqualTo<T>(this IEnumerable<T> sequence, int count) => !sequence.HasMoreThan(count);
        static public bool HasMoreOrEqualTo<T>(this IEnumerable<T> sequence, int count) => sequence.HasMoreThan(count - 1);
        static public bool HasMoreThan<T>(this IEnumerable<T> sequence, int count) => sequence.EnumerationCounterImpl(count, equals_vs_greaterThan: false);
        static public bool HasExactly<T>(this IEnumerable<T> sequence, int count) => sequence.EnumerationCounterImpl(count, equals_vs_greaterThan: true);

        static public bool EnumerationCounterImpl<T>(this IEnumerable<T> sequence, int count, bool equals_vs_greaterThan = true) //0
        {
            if (equals_vs_greaterThan && count < 0)
                throw new ArgumentException($"{nameof(count)} is less than zero!");

            if (!equals_vs_greaterThan && count < 0)
                return true;

            var staticCount = (sequence as ICollection)?.Count //order
                              ?? (sequence as ICollection<T>)?.Count //order
                              ?? (sequence as IReadOnlyCollection<T>)?.Count; //order

            if (staticCount != null)
                return staticCount > count;

            using (var enumerator = sequence.GetEnumerator()) //1 optimization
            {
                for (int i = 0; i < count + 1; i++)
                {
                    if (enumerator.MoveNext())
                        continue;

                    return false;
                }

                return !equals_vs_greaterThan //     ==
                       || enumerator.MoveNext(); //  >
            }

            //0 https://blog.slaks.net/2015-01-12/linq-count-considered-occasionally-harmful/
            //1 using the enumerator directly is slightly faster than using LINQ methods   it avoids allocating an extra iterator
            //  state machine compared to using skip()
        }
    }
}