﻿using System;
using System.Diagnostics;
using System.IO.Abstractions;
using System.Linq;
using System.Reflection;

//warning to future maintainers    the class u and any of its methods used in cshtml files and the like need
//warning to future maintainers    to be public so that they will be usable by the aspnet infrastructure
namespace GigHub.Core.Utilities
{
    static public class UAssemblies
    {
        static public string DeduceOriginFolderPathForAssemblyOf<T>()
        {
            var codeBase = typeof(T).Assembly.CodeBase;
            if (codeBase == null)
                throw new Exception($"[UA.DOFPFAO01] Failed to get the assembly origin-folder-path for type {typeof(T).Name}");

            var uri = new UriBuilder(codeBase);
            var path = Uri.UnescapeDataString(uri.Path);

            return new FileSystem()
                .Path
                .GetDirectoryName(path);

            //to future maintainers   never use this method in the efpowertools projects   it wont have the desired effect because those tools spawn the dlls inside
            //to future maintainers
            //to future maintainers                          C:\Users\<username>\AppData\Local\Microsoft\VisualStudio\15.0_<something random>
        }

        static private readonly FileSystem FileSystem = new FileSystem();
        static public string GetExecutingDirectory()
        {
            var entryAssembly = Assembly.GetEntryAssembly() ?? new StackTrace()
                .GetFrames()
                ?.Last()
                .GetMethod()
                .Module
                .Assembly;
            if (entryAssembly == null)
                throw new Exception("[UA.GED01] Failed to get entry assembly");

            var assemblyName = entryAssembly.GetName();
            if (assemblyName == null)
                throw new Exception("[UA.GED02] Failed to get the name of the entry assembly");

            var codeBase = assemblyName.CodeBase;
            if (codeBase == null)
                throw new Exception("[UA.GED03] Failed to get the code base of the entry assembly");

            var location = new Uri(codeBase);

            return FileSystem
                .FileInfo
                .FromFileName(location.AbsolutePath)
                .FullName;
        }
    }
}