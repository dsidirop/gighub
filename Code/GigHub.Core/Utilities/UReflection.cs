﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Ninject.Infrastructure.Language;

namespace GigHub.Core.Utilities
{
    static public class UReflection
    {
        static public TAttribute TryGrabAttributeFromLiveInstanceUsingBaseTypeExpression<
            TModel,
            TModelForExpression,
            TProperty,
            TAttribute
        >(
            this TModel liveInstance,
            Expression<Func<TModelForExpression, TProperty>> expression
        )
            where TModel : class
            where TAttribute : Attribute
            //where TModelForExpression : class //dont  because it can also be an interface
        {
            var typeOfModel = typeof(TModel);
            var typeOfModelForExpression = typeof(TModelForExpression);
            if (typeOfModel.IsInterface) //order
                throw new InvalidOperationException($"[UR.TGAFVUIE00] [SAFEGUARD] TModel='{typeOfModel.Name}' is an interface type which is an obvious blunder (there's is no way it will hold custom attributes!)");

            if (!typeOfModelForExpression.IsAssignableFrom(typeOfModel))
                throw new InvalidOperationException($"[UR.TGAFVUIE01] [SAFEGUARD] TModel='{typeOfModel.Name}' is not assignable from TModelForExpression='{typeOfModelForExpression.Name}'!");

            var typesAreIdentical = typeOfModel == typeOfModelForExpression;
            if (!typesAreIdentical && !typeOfModelForExpression.IsInterface)
                throw new NotSupportedException($"[UR.TGAFVUIE02] [NOT_SUPPORTED] TModelForExpression='{typeOfModelForExpression.Name}' is not an interface nor is it the same type as TModel='{typeOfModel.Name}' - this is not supported yet!");

            if (typesAreIdentical)
            {
                // TModelForExpression is the exact same class as TModel so its either a concrete or an abstract class
                // either way we can rely directly on it to get the custom attributes as an optimization
                return expression.TryGrabAttribute<TModelForExpression, TProperty, TAttribute>();
            }

            var interfaceMap = liveInstance
                .GetType()
                .GetInterfaceMap(interfaceType: typeof(TModelForExpression));

            var targetMemberInfo = expression.GrabMemberInfo() ;
            var targetPropertyInfo = targetMemberInfo as PropertyInfo;
            var targetPropertyGetterMethodInfo = targetPropertyInfo?.GetGetMethod();

            // var foo = interfaceMap.InterfaceMethods.ToList();

            var actualGetterMethodInfoFromLiveInstance = interfaceMap
                .InterfaceMethods
                .FirstOrDefault(
                    x => x == targetPropertyGetterMethodInfo
                         // || targetMemberInfoAsPropertyInfo?.PropertyType.IsAssignableFrom(x.) //todo
                );

            return actualGetterMethodInfoFromLiveInstance?.GrabAttribute<TAttribute>();

            //PropertyInfo nameProperty = interfaceType.GetProperty("Name");
            //var foo = (expression.GrabMemberInfo() as PropertyInfo).GetGetMethod();
            //var nameGetter = nameProperty.GetGetMethod();
            //
            // todo
            //
            // var memberExpression = expression.Body as MemberExpression;
            // var propertyInfo = memberExpression.Member as PropertyInfo;
            //
            // var owhefh = propertyInfo.GetType();
            //
            // InterfaceMapping mapping = rootModel.GetType().GetInterfaceMap(typeof(TModel));
            //
            // https://stackoverflow.com/questions/3285392/matching-an-interfaces-properyinfo-with-a-classs-propertyinfo/3285450
        }

        static public TAttribute TryGrabAttribute<TModel, TProperty, TAttribute>(this Expression<Func<TModel, TProperty>> expression) where TAttribute : Attribute
        {
            var memberInfo = expression.GrabMemberInfo();

            return memberInfo.GrabAttribute<TAttribute>()
                   ?? (memberInfo as PropertyInfo)?.PropertyType.GrabAttribute<TAttribute>();
        }

        static public MemberInfo GrabMemberInfo<TModel, TProperty>(this Expression<Func<TModel, TProperty>> expression)
        {
            if (!(expression.Body is MemberExpression memberExpression))
                throw new InvalidOperationException("[UR.GMI01] [SAFEGUARD] Expression must be a member expression");

            return memberExpression.Member;
        }

        static public TAttribute GrabAttribute<TAttribute>(this ICustomAttributeProvider provider) where TAttribute : Attribute
            => provider
                .GrabAttributes<TAttribute>()
                .FirstOrDefault();

        static public IEnumerable<TAttribute> GrabAttributes<TAttribute>(this ICustomAttributeProvider provider) where TAttribute : Attribute
            => provider
                .GetCustomAttributes(typeof(TAttribute), inherit: true) //todo    support TAttribute as an interface too
                .Cast<TAttribute>();
    }
}
