﻿using System.Collections.Generic;
using Antlr.Runtime.Misc;

namespace GigHub.Core.Utilities
{
    static public class UDictionary
    {
        static public IDictionary<TKey, TValue> AddIfMissing<TKey, TValue>(
            this IDictionary<TKey, TValue> dictionary,
            TKey key,
            TValue value
        )
        {
            if (!dictionary.ContainsKey(key))
            {
                dictionary.Add(key, value);
            }

            return dictionary;
        }

        static public TValue GetValueOrDefault<TKey, TValue>(
            this IDictionary<TKey, TValue> dictionary,
            TKey key
        )
            => dictionary.GetValueOrDefault(key, default(TValue));

        static public TValue GetValueOrDefault<TKey, TValue>(
            this IDictionary<TKey, TValue> dictionary,
            TKey key,
            TValue defaultValue
        )
            => dictionary.TryGetValue(key, out var value)
                ? value
                : defaultValue;

        static public TValue GetValueOrDefault<TKey, TValue>(
            this IDictionary<TKey, TValue> dictionary,
            TKey key,
            Func<TValue> defaultValueProvider
        )
            => dictionary.TryGetValue(key, out var value)
                ? value
                : defaultValueProvider();
    }
}