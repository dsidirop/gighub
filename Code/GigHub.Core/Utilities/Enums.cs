﻿using System.Reflection;

namespace GigHub.Core.Utilities
{

    public enum ENotificationType
    {
        GigUpdated = 1,
        GigCreated = 2,
        GigCanceled = 3,
        GigUncanceled = 4
    }
}