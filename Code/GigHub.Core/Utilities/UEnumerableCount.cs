﻿using System;
using System.Collections;
using System.Collections.Generic;

//warning to future maintainers    the class u and any of its methods used in cshtml files and the like need
//warning to future maintainers    to be public so that they will be usable by the aspnet infrastructure
namespace GigHub.Core.Utilities
{
    static public class UEnumerableCount
    {
        // ienumerable is a special kind of animal    we should not try to use IEnumerable.Cast<object>().HasLessThan() as its not the same thing
        static public bool HasLessThan(this IEnumerable sequence, int count) => !sequence.HasMoreThan(count - 1);
        static public bool HasLessOrEqualTo(this IEnumerable sequence, int count) => !sequence.HasMoreThan(count);
        static public bool HasMoreOrEqualTo(this IEnumerable sequence, int count) => sequence.HasMoreThan(count - 1);
        static public bool HasMoreThan(this IEnumerable sequence, int count) => sequence.EnumerationCounterNonGenericImpl(count, equals_vs_greaterThan: false);
        static public bool HasExactly(this IEnumerable sequence, int count) => sequence.EnumerationCounterNonGenericImpl(count, equals_vs_greaterThan: true);
        static public bool EnumerationCounterNonGenericImpl(this IEnumerable sequence, int count, bool equals_vs_greaterThan = true) //0
        {
            if (equals_vs_greaterThan && count < 0)
                throw new ArgumentException($"{nameof(count)} is less than zero!");

            if (!equals_vs_greaterThan && count < 0)
                return true;

            var staticCount = (sequence as ICollection)?.Count
                              ?? (sequence as ICollection<object>)?.Count //shooting in the dark now and hoping for the best
                              ?? (sequence as ICollection<int>)?.Count
                              //?? (sequence as ICollection<string>)?.Count
                              ?? (sequence as ICollection<long>)?.Count
                              ?? (sequence as ICollection<byte>)?.Count
                              ?? (sequence as ICollection<float>)?.Count
                              ?? (sequence as ICollection<double>)?.Count
                              ?? (sequence as ICollection<decimal>)?.Count
                              ?? (sequence as ICollection<char>)?.Count
                              ?? (sequence as ICollection<bool>)?.Count
                              ?? (sequence as ICollection<DateTime>)?.Count
                              ?? (sequence as ICollection<sbyte>)?.Count
                              ?? (sequence as ICollection<short>)?.Count
                              ?? (sequence as ICollection<ushort>)?.Count
                              ?? (sequence as ICollection<uint>)?.Count
                              ?? (sequence as ICollection<ulong>)?.Count
                              ?? (sequence as IReadOnlyCollection<object>)?.Count
                              ?? (sequence as IReadOnlyCollection<int>)?.Count
                              //?? (sequence as IReadOnlyCollection<string>)?.Count
                              ?? (sequence as IReadOnlyCollection<long>)?.Count
                              ?? (sequence as IReadOnlyCollection<byte>)?.Count
                              ?? (sequence as IReadOnlyCollection<float>)?.Count
                              ?? (sequence as IReadOnlyCollection<double>)?.Count
                              ?? (sequence as IReadOnlyCollection<decimal>)?.Count
                              ?? (sequence as IReadOnlyCollection<char>)?.Count
                              ?? (sequence as IReadOnlyCollection<bool>)?.Count
                              ?? (sequence as IReadOnlyCollection<DateTime>)?.Count
                              ?? (sequence as IReadOnlyCollection<sbyte>)?.Count
                              ?? (sequence as IReadOnlyCollection<short>)?.Count
                              ?? (sequence as IReadOnlyCollection<ushort>)?.Count
                              ?? (sequence as IReadOnlyCollection<uint>)?.Count
                              ?? (sequence as IReadOnlyCollection<ulong>)?.Count;

            if (staticCount != null)
                return staticCount > count;

            // ienumerable doesnt support using
            var enumerator = sequence.GetEnumerator(); //1 optimization
            {
                for (int i = 0; i < count + 1; i++)
                {
                    if (enumerator.MoveNext())
                        continue;

                    return false;
                }

                return !equals_vs_greaterThan //     ==
                       || enumerator.MoveNext(); //  >
            }

            //0 https://blog.slaks.net/2015-01-12/linq-count-considered-occasionally-harmful/
            //1 using the enumerator directly is slightly faster than using LINQ methods   it avoids allocating an extra iterator
            //  state machine compared to using skip()
        }
    }
}