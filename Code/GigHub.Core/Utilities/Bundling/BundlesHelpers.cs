﻿using System;
using System.Collections.Generic;
using System.IO.Abstractions;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web.Hosting;
using System.Web.Optimization;
using GigHub.Core.Utilities.Bundling.Transforms;

#pragma warning disable 162

//warning to future maintainers    the class u and any of its methods used in cshtml files and the like need
//warning to future maintainers    to be public so that they will be usable by the aspnet infrastructure
namespace GigHub.Core.Utilities.Bundling
{
    static public class BundlesHelpers
    {
        static public Bundle WithOrderer(this Bundle sb, IBundleOrderer orderer)
        {
            sb.Orderer = orderer;
            return sb;
        }

        static public Bundle WithCacheInvalidationToken(this Bundle sb)
        {
            sb.Transforms.Add(new CacheInvalidationTransform());
            return sb;
        }

        static public Bundle WithJsSourceUrl(this Bundle bundle)
        {
            bundle.Transforms.Add(new JsSourceUrlTransform());
            return bundle;
        }

        static public Bundle IncludeExisting(this Bundle bundle, params string[] virtualPaths)
        {
            EnsurePathsExist(virtualPaths);

            return bundle.Include(virtualPaths);

            //http://stackoverflow.com/questions/23802800/asp-net-mvc-bundling-best-way-to-detect-missing-file
        }

        static public Bundle IncludeExisting(this Bundle bundle, string virtualPath, params IItemTransform[] transforms)
        {
            RequireExistence(virtualPath);

            return bundle.Include(virtualPath, transforms);

            //http://stackoverflow.com/questions/23802800/asp-net-mvc-bundling-best-way-to-detect-missing-file
        }

        static private readonly FileSystem FileSystem = new FileSystem();
        static private void RequireExistence(string virtualPath)
        {
            var i = virtualPath.LastIndexOf('/');
            var parentDirectory = HostingEnvironment.MapPath(virtualPath.Substring(0, i));
            if (!FileSystem.Directory.Exists(parentDirectory))
                throw new Exception($"Bundle resource '{virtualPath}' not found");

            var nameOfTargetFile = virtualPath.Substring(i + 1);
            if (nameOfTargetFile.ContainsCI("{version}"))
            {
                var re = new Regex(
                    nameOfTargetFile
                        .Replace(oldValue: ".", newValue: @"\.")
                        .ReplaceCI(toReplace: "{version}", replacement: @"(\d+(?:\.\d+){1,3})")
                );
                var nameOfTargetFileWildcardFormat = nameOfTargetFile.ReplaceCI("{version}", "*");
                if (!FileSystem.Directory.EnumerateFiles(parentDirectory, nameOfTargetFileWildcardFormat).Any(file => re.IsMatch(file)))
                    throw new Exception($"Bundle resource '{virtualPath}' not found");
            }
            else if (!FileSystem.Directory.EnumerateFiles(parentDirectory, nameOfTargetFile).Any()) //0
            {
                throw new Exception($"Bundle resource '{virtualPath}' not found");
            }

            //0 filename may contain * wildcards
        }

        static private void EnsurePathsExist(IEnumerable<string> virtualPaths)
            => virtualPaths.Pipe(RequireExistence).Run();

        static internal void IncludePaths(this Bundle bundle, string[] virtualPaths, IItemTransform transformer)
            => virtualPaths.Pipe(virtualPath => bundle.Include(virtualPath, transformer)).Run();

        sealed private class CacheInvalidationTransform : IBundleTransform
        {
            public void Process(BundleContext context, BundleResponse response)
            {
                response
                    .Files
                    .Pipe(file =>
                    {
                        var filepath = HostingEnvironment.MapPath(file.IncludedVirtualPath);

                        file.IncludedVirtualPath = filepath == null
                            ? file.IncludedVirtualPath
                            : $"{file.IncludedVirtualPath}?v={GetVersionStringUrlEncoded(filepath)}";
                    })
                    .Run();
            }

            static private string GetVersionStringUrlEncoded(string filepath)
            {
                // ReSharper disable ConditionIsAlwaysTrueOrFalse
                // ReSharper disable UnreachableCode
                try
                {
                    return UEnv.IsDebugModeOn
                        ? WebUtility.UrlEncode(FileSystem.File.GetLastWriteTime(filepath).Ticks.ToString()) //development
                        : RenderAppVersionStringUrlEncoded; //0 production
                }
                catch //just in case
                {
                    return $"LMBT_GVS_01_{Guid.NewGuid()}";
                }

                //0 when it comes to production builds we need to make ensure that the build system of our ovens will religiously increment
                //  the build number by one for each and every production build   this is vital to ensure proper cache invalidation
                //
                // ReSharper restore ConditionIsAlwaysTrueOrFalse
                // ReSharper restore UnreachableCode
            }

            static private readonly string RenderAppVersionStringUrlEncoded = WebUtility.UrlEncode(Assembly.GetExecutingAssembly().GetName().Version.ToString());
        }
    }
}
#pragma warning restore 162