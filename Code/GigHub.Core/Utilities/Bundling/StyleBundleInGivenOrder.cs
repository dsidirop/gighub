﻿using System.Web.Optimization;

namespace GigHub.Core.Utilities.Bundling
{
    sealed internal class StyleBundleInGivenOrder : StyleBundle
    {
        public StyleBundleInGivenOrder(string virtualPath) : base(virtualPath)
        {
            Orderer = new AsIsBundleOrderer();
        }

        public StyleBundleInGivenOrder(string virtualPath, string cdnPath) : base(virtualPath, cdnPath)
        {
            Orderer = new AsIsBundleOrderer();
        }
    }
}