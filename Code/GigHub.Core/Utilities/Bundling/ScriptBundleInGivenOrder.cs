﻿using System.Web.Optimization;

namespace GigHub.Core.Utilities.Bundling
{
    sealed internal class ScriptBundleInGivenOrder : ScriptBundle
    {
        public ScriptBundleInGivenOrder(string virtualPath) : base(virtualPath)
        {
            Orderer = new AsIsBundleOrderer();
        }

        public ScriptBundleInGivenOrder(string virtualPath, string cdnPath) : base(virtualPath, cdnPath)
        {
            Orderer = new AsIsBundleOrderer();
        }
    }
}