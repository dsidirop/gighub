﻿using System.Collections.Generic;
using System.Web.Optimization;

namespace GigHub.Core.Utilities.Bundling
{
    sealed internal class AsIsBundleOrderer : IBundleOrderer
    {
        public IEnumerable<BundleFile> OrderFiles(BundleContext context, IEnumerable<BundleFile> files)
        {
            return files;
        }
    }
}