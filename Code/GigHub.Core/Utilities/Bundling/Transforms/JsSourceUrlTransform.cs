﻿using System.Web;
using System.Web.Optimization;

namespace GigHub.Core.Utilities.Bundling.Transforms
{
    sealed internal class JsSourceUrlTransform : IBundleTransform
    {
        public void Process(BundleContext context, BundleResponse response)
        {
#if DEBUG
            response.Content += $@"{UEnv.nl2}//# sourceURL={VirtualPathUtility.ToAbsolute(context.BundleVirtualPath)}{UEnv.nl}";
#endif
        }
    }
}