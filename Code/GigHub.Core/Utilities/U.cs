﻿using System;

namespace GigHub.Core.Utilities
{
    static public class U
    {
        static public void Try(Action action)
        {
            try
            {
                action();
            }
            catch
            {
                // ignored
            }
        }

        static public T Try<T>(Func<T> func)
        {
            try
            {
                return func();
            }
            catch
            {
                return default(T);
            }
        }
    }
}