﻿//warning to future maintainers    the class u and any of its methods used in cshtml files and the like need
//warning to future maintainers    to be public so that they will be usable by the aspnet infrastructure
//namespace GigHub.Core.Utilities
//{
// static public class UDbs
// {
//     static public bool CheckIfTableExists(this DbContext context, string tableName) //0
//     {
//         var connection = context?.Database?.Connection;
//         if (connection == null)
//             throw new ArgumentException(nameof(context));
//
//         if (tableName == null)
//             throw new ArgumentNullException(nameof(tableName));
//
//         if (connection.State == ConnectionState.Closed)
//             connection.Open();
//
//         var tableInfoOnTables = connection //1
//             .GetSchema("Tables")
//             .Columns
//             .Cast<DataColumn>()
//             .Select(x => x.ColumnName?.ToLowerInvariant() ?? "")
//             .ToList();
//
//         var tableNameColumnIndex = tableInfoOnTables.FindIndex(x => x.Contains("table".ToLowerInvariant()) && x.Contains("name".ToLowerInvariant())); //order
//         tableNameColumnIndex = tableNameColumnIndex == -1 ? tableInfoOnTables.FindIndex(x => x.Contains("table".ToLowerInvariant())) : tableNameColumnIndex; //order
//         tableNameColumnIndex = tableNameColumnIndex == -1 ? tableInfoOnTables.FindIndex(x => x.Contains("name".ToLowerInvariant())) : tableNameColumnIndex; //order
//
//         if (tableNameColumnIndex == -1)
//             throw new ApplicationException("Failed to spot which column holds the names of the tables in the dictionary-table of the DB");
//
//         var constraints = new string[tableNameColumnIndex + 1];
//         constraints[tableNameColumnIndex] = tableName;
//
//         return connection.GetSchema("Tables", constraints)?.Rows.Count > 0;
//
//         //0 this method not being used anywhere for the time being   its just here for future reference
//         //1 different databases have different number of columns and different names assigned to them
//         //
//         //     SCHEMA,TABLENAME,TYPE -> oracle
//         //     table_catalog,table_schema,table_name,table_type -> mssqlserver
//         //
//         //  we thus need to figure out which column represents the tablename and target that one
//     }
// }
//}

