﻿using System;

namespace GigHub.Core.Utilities
{
    static public class UDates
    {
        public const string GigDateFormat = @"d MMM yyyy";
        public const string GigTimeOfDayFormat = @"HH:mm";

        static public string FormatDateTryOmitYear(DateTime dateTime) => dateTime.Year == DateTime.UtcNow.Year
            ? dateTime.ToString("dd MMM")
            : dateTime.ToString("dd MMM yyyy");
    }
}