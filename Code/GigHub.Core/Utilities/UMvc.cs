﻿using System.Text.RegularExpressions;
using System.Web.Mvc;

//warning to future maintainers    the class u and any of its methods used in cshtml files and the like need
//warning to future maintainers    to be public so that they will be usable by the aspnet infrastructure
namespace GigHub.Core.Utilities
{
    static public class UMvc
    {
        static private readonly Regex TrailingControllerStringRegex = new Regex(@"Controller$", RegexOptions.IgnoreCase);

        static public string Ctrler<T>() where T : Controller => typeof(T).Name.RemoveControllerPostfix(); //dont use nameof(T) here it wont work

        static public string RemoveControllerPostfix(this string input) => TrailingControllerStringRegex.Replace(input, "");
    }
}