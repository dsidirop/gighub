﻿// https://github.com/babel/gulp-babel
// https://riptutorial.com/gulp/example/11376/minify-js-using-gulp-minify

const gulp = require('../node_modules/gulp');
const babel = require('../node_modules/gulp-babel');
const minify = require('../node_modules/gulp-minify');
const sourcemaps = require('../node_modules/gulp-sourcemaps');

gulp.task(
    'build:common', // ./Scripts/_Source/Common -> ./Scripts/Common
    function () {
        return gulp
            .src([
                './Scripts/_Source/Common/**/*.js',
                '!./Scripts/**/*.min.js',
                '!./Scripts/**/*-min.js'
            ])
            .pipe(sourcemaps.init())
            .pipe(
                babel({
                    presets: ['@babel/preset-env'],
                    plugins: ['@babel/transform-runtime']
                })
            )
            .pipe(
                minify({
                    ext: {min: '.min.js'},
                    noSource: true,
                    ignoreFiles: ['.min.js', '-min.js']
                })
            )
            .pipe(sourcemaps.write()) //embeds the jsmap right into the minjs file
            .pipe(gulp.dest('./Scripts/Common'))
    }
);

gulp.task(
    'build:controllers:partials', // ./Scripts/_Source/Controllers/Partials -> ./Scripts/Controllers/Partials
    function () {
        return gulp
            .src([
                './Scripts/_Source/Controllers/Partials/**/*.js',
                '!./Scripts/**/*.min.js',
                '!./Scripts/**/*-min.js'
            ])
            .pipe(sourcemaps.init())
            .pipe(
                babel({
                    presets: ['@babel/preset-env'],
                    plugins: ['@babel/transform-runtime']
                })
            )
            .pipe(
                minify({
                    ext: {min: '.min.js'},
                    noSource: true,
                    ignoreFiles: ['.min.js', '-min.js']
                })
            )
            .pipe(sourcemaps.write()) //embeds the jsmap right into the minjs file
            .pipe(gulp.dest('./Scripts/Controllers/Partials'))
    }
);

gulp.task(
    'build:controllers:root', // ./Scripts/_Source/Controllers/Root -> ./Scripts/Controllers/Root
    function () {
        return gulp
            .src([
                './Scripts/_Source/Controllers/Root/**/*.js',
                '!./Scripts/**/*.min.js',
                '!./Scripts/**/*-min.js'
            ])
            .pipe(sourcemaps.init())
            .pipe(
                babel({
                    presets: ['@babel/preset-env'],
                    plugins: ['@babel/transform-runtime']
                })
            )
            .pipe(
                minify({
                    ext: {min: '.js'}, //keep .js here   using .min.js will break things
                    noSource: true,
                    ignoreFiles: ['.min.js', '-min.js']
                })
            )
            .pipe(sourcemaps.write()) //embeds the jsmap right into the minjs file
            .pipe(gulp.dest('./Scripts/Controllers/Root')) // must be this exact directory otherwise requirejs.net will break immediately
    }
);

gulp.task(
    'watch:all',
    function () {
        gulp.watch(
            [
                './Scripts/_Source/Common/**/*.js',
                '!./Scripts/**/*.min.js',
                '!./Scripts/**/*-min.js'
            ],
            gulp.series('build:common')
        );

        gulp.watch(
            [
                './Scripts/_Source/Controllers/Partials/**/*.js',
                '!./Scripts/**/*.min.js',
                '!./Scripts/**/*-min.js'
            ],
            gulp.series('build:controllers:partials')
        );

        gulp.watch(
            [
                './Scripts/_Source/Controllers/Root/**/*.js',
                '!./Scripts/**/*.min.js',
                '!./Scripts/**/*-min.js'
            ],
            gulp.series('build:controllers:root')
        );
    }
);

gulp.task(
    'build:all',
    gulp.series(
        'build:common',
        'build:controllers:root',
        'build:controllers:partials'
    )
);

gulp.task(
    'default',
    gulp.series(
        'build:all',
        'watch:all'
    )
);

