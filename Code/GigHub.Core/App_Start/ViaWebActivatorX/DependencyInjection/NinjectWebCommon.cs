using System;
using System.Web;
using System.Web.Http;
using GigHub.Core.Foundation.Mapper;
using GigHub.Core.Foundation.Mapper.Mappings;
using GigHub.Core.ViaWebActivatorX.DependencyInjection;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using Ninject;
using Ninject.Extensions.Conventions;
using Ninject.Syntax;
using Ninject.Web.Common;
using Ninject.Web.Common.WebHost;
using Ninject.Web.WebApi;

// ReSharper disable UnusedMember.Global
// ReSharper disable UnusedParameter.Local

[assembly: WebActivatorEx.PreApplicationStartMethod(type: typeof(NInjectWebCommon), methodName: nameof(NInjectWebCommon.Start))]
[assembly: WebActivatorEx.ApplicationShutdownMethod(type: typeof(NInjectWebCommon), methodName: nameof(NInjectWebCommon.Stop))]

namespace GigHub.Core.ViaWebActivatorX.DependencyInjection
{
    static public class NInjectWebCommon
    {
        static private readonly Bootstrapper Bootstrapper = new Bootstrapper();

        static public void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            Bootstrapper.Initialize(() =>
            {
                var kernel = CreateKernel();

                GlobalConfiguration
                    .Configuration
                    .DependencyResolver = new NinjectDependencyResolver(kernel); //0

                // System.Web.Mvc //mvc
                //     .DependencyResolver
                //     .SetResolver(new Ninject.Web.Mvc.NinjectDependencyResolver(kernel)); //noneed

                return kernel;
            });

            //0 https://nodogmablog.bryanhogan.net/2016/04/web-api-2-and-ninject-how-to-make-them-work-together/
        }

        static public void Stop() => Bootstrapper.ShutDown();

        static private IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();
                RegisterServices(kernel);

                kernel.Bind(x =>
                {
                    x
                        .FromThisAssembly()
                        .Select(type => type != typeof(MappingService))
                        .BindDefaultInterface();
                });

                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        static private void RegisterServices(IBindingRoot kernel)
        {
            kernel.Bind<IMapping>().To<GigToGigDto>();
            kernel.Bind<IMapping>().To<GenreToGenreDto>();
            kernel.Bind<IMapping>().To<ApplicationToApplicationDto>();
            kernel.Bind<IMapping>().To<NotificationToNotificationDto>();

            kernel.Bind<IMappingService>().To<MappingService>().InSingletonScope();
        }
    }
}