﻿using GigHub.Core.ViaOwin;
using GigHub.Core.ViaOwin.Auth;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Startup))]
namespace GigHub.Core.ViaOwin
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            AuthSetup.Setup(app);
        }
    }
}
