﻿using System;
using GigHub.Core.Foundation.Models;
using GigHub.Core.Persistence.Contexts;
using GigHub.Core.ViaGlobalAsax.MVCSpecific.Identity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;

namespace GigHub.Core.ViaOwin.Auth
{
    static public class AuthSetup // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
    {
        static public void Setup(IAppBuilder app)
        {
            app.CreatePerOwinContext(ApplicationDbContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);
            app.CreatePerOwinContext<ApplicationSignInManager>(ApplicationSignInManager.Create);

            app.UseCookieAuthentication(new CookieAuthenticationOptions //0
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
                Provider = new CookieAuthenticationProvider
                {
                    OnApplyRedirect = ctx =>
                    {
                        if (!IsAjaxRequest(ctx.Request)) //1 [authorize] attribute on webapi controllers
                        {
                            ctx.Response.Redirect(ctx.RedirectUri);
                        }
                    },
                    OnValidateIdentity = SecurityStampValidator.OnValidateIdentity<ApplicationUserManager, ApplicationUser>( //2
                        validateInterval: TimeSpan.FromMinutes(30),
                        regenerateIdentity: (manager, user) => user.GenerateUserIdentityAsync(manager)
                    )
                }
            });
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);
            app.UseTwoFactorSignInCookie(DefaultAuthenticationTypes.TwoFactorCookie, TimeSpan.FromMinutes(5)); //3
            app.UseTwoFactorRememberBrowserCookie(DefaultAuthenticationTypes.TwoFactorRememberBrowserCookie); //4 5

            //0 Enable the application to use a cookie to store information for the signed in user and to use a cookie to temporarily
            //  store information about a user logging in with a third party login provider   Configure the sign in cookie
            //
            //1 [authorize] attribute in mvc5 webapi controllers can return 401 if and only if we apply this tweak on applyredirect
            //  if this tweak is ommitted then instead of 401 we will get 200ok even when unauthorized which is completely crazy
            //                  http://kevin-junghans.blogspot.gr/2013/12/returning-401-http-status-code-on.html
            //
            //2 Enables the application to validate the security stamp when the user logs in   This is a security feature which is used
            //  when you change a password or add an external login to your account
            //
            //3 Enables the application to temporarily store user information when they are verifying the second factor in the twofactor
            //  authentication process
            //
            //4 Enables the application to remember the second login verification factor such as phone or email   Once you check this
            //  option your second step of verification during the login process will be remembered on the device where you logged in from
            //  This is similar to the RememberMe option when you log in
            //
            //5 Uncomment the following lines to enable logging in with third party login providers
            //
            //  app.UseMicrosoftAccountAuthentication(clientId: "", clientSecret: "");
            //  app.UseTwitterAuthentication(consumerKey: "", consumerSecret: "");
            //  app.UseFacebookAuthentication(appId: "", appSecret: "");
            //  app.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions { ClientId = "", ClientSecret = "" });
        }

        static private bool IsAjaxRequest(IOwinRequest request)
        {
            var query = (IReadableStringCollection)null;
            var headers = (IHeaderDictionary)null;
            return (query = request.Query) != null && query["X-Requested-With"] == "XMLHttpRequest"
                || (headers = request.Headers) != null && headers["X-Requested-With"] == "XMLHttpRequest";
        }
    }
}
