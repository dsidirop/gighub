﻿using System.Linq;
using System.Web.Mvc;
using GigHub.Core.Foundation.ModelBinders.MVC;

namespace GigHub.Core.ViaGlobalAsax.MVCSpecific.Deserialization
{
    static public class ParametersDeserializationConfig
    {
        static public void Setup()
        {
            SetupGlobalModelBindings(); //0
            SetupGlobalValueProviders(); //1

            //0 ensures that "{foo:''}" => foo=emptystring instead of null
            //1 ensures that "{foo:[]}" => foo=new long[0] instead of null
        }

        static private void SetupGlobalModelBindings()
        {
            ModelBinders.Binders.DefaultBinder = new CustomModelBinderMvc(); //0 here be dragons

            //0 here be dragons     we absolutely need to ensure that "{foo:''}" get turned into foo=emptystring instead of null    omitting this tweak would
            //  here be dragons     allow the default behaviour of mvc to kick in and that behaviour is to turn empty strings to null which is just flat out wrong
        }

        static private void SetupGlobalValueProviders()
        {
            ValueProviderFactories.Factories.Remove(ValueProviderFactories.Factories.OfType<JsonValueProviderFactory>().FirstOrDefault()); //0 vital   bugworkaround for empty array
            ValueProviderFactories.Factories.Add(new NewtonsoftJsonValueProviderFactoryMvc()); //0 vital                                               bugworkaround for empty array

            //0 to future maintainers    we need to use a customized newtonsoft json deserializer in order to workaround a malevolent bu.g in the
            //  to future maintainers    builtin serializer of mvc by which empty json arrays ala "{ foo: [] }" result in csharp objects where the
            //  to future maintainers    foo property is null   this is completely wrong and undesirable hence this approach here with newtosoftjson
            //  to future maintainers    this tweak here changes just the deserialization of parameters when the browser clients invoke the server
            //  to future maintainers    methods   when the server returns json data it still uses the builtin serializer which is still unaphased
            //  to future maintainers
            //  to future maintainers                                    http://stackoverflow.com/a/23405148/863651
            //  to future maintainers
        }
    }
}