﻿using System.Web.Mvc;
using GigHub.Core.Foundation.Attributes;
using GigHub.Core.Foundation.Attributes.MVC;

namespace GigHub.Core.ViaGlobalAsax.MVCSpecific.Filters
{
    static public class FiltersConfig
    {
        static public void Setup()
        {
            var filters = GlobalFilters.Filters;

            //inbound
            filters.Add(new ValidateActionParametersMvcAttribute());
            filters.Add(new ValidateModelMvcAttribute());

            //outbound
            filters.Add(new RequirejsGlobalOptionsFilterAttribute());
            filters.Add(new HandleErrorAttribute());
        }
    }
}