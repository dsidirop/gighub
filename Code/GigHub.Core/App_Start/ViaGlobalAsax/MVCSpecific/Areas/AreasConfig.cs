﻿using System.Web.Mvc;

namespace GigHub.Core.ViaGlobalAsax.MVCSpecific.Areas
{
    static public class AreasConfig
    {
        static public void Setup()
        {
            AreaRegistration.RegisterAllAreas();
        }
    }
}
