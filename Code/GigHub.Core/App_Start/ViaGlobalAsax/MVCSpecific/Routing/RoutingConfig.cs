﻿using System.Web.Mvc;
using System.Web.Routing;
using GigHub.Core.Controllers;
using GigHub.Core.Utilities;

namespace GigHub.Core.ViaGlobalAsax.MVCSpecific.Routing
{
    static public class RoutingConfig
    {
        static public void Setup()
        {
            var routes = RouteTable.Routes;

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.LowercaseUrls = true; //order

            routes.MapMvcAttributeRoutes(); //order
            AreaRegistration.RegisterAllAreas(); //order

            routes.MapRoute( //order
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = UMvc.Ctrler<HomeController>(), action = nameof(HomeController.GigsList), id = UrlParameter.Optional }
            );
        }
    }
}
