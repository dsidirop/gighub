﻿using GigHub.Core.ViaGlobalAsax.MVCSpecific.Areas;
using GigHub.Core.ViaGlobalAsax.MVCSpecific.Bundles;
using GigHub.Core.ViaGlobalAsax.MVCSpecific.Deserialization;
using GigHub.Core.ViaGlobalAsax.MVCSpecific.Filters;
using GigHub.Core.ViaGlobalAsax.MVCSpecific.Routing;

namespace GigHub.Core.ViaGlobalAsax.MVCSpecific
{
    static public class MvcSetup
    {
        static public void Setup()
        {
            AreasConfig.Setup(); //                        order
            FiltersConfig.Setup(); //                      order
            RoutingConfig.Setup(); //                      order
            BundleConfig.Setup(); //                       order
            ParametersDeserializationConfig.Setup(); //    order
        }
    }
}
