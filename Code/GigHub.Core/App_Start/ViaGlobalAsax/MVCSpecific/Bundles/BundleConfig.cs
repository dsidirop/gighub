﻿using System.Web.Optimization;
using BundleTransformer.Core.Bundles;
using BundleTransformer.Core.Resolvers;
using GigHub.Core.Utilities.BundlesRegistry;
using GigHub.Core.Utilities.Bundling;

namespace GigHub.Core.ViaGlobalAsax.MVCSpecific.Bundles
{
    static public class BundleConfig
    {
        // for more information on bundling visit http://go.microsoft.com/fwlink/?LinkId=301862   always bare in mind that a scriptbundler doesnt
        // complain when it doesnt manage to locate any of the files assigned to it    it appears that this was done by design by microsoft folks
        static public void Setup()
        {
            var bundles = BundleTable.Bundles;

            SetDefaultBundleResolver(); //   order
            Rockbed(bundles); //             order
            CreateOrEditGig(bundles); //     order
        }

        static private void SetDefaultBundleResolver()
        {
            BundleResolver.Current = new CustomBundleResolver(); //0

            //0 replace the default bundle resolver so that the debugging http handler can use transformations of the corresponding bundle
        }

        static private void Rockbed(BundleCollection bundles) //layout.cshtml and friends
        {
            //styles
            bundles.Add(new CustomStyleBundle(AppStyles.Rockbed)
                .Include(
                    "~/Content/bootstrap.css", //   order
                    "~/Content/animate.css", //     order
                    "~/Content/Site.sass" //        order
                )
                .WithOrderer(new AsIsBundleOrderer())
                .WithCacheInvalidationToken()
            );

            //scripts
            bundles.Add(new CustomScriptBundle(AppScripts.Rockbed)
                .IncludeExisting("~/Scripts/ThirdParty/modernizr/modernizr-*") //0
                .WithOrderer(new AsIsBundleOrderer())
                .WithJsSourceUrl()
                .WithCacheInvalidationToken()
            );

            //0 use the development version of modernizr to develop with and learn from  then when you are ready for production use the build tool at http://modernizr.com to pick only the tests you need
        }

        static private void CreateOrEditGig(BundleCollection bundles)
        {
            //styles
            bundles.Add(new CustomStyleBundle(AppStyles.CreateOrEdit)
                .Include(
                    "~/Content/select2.css",
                    "~/Content/select2-bootstrap.css",
                    "~/Content/views/create-or-edit.sass"
                )
                .WithOrderer(new AsIsBundleOrderer())
                .WithCacheInvalidationToken()
            );

            //scripts
            //no need   we load js scripts through requirejs
        }
    }
}
