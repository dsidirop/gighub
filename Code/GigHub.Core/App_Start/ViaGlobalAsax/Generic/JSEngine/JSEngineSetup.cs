﻿using JavaScriptEngineSwitcher.Core;
using JavaScriptEngineSwitcher.V8;

namespace GigHub.Core.ViaGlobalAsax.Generic.JSEngine
{
    static public class JsEngineSetup
    {
        static public void Setup()
        {
            var engineSwitcher = JsEngineSwitcher.Current;

            engineSwitcher.EngineFactories.AddV8();
            engineSwitcher.DefaultEngineName = V8JsEngine.EngineName;
        }
    }
}