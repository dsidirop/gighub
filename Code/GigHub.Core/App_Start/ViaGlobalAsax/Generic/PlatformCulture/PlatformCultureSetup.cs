﻿using System.Globalization;

namespace GigHub.Core.ViaGlobalAsax.Generic.PlatformCulture
{
    static public class PlatformCultureSetup
    {
        static public void Setup()
        {
            CultureInfo.DefaultThreadCurrentCulture = CultureInfo.InvariantCulture;
            CultureInfo.DefaultThreadCurrentUICulture = CultureInfo.InvariantCulture;
        }
    }
}
