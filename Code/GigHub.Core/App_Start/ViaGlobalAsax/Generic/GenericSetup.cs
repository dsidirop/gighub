﻿using GigHub.Core.ViaGlobalAsax.Generic.JSEngine;
using GigHub.Core.ViaGlobalAsax.Generic.PlatformCulture;

namespace GigHub.Core.ViaGlobalAsax.Generic
{
    static public class GenericSetup
    {
        static public void Setup()
        {
            PlatformCultureSetup.Setup(); //  order
            JsEngineSetup.Setup(); //         order
        }
    }
}
