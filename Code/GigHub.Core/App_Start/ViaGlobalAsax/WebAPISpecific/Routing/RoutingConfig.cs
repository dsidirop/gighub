﻿using System.Web.Http;

namespace GigHub.Core.ViaGlobalAsax.WebAPISpecific.Routing
{
    static public class RoutingConfig
    {
        static public void Setup(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes(); //0

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new {id = RouteParameter.Optional}
            );

            //0 we need to call maphttpattributeroutes in order to have data annotations like [route("customers/{customerId}/orders")] work as intended in terms of webapi routing
        }
    }
}
