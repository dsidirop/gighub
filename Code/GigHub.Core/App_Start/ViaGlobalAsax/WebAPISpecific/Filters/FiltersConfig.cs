﻿using System.Web.Http;
using GigHub.Core.Foundation.Attributes.WebAPI;

namespace GigHub.Core.ViaGlobalAsax.WebAPISpecific.Filters
{
    static public class FiltersConfig
    {
        static public void Setup(HttpConfiguration config)
        {
            //inbound
            config.Filters.Add(new ValidateActionParametersWebApiAttribute()); //order 0
            config.Filters.Add(new ValidateModelWebApiAttribute()); //order

            //outbound
            //nothing yet

            //0 this sort of tweak is necessary for the [required] attribute to be truly taken into account when applied to action parameters
        }
    }
}
