﻿using System.Web.Http;
using GigHub.Core.ViaGlobalAsax.WebAPISpecific.Deserialization;
using GigHub.Core.ViaGlobalAsax.WebAPISpecific.Filters;
using GigHub.Core.ViaGlobalAsax.WebAPISpecific.Routing;
using GigHub.Core.ViaGlobalAsax.WebAPISpecific.Serialization;

namespace GigHub.Core.ViaGlobalAsax.WebAPISpecific
{
    static public class WebApiSetup
    {
        static public void Setup()
        {
            GlobalConfiguration.Configure(config =>
            {
                RoutingConfig.Setup(config);
                FiltersConfig.Setup(config);
                SerializationConfig.Setup(config);
                ParameterDeserializationConfig.Setup(config);
            });
        }
    }
}