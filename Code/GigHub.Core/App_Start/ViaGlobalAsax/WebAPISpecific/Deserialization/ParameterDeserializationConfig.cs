﻿using System.Globalization;
using System.Web.Http;
using System.Web.Http.Metadata;
using GigHub.Core.Foundation.ModelBinders.WebAPI;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace GigHub.Core.ViaGlobalAsax.WebAPISpecific.Deserialization
{
    static internal class ParameterDeserializationConfig
    {
        static public void Setup(HttpConfiguration config)
        {
            SetupModelBinders(config);
            SetupFormattersForSerialization(config);
        }

        static private void SetupModelBinders(HttpConfiguration config)
        {
            config.Services.Replace(typeof(ModelMetadataProvider), new CustomModelMetadataProviderWebApi()); //0 emptystrings

            //0 respect the emptystring in ajaxstrings aka "{foo:''}" gets converted into foo="" instead of null which is the default and completely wrong
        }

        static private void SetupFormattersForSerialization(HttpConfiguration config)
        {
            config.Formatters.Remove(config.Formatters.XmlFormatter);

            var jsonFormatter = config.Formatters.JsonFormatter; //0 newtonsoft
            jsonFormatter.SerializerSettings.Converters.Add(new IsoDateTimeConverter { Culture = CultureInfo.InvariantCulture });
            jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            jsonFormatter.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            jsonFormatter.SerializerSettings.PreserveReferencesHandling = PreserveReferencesHandling.None; //yank$id

#if DEBUG
            jsonFormatter.SerializerSettings.Formatting = Formatting.Indented;
#endif
            //0 note that around 2012 or so ms switched over to use newtonsoft jsonnet serializer internally
        }
    }
}