﻿using System.Linq;
using System.Net.Http.Formatting;
using System.Web.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace GigHub.Core.ViaGlobalAsax.WebAPISpecific.Serialization
{
    static public class SerializationConfig
    {
        static public void Setup(HttpConfiguration config)
        {
            UninstallXmlFormatter(config);
            ConfigureJsonFormatter(config);
        }

        static private void UninstallXmlFormatter(HttpConfiguration config)
        {
            config.Formatters.Remove(config.Formatters.XmlFormatter);
        }

        static private void ConfigureJsonFormatter(HttpConfiguration config)
        {
            var jsonFormatter = config
                .Formatters
                .OfType<JsonMediaTypeFormatter>()
                .First();

            jsonFormatter.SerializerSettings.Formatting = Formatting.Indented;
            jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            jsonFormatter.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            jsonFormatter.SerializerSettings.PreserveReferencesHandling = PreserveReferencesHandling.None; //yank$id
        }
    }
}
