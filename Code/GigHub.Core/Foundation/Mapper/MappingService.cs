﻿using System.Collections.Generic;
using AutoMapper;
using GigHub.Core.Foundation.Mapper.Mappings;
using GigHub.Core.Utilities;

namespace GigHub.Core.Foundation.Mapper
{
    sealed public class MappingService : IMappingService
    {
        private readonly IMapper _mapper; //automapper essentially

        public MappingService(IEnumerable<IMapping> configs)
        {
            var configuration = new MapperConfiguration(
                cfg =>
                {
                    //cfg.CreateMissingTypeMaps = true;
                    configs
                        .Pipe(c => c.Register(cfg))
                        .Run();
                }
            );

            _mapper = configuration.CreateMapper();
        }

        public TDestination Map<TSource, TDestination>(TSource source)
            => _mapper.Map<TSource, TDestination>(source);
    }
}