﻿using AutoMapper;
using GigHub.Core.Foundation.Dtos;
using GigHub.Core.Foundation.Models;

namespace GigHub.Core.Foundation.Mapper.Mappings
{
    public class NotificationToNotificationDto : IMapping
    {
        public void Register(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<Notification, NotificationDto>().ReverseMap();
        }
    }
}