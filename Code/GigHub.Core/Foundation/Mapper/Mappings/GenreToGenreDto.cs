﻿using AutoMapper;
using GigHub.Core.Foundation.Dtos;
using GigHub.Core.Foundation.Models;

namespace GigHub.Core.Foundation.Mapper.Mappings
{
    public class GenreToGenreDto : IMapping
    {
        public void Register(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<Genre, GenreDto>().ReverseMap();
        }
    }
}