﻿using AutoMapper;
using GigHub.Core.Foundation.Dtos;
using GigHub.Core.Foundation.Models;

namespace GigHub.Core.Foundation.Mapper.Mappings
{
    public class ApplicationToApplicationDto : IMapping
    {
        public void Register(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<ApplicationUser, ApplicationUserDto>().ReverseMap();
        }
    }
}