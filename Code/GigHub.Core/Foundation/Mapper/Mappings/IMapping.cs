﻿using AutoMapper;

namespace GigHub.Core.Foundation.Mapper.Mappings
{
    public interface IMapping
    {
        void Register(IMapperConfigurationExpression cfg);
    }
}