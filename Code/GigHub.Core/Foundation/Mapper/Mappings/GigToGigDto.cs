﻿using AutoMapper;
using GigHub.Core.Foundation.Dtos;
using GigHub.Core.Foundation.Models;

namespace GigHub.Core.Foundation.Mapper.Mappings
{
    sealed public class GigToGigDto : IMapping
    {
        public void Register(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<Gig, GigDto>().ReverseMap();
        }
    }
}