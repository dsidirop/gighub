﻿namespace GigHub.Core.Foundation.Mapper
{
    public interface IMappingService
    {
        TDestination Map<TSource, TDestination>(TSource source);
    }
}