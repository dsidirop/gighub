﻿using System.Web.Mvc;
using GigHub.Core.Foundation.Results.MVC;
using GigHub.Core.Utilities;

namespace GigHub.Core.Foundation.Attributes.MVC
{
    public class ValidateModelMvcAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            try
            {
                var modelState = context.Controller.ViewData.ModelState;
                if (modelState.IsValid)
                    return;

                context.Result = HttpStatusWithContentResult.Bad(modelState.Stringify());
            }
            finally
            {
                base.OnActionExecuting(context);
            }
        }
    }
}