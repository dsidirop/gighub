﻿using System.Web.Mvc;
using GigHub.Core.Utilities;
using RequireJsNet;

// http://requirejsnet.veritech.io/setup.html

namespace GigHub.Core.Foundation.Attributes.MVC
{
    public class RequirejsGlobalOptionsFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var isAuthenticated = filterContext
                                      ?.RequestContext
                                      ?.HttpContext
                                      .Request
                                      .IsAuthenticated
                                  ?? false;

            var isDebugModeOn = UEnv.IsDebugModeOn; //todo  get this from di

            RequireJsOptions.Add("isDebugModeOn", isDebugModeOn, RequireJsOptionsScope.Global);
            RequireJsOptions.Add("isAuthenticated", isAuthenticated, RequireJsOptionsScope.Global);
        }
    }
}
