﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;

namespace GigHub.Core.Foundation.Attributes.MVC
{
    public class ValidateActionParametersMvcAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var parameters = context.ActionDescriptor.GetParameters();
            foreach (var parameter in parameters)
            {
                var argumentValue = context.ActionParameters[parameter.ParameterName];

                EvaluateValidationAttributes(
                    argument: argumentValue,
                    parameter: parameter,
                    modelState: context.Controller.ViewData.ModelState
                );
            }

            base.OnActionExecuting(context);
        }

        static private void EvaluateValidationAttributes(ParameterDescriptor parameter, object argument, ModelStateDictionary modelState)
        {
            parameter
                .GetCustomAttributes(typeof(ValidationAttribute), inherit: true)
                .OfType<ValidationAttribute>()
                .Where(validationAttribute => !validationAttribute.IsValid(argument))
                .Aggregate(
                    modelState,
                    (m, validationAttribute) =>
                    {
                        m.AddModelError(parameter.ParameterName, validationAttribute.FormatErrorMessage(parameter.ParameterName));
                        return m;
                    }
                );
        }
    }
}
