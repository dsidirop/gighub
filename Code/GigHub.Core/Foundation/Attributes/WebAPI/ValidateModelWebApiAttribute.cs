﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace GigHub.Core.Foundation.Attributes.WebAPI
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true)]
    public class ValidateModelWebApiAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            try
            {
                if (actionContext.ModelState.IsValid)
                    return;

                actionContext.Response = actionContext.Request.CreateErrorResponse( //todo  use json and specialize the response for ajax
                    statusCode: HttpStatusCode.BadRequest,
                    modelState: actionContext.ModelState
                );
            }
            finally
            {
                base.OnActionExecuting(actionContext);
            }
        }
    }
}