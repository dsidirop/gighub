using System;
using System.Reflection;

namespace GigHub.Core.Foundation.Dtos
{
 //json
    sealed public class GigDto
    {
        public int Id { get; set; }
        public string Venue { get; set; }
        public GenreDto Genre { get; set; }
        public DateTime DateTime { get; set; }
        public ApplicationUserDto Artist { get; set; }
    }
}
