using System.Reflection;

namespace GigHub.Core.Foundation.Dtos
{

    sealed public class FanFollowsArtistVerdictDto
    {
        public bool Following { get; set; }
    }
}