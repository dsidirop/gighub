﻿namespace GigHub.Core.Foundation.Dtos
{
    sealed public class AttendanceVerdictDto
    {
        public bool Going { get; set; }
    }
}