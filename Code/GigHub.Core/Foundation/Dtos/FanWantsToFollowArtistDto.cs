﻿using System.ComponentModel.DataAnnotations;

namespace GigHub.Core.Foundation.Dtos
{
    public class FanWantsToFollowArtistDto
    {
        [Required]
        public string ArtistId { get; set; }
    }
}