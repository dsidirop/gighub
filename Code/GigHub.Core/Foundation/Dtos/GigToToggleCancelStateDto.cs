using System.ComponentModel.DataAnnotations;

namespace GigHub.Core.Foundation.Dtos
{
    public class GigToToggleCancelStateDto
    {
        [Range(1, int.MaxValue, ErrorMessage = "The Genre field is required.")]
        public int GigId { get; set; }
        public bool CancelNotUncancel { get; set; }
    }
}