using System.Reflection;

namespace GigHub.Core.Foundation.Dtos
{
 //json
    sealed public class ApplicationUserDto
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}