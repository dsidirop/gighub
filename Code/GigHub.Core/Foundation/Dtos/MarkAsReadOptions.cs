using System.Collections.Generic;

namespace GigHub.Core.Foundation.Dtos
{
    sealed public class MarkAsReadOptions
    {
        public IEnumerable<int> Ids { get; set; } //dont convert this to array []
    }
}