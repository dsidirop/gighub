using System.Reflection;

namespace GigHub.Core.Foundation.Dtos
{
    sealed public class GenreDto
    {
        public byte Id { get; set; }
        public string Name { get; set; }
    }
}