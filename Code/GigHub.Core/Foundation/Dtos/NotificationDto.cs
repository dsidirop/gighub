using System;
using System.Reflection;

namespace GigHub.Core.Foundation.Dtos
{
 //json
    sealed public class NotificationDto
    {
        public int Id { get; set; }
        public string Type { get; set; } //1
        public DateTime DateTime { get; set; }

        public GigDto Gig { get; set; }
        public string OriginalVenue { get; set; }
        public GenreDto OriginalGenre { get; set; }
        public DateTime? OriginalDateTime { get; set; }
    }
}
//1 we prefer having the notificationtype serialized as a string in order to have it being readable in the js world