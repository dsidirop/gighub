﻿using System;
using System.Runtime.Serialization;

namespace GigHub.Core.Foundation.Exceptions
{
    [Serializable] //best practice per https://stackoverflow.com/a/100369/863651
    public class PlatformException : Exception //0
    {
        public PlatformException()
        {
        }

        public PlatformException(string message)
            : base(message)
        {
        }

        public PlatformException(string message, Exception inner)
            : base(message, inner)
        {
        }

        public PlatformException(SerializationInfo info, StreamingContext context) //1
            : base(info, context) //2
        {
        }

        //0 root exception class meant to be employed directly or indirectly by all custom exceptions of our platform
        //1 without this constructor deserialization will fail
        //2 the exception baseclass takes care of properly deserializing message stacktrace and so on
    }
}