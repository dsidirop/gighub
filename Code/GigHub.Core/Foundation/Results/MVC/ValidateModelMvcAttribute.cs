﻿using System;
using System.Net;
using System.Web.Mvc;
using GigHub.Core.Utilities;

namespace GigHub.Core.Foundation.Results.MVC
{
    sealed public class HttpStatusWithContentResult : ActionResult
    {
        private readonly string _content;
        private readonly string _mimeType;
        private readonly string _statusDescription;
        private readonly HttpStatusCode _statusCode;

        // static public ActionResult OK(string content = null) => new HttpStatusWithContentResult(HttpStatusCode.OK, content);
        // static public ActionResult NotFound(string content = null) => new HttpStatusWithContentResult(HttpStatusCode.NotFound, content);

        static public ActionResult Bad(string content = null) => new HttpStatusWithContentResult(
            content: content,
            statusCode: HttpStatusCode.BadRequest,
            onelinerShortStatusDescription: @"Invalid parameters"
        );

        private HttpStatusWithContentResult(HttpStatusCode statusCode, string onelinerShortStatusDescription, string content, string mimeType = null) //keep the order of these parameters asis
        {
            if (onelinerShortStatusDescription?.Contains("\n") ?? false) //0 vitalcheck
                throw new ArgumentException(nameof(onelinerShortStatusDescription));

            _content = (content ?? "").Trim();
            _mimeType = (mimeType ?? "text/plain").Trim();
            _statusCode = statusCode;
            _statusDescription = (onelinerShortStatusDescription ?? "").Trim();

            //0 statusdescription is used to set the response headers and the response headers are not allowed to contain newlines lest the
            //  response gets clogged altogether
            //
            //  http://stackoverflow.com/questions/11917212/why-does-adding-a-newline-to-statusdescription-in-asp-net-close-the-connection
            //
            //  one would expect that aspnet should have provisions in place to readily detect this issue and throw a fit like we do here however
            //  this is not the case for some mysterious reason   the end result of this omission? a entire chain of quirks and bugs which results
            //  in the controller getting invoked five or more times in a row via ajax at least when the browser being used is firefox
            //
            //  http://stackoverflow.com/questions/43897746/asp-net-controller-gets-called-repeatedly-by-firefox-if-httpstatuscoderesult-has
            //
            //  to counter all this we had to take matters into our own hands by using this utility class
        }

        public override void ExecuteResult(ControllerContext context)
        {
            var response = context.HttpContext.Response;

            response.StatusCode = (int)_statusCode;
            if (!string.IsNullOrWhiteSpace(_statusDescription))
            {
                response.StatusDescription = _statusDescription;
            }

            if (!string.IsNullOrWhiteSpace(_content))
            {
                context.HttpContext.Response.Write(_content);
                context.HttpContext.Response.ContentType = _mimeType;
            }

            U.Try(() => context.HttpContext.Response.Flush()); //0
            U.Try(() => context.HttpContext.Response.Close());

            //0 the other side might have closed the connection midflight and the flushing might haywire   we simply ignore any exception in this case
        }
    }
}