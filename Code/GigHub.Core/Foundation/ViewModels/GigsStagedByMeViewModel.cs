﻿using System.Collections.Generic;
using System.Web.Mvc;
using GigHub.Core.Controllers;
using GigHub.Core.Foundation.Models;
using GigHub.Core.Utilities.HtmlHelpers.Attributes;

namespace GigHub.Core.Foundation.ViewModels
{
    public class GigsStagedByMeViewModel
    {
        public IEnumerable<Gig> Gigs { get; set; }

        public FilteringForGigsStagedByMeOptions Filtering { get; set; } = new FilteringForGigsStagedByMeOptions();

        [FormSubmitMethod(FormMethod.Get)]
        [FormSubmitAction(nameof(GigsController.GigsStagedByMe))]
        [FormSubmitController(nameof(GigsController))]
        sealed public class FilteringForGigsStagedByMeOptions
        {
            [WithHtmlName(nameof(GigsStagedByMeOptions.Query))]
            public string SearchTerm { get; set; }

            //temp workaround   todo get rid of this once we fix XTextBox
            public string SearchTermHtmlName { get; set; } = nameof(GigsStagedByMeOptions.Query).ToLowerInvariant();
        }
    }
}