﻿using System.Web.Mvc;
using GigHub.Core.Controllers;
using GigHub.Core.Utilities.HtmlHelpers.Attributes;

namespace GigHub.Core.Foundation.ViewModels
{
    public class AttendingViewModel : GigsViewModelBase
    {
        public override IFilteringOptions FilteringOptions { get; set; } = new FilteringViaGigsListOptions();

        [FormSubmitMethod(FormMethod.Get)]
        [FormSubmitAction(nameof(GigsController.Attending))]
        [FormSubmitController(nameof(GigsController))]
        public class FilteringViaGigsListOptions : IFilteringOptions
        {
            [WithHtmlName(nameof(AttendingGigsListOptions.Query))]
            public string SearchTerm { get; set; }

            //temp workaround
            public string SearchTermHtmlName { get; set; } = nameof(AttendingGigsListOptions.Query).ToLowerInvariant();
        }
    }
}