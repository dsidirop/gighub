﻿using System.ComponentModel.DataAnnotations;

namespace GigHub.Core.Foundation.ViewModels
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}