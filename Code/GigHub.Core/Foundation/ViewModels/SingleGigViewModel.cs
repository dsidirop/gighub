﻿using GigHub.Core.Foundation.Models;
using System.ComponentModel.DataAnnotations;

namespace GigHub.Core.Foundation.ViewModels
{
    public class SingleGigViewModel
    {
        [Required]
        public Gig Gig { get; set; }
        public bool Following { get; set; }
        public bool Attending { get; set; }
    }
}