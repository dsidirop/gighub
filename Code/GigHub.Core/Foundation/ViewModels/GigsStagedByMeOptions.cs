﻿namespace GigHub.Core.Foundation.ViewModels
{
    public class GigsStagedByMeOptions
    {
        public string Query { get; set; }
    }
}