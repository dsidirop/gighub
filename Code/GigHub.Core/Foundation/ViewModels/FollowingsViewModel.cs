﻿using GigHub.Core.Foundation.Models;
using System.Collections.Generic;

namespace GigHub.Core.Foundation.ViewModels
{
    public class FollowingsViewModel
    {
        public IEnumerable<ApplicationUser> Artists { get; set; }
    }
}