﻿using GigHub.Core.Foundation.Models;
using GigHub.Core.Utilities.DataAnnotationAttributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GigHub.Core.Foundation.ViewModels
{
    public class GigFormViewModel
    {
        public int Id { get; set; } //hiddenfield

        [Required]
        [FutureDateValidator]
        public string Date { get; set; }

        [Required] //todo   [TimeValidator] gh0003 for some reason this doesnt check time the way it should
        public string Time { get; set; }

        [Required]
        public string Venue { get; set; }

        [Required(ErrorMessage = "The Genre field is required.")]
        [Range(1, int.MaxValue, ErrorMessage = "The Genre field is required.")]
        public byte GenreId { get; set; }

        public string Title => Id == 0 ? "Create Gig" : "Edit Gig"; //helper
        public DateTime GetDateTime() => Date != null && Time != null ? DateTime.Parse($"{Date} {Time}") : DateTime.MinValue; //keepmethod

        public string PossibleError { get; set; }
        public IEnumerable<Genre> Genres { get; set; } //helper
    }
}