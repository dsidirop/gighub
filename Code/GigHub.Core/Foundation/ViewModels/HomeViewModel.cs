﻿using System.Web.Mvc;
using GigHub.Core.Controllers;
using GigHub.Core.Utilities.HtmlHelpers.Attributes;

namespace GigHub.Core.Foundation.ViewModels
{
    public class HomeViewModel : GigsViewModelBase
    {
        public override IFilteringOptions FilteringOptions { get; set; } = new FilteringViaGigsListOptions();

        [FormSubmitMethod(FormMethod.Get)]
        [FormSubmitAction(nameof(HomeController.GigsList))]
        [FormSubmitController(nameof(HomeController))]
        public class FilteringViaGigsListOptions : IFilteringOptions
        {
            [WithHtmlName(nameof(HomeGigsListOptions.Query))]
            public string SearchTerm { get; set; }

            //temp workaround
            public string SearchTermHtmlName { get; set; } = nameof(HomeGigsListOptions.Query).ToLowerInvariant();
        }
    }
}