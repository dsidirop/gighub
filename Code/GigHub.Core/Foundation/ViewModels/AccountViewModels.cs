﻿using System.ComponentModel.DataAnnotations;

namespace GigHub.Core.Foundation.ViewModels
{
    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}
