﻿using System.Collections.Generic;
using System.Linq;
using GigHub.Core.Foundation.Models;

namespace GigHub.Core.Foundation.ViewModels
{
    public abstract class GigsViewModelBase : IGigsViewModel
    {
        public virtual string UserId { get; set; }

        public virtual string Heading { get; set; }

        public virtual IEnumerable<Gig> UpcomingGigs { get; set; }

        public virtual ILookup<int, Attendance> AttendancesOfUser { get; set; }

        public virtual ILookup<string, FanFollowingArtist> ArtistsFollowedByUser { get; set; }

        public abstract IFilteringOptions FilteringOptions { get; set; } // <- specialized by derived classes
    }

    public interface IGigsViewModel
    {
        string UserId { get; set; }

        string Heading { get; set; }

        IEnumerable<Gig> UpcomingGigs { get; set; }

        ILookup<int, Attendance> AttendancesOfUser { get; set; }

        ILookup<string, FanFollowingArtist> ArtistsFollowedByUser { get; set; }

        IFilteringOptions FilteringOptions { get; set; }
    }

    public interface IFilteringOptions
    {
        string SearchTerm { get; set; }
        string SearchTermHtmlName { get; set; } //temp workaround
    }
}