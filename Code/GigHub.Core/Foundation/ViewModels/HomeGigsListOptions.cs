﻿namespace GigHub.Core.Foundation.ViewModels
{
    public class HomeGigsListOptions
    {
        public string Query { get; set; }
    }
}