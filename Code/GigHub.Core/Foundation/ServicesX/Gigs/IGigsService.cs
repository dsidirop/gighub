﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GigHub.Core.Foundation.Models;

namespace GigHub.Core.Foundation.ServicesX.Gigs
{
    public interface IGigsService
    {
        Task<bool> UpsertGig(int gigId, string venue, byte genreId, string artistId, DateTime dateTime);
        Task<Gig> GetGigAsync(int gigId, bool includeArtist = false, bool includeGenre = false, bool includeAttendances = false, bool includeArtistFollowers = false, bool includeAttendancesAttendees = false);
        Task SetGigCancelStatusAsync(string userId, int gigId, bool cancelNotUncancel);
        Task<IEnumerable<Gig>> GetGigsAsync(string artistId = null, string searchTerm = null, bool onlyFutureOnesNotAll = false, bool includeCancelled = true);
        Task<bool> ToggleGigAttendanceForUserAsync(string userId, int gigId);
        Task<IEnumerable<Genre>> GetAllGenresAsync();
        Task<IEnumerable<Attendance>> GetAttendancesOfUserAsync(string userId, string searchTerm = null, IEnumerable<int> gigIds = null);
    }
}