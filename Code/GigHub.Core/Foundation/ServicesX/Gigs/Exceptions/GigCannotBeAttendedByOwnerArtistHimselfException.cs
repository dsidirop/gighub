﻿using System;
using System.Runtime.Serialization;
using GigHub.Core.Foundation.Exceptions;

namespace GigHub.Core.Foundation.ServicesX.Gigs.Exceptions
{
    [Serializable]
    public class GigCannotBeAttendedByOwnerArtistHimselfException : PlatformException
    {
        public GigCannotBeAttendedByOwnerArtistHimselfException()
            : this(@"An Artist cannot attend his own Gig as a fan")
        {
        }

        public GigCannotBeAttendedByOwnerArtistHimselfException(string message)
            : base(message)
        {
        }

        public GigCannotBeAttendedByOwnerArtistHimselfException(string message, Exception inner)
            : base(message, inner)
        {
        }

        public GigCannotBeAttendedByOwnerArtistHimselfException(SerializationInfo info, StreamingContext context) //0
            : base(info, context)
        {
            //0 without this constructor deserialization will fail
        }
    }
}