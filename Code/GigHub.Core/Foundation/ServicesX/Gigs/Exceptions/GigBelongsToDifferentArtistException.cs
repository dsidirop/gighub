﻿using System;
using System.Runtime.Serialization;
using GigHub.Core.Foundation.Exceptions;

namespace GigHub.Core.Foundation.ServicesX.Gigs.Exceptions
{
    [Serializable]
    public class GigBelongsToDifferentArtistException : PlatformException
    {
        public GigBelongsToDifferentArtistException()
            : this(@"Gig belonds to a different Artist")
        {
        }

        public GigBelongsToDifferentArtistException(string message)
            : base(message)
        {
        }

        public GigBelongsToDifferentArtistException(string message, Exception inner)
            : base(message, inner)
        {
        }

        public GigBelongsToDifferentArtistException(SerializationInfo info, StreamingContext context) //0
            : base(info, context)
        {
            //0 without this constructor deserialization will fail
        }
    }
}