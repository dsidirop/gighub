﻿using System;
using System.Runtime.Serialization;
using GigHub.Core.Foundation.Exceptions;

namespace GigHub.Core.Foundation.ServicesX.Gigs.Exceptions
{
    [Serializable] //best practice per https://stackoverflow.com/a/100369/863651
    public class GigAttendanceTweakingFailedException : PlatformException
    {
        public GigAttendanceTweakingFailedException()
            : this(@"Failed to set new attendance setting for specified Gig")
        {
        }

        public GigAttendanceTweakingFailedException(string message)
            : base(message)
        {
        }

        public GigAttendanceTweakingFailedException(string message, Exception inner)
            : base(message, inner)
        {
        }

        public GigAttendanceTweakingFailedException(SerializationInfo info, StreamingContext context) //0
            : base(info, context)
        {
            //0 without this constructor deserialization will fail
        }
    }
}