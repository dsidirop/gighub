﻿using System;
using System.Runtime.Serialization;
using GigHub.Core.Foundation.Exceptions;

namespace GigHub.Core.Foundation.ServicesX.Gigs.Exceptions
{
    [Serializable] //best practice per https://stackoverflow.com/a/100369/863651
    public class GigNotFoundException : PlatformException
    {
        public GigNotFoundException()
            : this(@"Gig Doesn't Exist")
        {
        }

        public GigNotFoundException(string message)
            : base(message)
        {
        }

        public GigNotFoundException(string message, Exception inner)
            : base(message, inner)
        {
        }

        public GigNotFoundException(SerializationInfo info, StreamingContext context) //0
            : base(info, context)
        {
            //0 without this constructor deserialization will fail
        }
    }
}