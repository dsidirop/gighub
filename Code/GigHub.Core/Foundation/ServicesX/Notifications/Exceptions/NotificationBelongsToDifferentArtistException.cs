﻿using System;
using System.Runtime.Serialization;
using GigHub.Core.Foundation.Exceptions;

namespace GigHub.Core.Foundation.ServicesX.Notifications.Exceptions
{
    [Serializable] //best practice per https://stackoverflow.com/a/100369/863651
    public class NotificationBelongsToDifferentArtistException : PlatformException
    {
        public NotificationBelongsToDifferentArtistException()
            : this(@"Failed to set new attendance setting for specified Gig")
        {
        }

        public NotificationBelongsToDifferentArtistException(string message)
            : base(message)
        {
        }

        public NotificationBelongsToDifferentArtistException(string message, Exception inner)
            : base(message, inner)
        {
        }

        public NotificationBelongsToDifferentArtistException(SerializationInfo info, StreamingContext context) //0
            : base(info, context)
        {
            //0 without this constructor deserialization will fail
        }
    }
}