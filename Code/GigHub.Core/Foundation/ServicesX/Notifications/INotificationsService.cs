﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GigHub.Core.Foundation.Models;

namespace GigHub.Core.Foundation.ServicesX.Notifications
{
    public interface INotificationsService
    {
        Task MarkNotificationsAsReadAsync(string userId, IEnumerable<int> notificationsIds = null);
        Task<IEnumerable<UserHasReadNotification>> GetUnreadNotificationsAsync(string userId, IEnumerable<int> notificationsIds = null);
    }
}