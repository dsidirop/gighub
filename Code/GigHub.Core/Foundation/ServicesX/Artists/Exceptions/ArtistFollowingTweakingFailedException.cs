﻿using System;
using System.Runtime.Serialization;
using GigHub.Core.Foundation.Exceptions;

namespace GigHub.Core.Foundation.ServicesX.Artists.Exceptions
{
    [Serializable] //best practice per https://stackoverflow.com/a/100369/863651
    public class ArtistFollowingTweakingFailedException : PlatformException
    {
        public ArtistFollowingTweakingFailedException()
            : this(@"Failed to set new following-setting for specified Artist")
        {
        }

        public ArtistFollowingTweakingFailedException(string message)
            : base(message)
        {
        }

        public ArtistFollowingTweakingFailedException(string message, Exception inner)
            : base(message, inner)
        {
        }

        public ArtistFollowingTweakingFailedException(SerializationInfo info, StreamingContext context) //0
            : base(info, context)
        {
            //0 without this constructor deserialization will fail
        }
    }
}