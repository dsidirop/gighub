﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GigHub.Core.Foundation.Models;

namespace GigHub.Core.Foundation.ServicesX.Artists
{
    public interface IArtistsService
    {
        Task<bool> ToggleFollowingAsync(string fanId, string artistId);
        Task<IEnumerable<FanFollowingArtist>> GetFollowingsAsync(string userId, IEnumerable<string> idsOfArtistsHostingGigsUserIsAttending = null);
    }
}