﻿using System;

// ReSharper disable UnusedAutoPropertyAccessor.Local

namespace GigHub.Core.Foundation.Models
{
    public class UserHasReadNotification
    {
        protected UserHasReadNotification() //neededbyef
        {
        }

        public UserHasReadNotification(ApplicationUser user, Notification notification)
        {
            if (user == null) throw new NullReferenceException(nameof(user));
            if (notification == null) throw new NullReferenceException(nameof(notification));

            User = user;
            Notification = notification;
        }

        public bool IsRead { set; get; }

        public string UserId { get; set; }
        public int NotificationId { get; set; }

        public ApplicationUser User { get; private set; }
        public Notification Notification { get; private set; }
    }
}
