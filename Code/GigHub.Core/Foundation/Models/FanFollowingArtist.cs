﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GigHub.Core.Foundation.Models
{
    public class FanFollowingArtist
    {
        [Key]
        [Required]
        [Column(Order = 1)]
        public string FanId { get; set; }

        [Key]
        [Required]
        [Column(Order = 2)]
        public string ArtistId { get; set; }

        //[Required]
        [Column(Order = 3)]
        [ForeignKey(nameof(FanId))]
        public ApplicationUser Fan { get; set; }

        [Column(Order = 4)]
        [ForeignKey(nameof(ArtistId))]
        public ApplicationUser Artist { get; set; }
    }
}