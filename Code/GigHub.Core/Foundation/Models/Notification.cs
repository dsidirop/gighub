﻿using GigHub.Core.Utilities;
using System;
using System.Reflection;

namespace GigHub.Core.Foundation.Models
{

    public class Notification
    {
        static public Notification ForGigCreated(Gig gig)
            => new Notification(gig, ENotificationType.GigCreated);

        static public Notification ForGigCancelled(Gig gig)
            => new Notification(gig, ENotificationType.GigCanceled);

        static public Notification ForGigUncancelled(Gig gig)
            => new Notification(gig, ENotificationType.GigUncanceled);

        static public Notification ForGigUpdated(Gig newGig, string originalVenue, DateTime? originalDateTime, Genre originalGenre)
            => new Notification(newGig, ENotificationType.GigUpdated, originalVenue, originalDateTime, originalGenre);

        protected Notification()
        {
        }

        static private readonly string DateFormatForComparison = $"{UDates.GigDateFormat} {UDates.GigTimeOfDayFormat}";
        private Notification(Gig gig, ENotificationType notificationType, string originalVenue = null, DateTime? originalDateTime = null, Genre originalGenre = null)
        {
            if (gig == null) throw new NullReferenceException(nameof(gig));
            if (notificationType != ENotificationType.GigUpdated && originalVenue != null) throw new ArgumentException(nameof(originalVenue));
            if (notificationType != ENotificationType.GigUpdated && originalGenre != null) throw new ArgumentException(nameof(originalGenre));
            if (notificationType != ENotificationType.GigUpdated && originalDateTime != null) throw new ArgumentException(nameof(originalDateTime));

            Gig = gig;
            Type = notificationType;
            DateTime = System.DateTime.Now; //0 todo utc
            OriginalVenue = originalVenue == gig.Venue ? null : originalVenue;
            OriginalGenre = originalGenre?.Id == gig.GenreId ? null : originalGenre;
            OriginalDateTime = originalDateTime?.ToString(DateFormatForComparison) == gig.DateTime.ToString(DateFormatForComparison) ? null : originalDateTime;
        }
        //0 todo  use utcnow and improve the implementation of the guis to have it translate the utcnow to localtime

        public int Id { get; set; }
        public DateTime? DateTime { get; private set; } //updatetime
        public ENotificationType Type { get; private set; }

        public Genre OriginalGenre { get; private set; }
        public string OriginalVenue { get; private set; }
        public DateTime? OriginalDateTime { get; private set; }

        public Gig Gig { get; private set; }
    }
}