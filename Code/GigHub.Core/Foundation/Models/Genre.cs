﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GigHub.Core.Foundation.Models
{
    public class Genre
    {
        [Key]
        [Required]
        [Column(Order = 1)]
        public byte Id { get; set; } //not going to have more than 255 genres

        [Required]
        [MaxLength(128)]
        [StringLength(128)] //dont use nvarchar(128)
        [Column(TypeName = "nvarchar", Order = 2)]
        public string Name { get; set; }
    }
}