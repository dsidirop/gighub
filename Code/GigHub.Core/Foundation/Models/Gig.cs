﻿using GigHub.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GigHub.Core.Foundation.Models
{
    public class Gig
    {
        public Gig()
        {
            Attendances = new List<Attendance>(capacity: 16);
        }

        public int Id { get; set; }
        public byte GenreId { get; set; } //standard practice to use ids in order to optimize certain db operations
        public string ArtistId { get; set; } //standard practice to use ids in order to optimize certain db operations

        public Genre Genre { get; set; }
        public string Venue { get; set; }
        public DateTime DateTime { get; set; }
        public bool GotCancelled { get; private set; }
        public ApplicationUser Artist { get; set; }
        public ICollection<Attendance> Attendances { get; private set; }

        public Gig SetCancelStatus(bool cancelNotUncancel)
        {
            GotCancelled = cancelNotUncancel; //order

            var notification = cancelNotUncancel ? Notification.ForGigCancelled(this) : Notification.ForGigUncancelled(this); //order
            Attendances
                .Select(a => a.Attendee) //order
                .ToList() //snapshot
                .ForEach(user => user.Notify(notification));

            return this;
        }

        static private readonly string DateFormatForComparison = $"{UDates.GigDateFormat} {UDates.GigTimeOfDayFormat}";
        public bool TryUpdate(string newVenue, DateTime? newDateTime, Genre newGenre)
        {
            newGenre = newGenre ?? Genre;
            newVenue = (newVenue ?? Venue).Trim();
            newDateTime = newDateTime ?? DateTime;

            if (Venue == newVenue
                && Genre.Id == newGenre.Id
                && DateTime.ToString(DateFormatForComparison) == newDateTime.Value.ToString(DateFormatForComparison)) return false; //0

            var originalVenueSnapshot = Venue; //order
            var originalGenreSnapshot = Genre;
            var originalDateTimeSnapshot = DateTime;

            Venue = newVenue; //order
            Genre = newGenre; //crucial
            GenreId = newGenre.Id;
            DateTime = newDateTime.Value;

            var notification = Notification.ForGigUpdated(this, originalVenueSnapshot, originalDateTimeSnapshot, originalGenreSnapshot); //order
            Attendances.Select(a => a.Attendee) //order
                .ToList() //snapshot
                .ForEach(user => user.Notify(notification));

            return true;
        }
        //0 notice that we set the dateformat explicitly because the newdate stems from model.getdatetime() which yanks off the seconds from the date   thusly if we simply compare
        //  datetime == newdatetime we are always going to be getting false no matter what
    }
}