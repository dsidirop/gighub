﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace GigHub.Core.Foundation.Models
{
    public class ApplicationUser : IdentityUser //0
    {
        public string Name { get; set; }

        public ICollection<FanFollowingArtist> Followees { get; set; }
        public ICollection<FanFollowingArtist> Followers { get; set; }
        public ICollection<UserHasReadNotification> UserNotifications { get; set; } //1

        public ApplicationUser()
        {
            Followees = new List<FanFollowingArtist>(capacity: 16);
            Followers = new List<FanFollowingArtist>(capacity: 16);
            UserNotifications = new List<UserHasReadNotification>(capacity: 16);
        }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager) //2
            => await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie).ConfigureAwait(continueOnCapturedContext: false);

        public void Notify(Notification notification)
            => UserNotifications.Add(new UserHasReadNotification(this, notification));
    }
}
//0 you can add profile data for the user by adding more properties to your applicationuser class please visit http://go.microsoft.com/fwlink/?linkid=317594 to learn more
//  also note the authenticationtype must match the one defined in cookieauthenticationoptions.authenticationtype
//1 regarding the collection of usernotifications we had to tweak our fluentapi codefirst approach via applicationdbcontext.onmodelcreating
//  so that ef will be informed that usernotifications is simply a navigation property which we use as a reversemapping utility  this utility
//  allows us to gain access from each appuser instance to the usernotifications related to said appuser
//2 add custom user claims here