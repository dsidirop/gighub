﻿using GigHub.Core.Foundation.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GigHub.Core.Foundation.Persistence
{
    public interface IGenresRepository
    {
        Task<Genre> GetAsync(byte genreId);
        Task<IEnumerable<Genre>> GetAllAsync();
    }
}