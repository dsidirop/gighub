﻿using GigHub.Core.Foundation.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GigHub.Core.Foundation.Persistence
{
    public interface IArtistsFollowingsRepository
    {
        void Add(string fanId, string artistId);
        void Delete(FanFollowingArtist fanFollowingArtist);
        Task<FanFollowingArtist> GetSingleAsync(string fanId, string artistId);
        Task<IEnumerable<FanFollowingArtist>> GetMultipleAsync(string fanId, IEnumerable<string> artistsId = null);
    }
}