﻿using GigHub.Core.Foundation.Models;
using System.Data.Entity;
using System.Threading.Tasks;

namespace GigHub.Core.Foundation.Persistence
{
    public interface IApplicationDbContext
    {
        int SaveChanges();
        Task<int> SaveChangesAsync();

        DbSet<Gig> Gigs { get; set; }
        DbSet<Genre> Genres { get; set; }
        DbSet<Attendance> Attendances { get; set; }
        DbSet<Notification> Notifications { get; set; }
        DbSet<FanFollowingArtist> ArtistsFollowings { get; set; }
        DbSet<UserHasReadNotification> UserHasReadNotifications { get; set; }
    }
}