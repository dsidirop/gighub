﻿using System.Threading.Tasks;

namespace GigHub.Core.Foundation.Persistence
{
    public interface IUnitOfWork
    {
        IGigsRepository GigsRepo { get; }
        IGenresRepository GenresRepo { get; }
        IAttendancesRepository AttendancesRepo { get; }
        IArtistsFollowingsRepository ArtistsFollowingsRepo { get; }
        IUserHasReadNotificationsRepository UserHasReadNotificationsRepo { get; }

        void CommitChanges();
        Task CommitChangesAsync();
    }
}