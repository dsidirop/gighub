﻿using GigHub.Core.Foundation.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GigHub.Core.Foundation.Persistence
{
    public interface IUserHasReadNotificationsRepository
    {
        IApplicationDbContext Context { get; set; }

        Task<IEnumerable<UserHasReadNotification>> GetUnreadNotificationsAsync(string userId, IEnumerable<int> ids = null);
    }
}