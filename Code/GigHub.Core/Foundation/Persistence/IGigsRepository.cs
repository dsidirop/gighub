﻿using GigHub.Core.Foundation.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GigHub.Core.Foundation.Persistence
{
    public interface IGigsRepository
    {
        IApplicationDbContext Context { get; set; }

        void Add(Gig gig);
        Task<IEnumerable<Gig>> GetMultipleAsync(string artistId = null, string searchTerm = null, bool onlyFutureOnesNotAll = false, bool includeCancelled = true);
        Task<Gig> GetSingleAsync(int gigId, bool includeGenre = false, bool includeAttendances = false, bool includeAttendancesAttendees = false, bool includeArtist = false, bool includeArtistFollowers = false);
    }
}

