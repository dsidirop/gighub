﻿using GigHub.Core.Foundation.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GigHub.Core.Foundation.Persistence
{
    public interface IAttendancesRepository
    {
        IApplicationDbContext Context { get; set; }

        void Add(string userId, int gigId);
        void Delete(Attendance attendance);
        Task<Attendance> GetSingleAsync(string userId, int gigId);
        Task<IEnumerable<Attendance>> GetAllMatchingAsync(string userId, string searchTerm = null, IEnumerable<int> gigIds = null);
    }
}