using System;
using System.Collections.Generic;
using System.Web.Http.Metadata.Providers;

namespace GigHub.Core.Foundation.ModelBinders.WebAPI
{
    sealed public class CustomModelMetadataProviderWebApi : DataAnnotationsModelMetadataProvider
    {
        protected override CachedDataAnnotationsModelMetadata CreateMetadataFromPrototype(CachedDataAnnotationsModelMetadata prototype, Func<object> modelAccessor)
        {
            var metadata = base.CreateMetadataFromPrototype(prototype, modelAccessor);
            metadata.ConvertEmptyStringToNull = false;
            return metadata;
        }

        protected override CachedDataAnnotationsModelMetadata CreateMetadataPrototype(IEnumerable<Attribute> attributes, Type containerType, Type modelType, string propertyName)
        {
            var metadata = base.CreateMetadataPrototype(attributes, containerType, modelType, propertyName);
            metadata.ConvertEmptyStringToNull = false;
            return metadata;
        }

        //0 to future maintainers    respect the emptystring in ajaxstrings aka "{foo:''}" gets converted into foo="" instead of null which is
        //  to future maintainers    the default and completely wrong
        //  to future maintainers
        //  to future maintainers                                         http://stackoverflow.com/a/36558116/863651
    }
}
