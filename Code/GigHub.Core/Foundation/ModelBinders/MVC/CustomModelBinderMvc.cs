using System.Web.Mvc;

namespace GigHub.Core.Foundation.ModelBinders.MVC
{
    sealed public class CustomModelBinderMvc : DefaultModelBinder //0
    {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            bindingContext.ModelMetadata.ConvertEmptyStringToNull = false;
            Binders = new ModelBinderDictionary { DefaultBinder = this };
            return base.BindModel(controllerContext, bindingContext);
        }
    }
    //0 respect empty ajaxstrings aka "{ foo: '' }" gets converted to foo="" instead of null  http://stackoverflow.com/a/12734370/863651
}