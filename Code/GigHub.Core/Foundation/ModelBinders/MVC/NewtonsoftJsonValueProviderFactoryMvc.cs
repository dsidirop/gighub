// ReSharper disable RedundantCast

using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.IO;
using System.Runtime.Serialization;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

//to future maintainers   this is meant to be used for deserializing parameters which get fed into csharp actions of mvc controllers
//to future maintainers   apart from the general advantages of using newtonsoftjson deserializer we also employ a vital bugfix which
//to future maintainers   allows us to accurately empty json arrays "{ foo: [] }" into empty csharp arrays aka  new object[0]   if we
//to future maintainers   omit this techqique then these arrays turn out as null which is completely and utterly wrong and undesirable
//to future maintainers
//to future maintainers                                   http://stackoverflow.com/a/23405148/863651
//to future maintainers

namespace GigHub.Core.Foundation.ModelBinders.MVC
{
    sealed public class NewtonsoftJsonValueProviderFactoryMvc : ValueProviderFactory //parameter deserializer
    {
        public override IValueProvider GetValueProvider(ControllerContext controllerContext)
        {
            if (controllerContext == null)
                throw new ArgumentNullException(nameof(controllerContext));

            if (!controllerContext.HttpContext.Request.ContentType.StartsWith("application/json", StringComparison.OrdinalIgnoreCase))
                return null;

            try
            {
                using (var streamReader = new StreamReader(controllerContext.HttpContext.Request.InputStream))
                using (var jsonReader = new JsonTextReader(streamReader))
                {
                    if (!jsonReader.Read())
                        return null;

                    var jsonObject = jsonReader.TokenType == JsonToken.StartArray //0
                        ? (object) JsonSerializer.Deserialize<List<ExpandoObject>>(jsonReader)
                        : (object) JsonSerializer.Deserialize<ExpandoObject>(jsonReader);

                    return new DictionaryValueProvider<object>(AddToBackingStore(jsonObject), InvariantCulture); //1
                }
            }
            catch (JsonException ex)
            {
                throw new JsonDeserializationOfParametersFailedException(ex.Message);
            }
        }
        static private readonly CultureInfo InvariantCulture = CultureInfo.InvariantCulture;
        static private readonly JsonSerializer JsonSerializer = new JsonSerializer //newtonsoft
        {
            Converters =
            {
                new ExpandoObjectConverter(),
                new IsoDateTimeConverter {Culture = InvariantCulture} //vital invariantculture
            }
        };
        //0 use jsonnet to deserialize object to a dynamic expando object  if we start with a [ treat this as an array
        //1 return the object in a dictionary value provider which mvc can understand

        static private IDictionary<string, object> AddToBackingStore(object value, string prefix = "", IDictionary<string, object> backingStore = null)
        {
            backingStore = backingStore ?? new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);

            if (value is IDictionary<string, object> d)
            {
                foreach (var entry in d)
                {
                    AddToBackingStore(entry.Value, MakePropertyKey(prefix, entry.Key), backingStore);
                }
                return backingStore;
            }

            if (value is IList l)
            {
                if (l.Count == 0) //0 here be dragons
                {
                    backingStore[prefix] = new object[0]; //0 here be dragons
                }
                else
                {
                    for (var i = 0; i < l.Count; i++)
                    {
                        AddToBackingStore(l[i], MakeArrayKey(prefix, i), backingStore);
                    }
                }
                return backingStore;
            }

            backingStore[prefix] = value;
            return backingStore;
        }

        static private string MakeArrayKey(string prefix, int index) => $"{prefix}[{index.ToString(CultureInfo.InvariantCulture)}]";
        static private string MakePropertyKey(string prefix, string propertyName) => string.IsNullOrEmpty(prefix) ? propertyName : $"{prefix}.{propertyName}";

        [Serializable] //best practice per https://stackoverflow.com/a/100369/863651
        public class JsonDeserializationOfParametersFailedException : Exception //0
        {
            public JsonDeserializationOfParametersFailedException()
            {
            }

            public JsonDeserializationOfParametersFailedException(string message)
                : base(message)
            {
            }

            public JsonDeserializationOfParametersFailedException(string message, Exception inner)
                : base(message, inner)
            {
            }

            public JsonDeserializationOfParametersFailedException(SerializationInfo info, StreamingContext context) //1
                : base(info, context) //2
            {
            }

            //0 root exception class meant to be employed directly or indirectly by all custom exceptions of our platform
            //1 without this constructor deserialization will fail
            //2 the exception baseclass takes care of properly deserializing message stacktrace and so on
        }

        //0 here be dragons      its vital to deserialize empty jsarrays "{ foo: [] }" to empty csharp array aka new object[0]
        //0 here be dragons      without this tweak we would get null which is completely wrong
    }
}