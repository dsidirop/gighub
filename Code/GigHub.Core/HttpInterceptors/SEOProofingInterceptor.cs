﻿using System;
using System.Text.RegularExpressions;
using System.Web;
using GigHub.Core.Utilities;

namespace GigHub.Core.HttpInterceptors
{
    sealed internal class SeoProofingInterceptor // https://stackoverflow.com/a/11647303/863651
    {
        private readonly MvcApplication _app;

        public SeoProofingInterceptor(MvcApplication app)
        {
            _app = app;
        }

        public void Init()
        {
            _app.BeginRequest += Application_BeginRequest;
        }

        static private void Application_BeginRequest(object sender, EventArgs ea)
        {
            try
            {
                var currentRequest = HttpContext.Current;
                var currentRequestSpecs = currentRequest.Request;

                var isGet = currentRequestSpecs
                    .RequestType
                    .ToLowerInvariant()
                    .Contains("get");
                if (!isGet)
                    return;

                var absolutePathOfUrl = currentRequestSpecs.Url.AbsolutePath;
                if (absolutePathOfUrl.Contains(".")) //1 images css etc
                    return;

                var queryOfUrl = currentRequestSpecs.Url.Query;
                var schemeOfUrl = currentRequestSpecs.Url.Scheme;
                var authorityOfUrl = currentRequestSpecs.Url.Authority;
                var originalUrlWithoutQuery = $@"{schemeOfUrl}://{authorityOfUrl}{absolutePathOfUrl}";
                if (!AtLeastOneUppercaseLetterRegex.IsMatch(originalUrlWithoutQuery))
                    return;

                var newUrlLowercased = originalUrlWithoutQuery.ToLower() + queryOfUrl; //2

                var response = currentRequest.Response;
                response.Clear(); //order
                response.Status = @"301 Moved Permanently"; //order
                response.AddHeader(@"Location", newUrlLowercased);
                response.End();
            } // ReSharper disable once EmptyGeneralCatchClause
            catch
            {
            }

            //1 we dont want to redirect on posts or images/css/js
            //2 you dont want to change casing on query strings
        }

        static private readonly Regex AtLeastOneUppercaseLetterRegex = new Regex(
            pattern: @"[A-Z]",
            options: UEnv.IsDebugModeOn ? RegexOptions.None : RegexOptions.Compiled //we only use compiled mode in production as it will slow down server startup alot in debug mode
        );
    }
}