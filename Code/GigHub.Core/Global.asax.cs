﻿using System.Web;
using GigHub.Core.HttpInterceptors;
using GigHub.Core.ViaGlobalAsax.Generic;
using GigHub.Core.ViaGlobalAsax.MVCSpecific;
using GigHub.Core.ViaGlobalAsax.WebAPISpecific;

namespace GigHub.Core
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            GenericSetup.Setup(); //   order
            WebApiSetup.Setup(); //    order
            MvcSetup.Setup(); //       order
        }

        public override void Init()
        {
            try
            {
                new SeoProofingInterceptor(this).Init();
            }
            finally
            {
                base.Init();
            }
        }
    }
}