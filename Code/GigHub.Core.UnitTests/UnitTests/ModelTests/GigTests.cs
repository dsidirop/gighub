﻿using FluentAssertions;
using GigHub.Core.Foundation.Models;
using GigHub.Core.UnitTests.Utilities;
using GigHub.Core.Utilities;
using NUnit.Framework;
using System;
using System.Globalization;
using System.Linq;

namespace GigHub.Core.UnitTests.UnitTests.ModelTests
{
    [TestFixture]
    public class GigTests
    {
        static private readonly Tuple<string, bool>[] SetCancelStatusCases; //must be static
        static private readonly Tuple<string, Gig, string, DateTime?, Genre, bool, Tuple<string, DateTime?, Genre>>[] TryUpdateCases; //must be static

        static GigTests() //dont move this into [onetimesetup]
        {
            SetCancelStatusCases = new[]
            {
                new Tuple<string, bool>("000", false),
                new Tuple<string, bool>("001", true)
            };

            TryUpdateCases = new[]
            {
                new Tuple<string, Gig, string, DateTime?, Genre, bool, Tuple<string, DateTime?, Genre>>("000", Artifacts.SpawnGig(Artifacts.PrincipalOfUser1), null, null, null, false, new Tuple<string, DateTime?, Genre>(null, null, null)),
                new Tuple<string, Gig, string, DateTime?, Genre, bool, Tuple<string, DateTime?, Genre>>("001", Artifacts.SpawnGig(Artifacts.PrincipalOfUser1), Artifacts.DummyGigOfUser1.Venue, null, null, false, new Tuple<string, DateTime?, Genre>(null, null, null)),
                new Tuple<string, Gig, string, DateTime?, Genre, bool, Tuple<string, DateTime?, Genre>>("002", Artifacts.SpawnGig(Artifacts.PrincipalOfUser1), null, Artifacts.DummyGigOfUser1.DateTime, null, false, new Tuple<string, DateTime?, Genre>(null, null, null)),
                new Tuple<string, Gig, string, DateTime?, Genre, bool, Tuple<string, DateTime?, Genre>>("003", Artifacts.SpawnGig(Artifacts.PrincipalOfUser1), null, null, Artifacts.DummyGigOfUser1.Genre, false, new Tuple<string, DateTime?, Genre>(null, null, null)),
                new Tuple<string, Gig, string, DateTime?, Genre, bool, Tuple<string, DateTime?, Genre>>("004", Artifacts.SpawnGig(Artifacts.PrincipalOfUser1), Artifacts.DummyGigOfUser1.Venue, Artifacts.DummyGigOfUser1.DateTime, null, false, new Tuple<string, DateTime?, Genre>(null, null, null)),
                new Tuple<string, Gig, string, DateTime?, Genre, bool, Tuple<string, DateTime?, Genre>>("005", Artifacts.SpawnGig(Artifacts.PrincipalOfUser1), Artifacts.DummyGigOfUser1.Venue, null, Artifacts.DummyGigOfUser1.Genre, false, new Tuple<string, DateTime?, Genre>(null, null, null)),
                new Tuple<string, Gig, string, DateTime?, Genre, bool, Tuple<string, DateTime?, Genre>>("006", Artifacts.SpawnGig(Artifacts.PrincipalOfUser1), null, Artifacts.DummyGigOfUser1.DateTime, Artifacts.DummyGigOfUser1.Genre, false, new Tuple<string, DateTime?, Genre>(null, null, null)),
                new Tuple<string, Gig, string, DateTime?, Genre, bool, Tuple<string, DateTime?, Genre>>("007", Artifacts.SpawnGig(Artifacts.PrincipalOfUser1), Artifacts.DummyGigOfUser1.Venue, Artifacts.DummyGigOfUser1.DateTime, Artifacts.DummyGigOfUser1.Genre, false, new Tuple<string, DateTime?, Genre>(null, null, null)),
                new Tuple<string, Gig, string, DateTime?, Genre, bool, Tuple<string, DateTime?, Genre>>("008", Artifacts.SpawnGig(Artifacts.PrincipalOfUser1), Artifacts.DummyGigOfUser1.Venue + "-", null, null, true, new Tuple<string, DateTime?, Genre>(Artifacts.DummyGigOfUser1.Venue, null, null)),
                new Tuple<string, Gig, string, DateTime?, Genre, bool, Tuple<string, DateTime?, Genre>>("009", Artifacts.SpawnGig(Artifacts.PrincipalOfUser1), null, Artifacts.DummyGigOfUser1.DateTime.AddDays(1), null, true, new Tuple<string, DateTime?, Genre>(null, Artifacts.DummyGigOfUser1.DateTime, null)),
                new Tuple<string, Gig, string, DateTime?, Genre, bool, Tuple<string, DateTime?, Genre>>("010", Artifacts.SpawnGig(Artifacts.PrincipalOfUser1), null, null, Artifacts.DummyGigOfUser2.Genre, true, new Tuple<string, DateTime?, Genre>(null, null, Artifacts.DummyGigOfUser1.Genre))
            };
        }

        [OneTimeSetUp]
        public void TestFixtureSetUp()
        {
            CultureInfo.DefaultThreadCurrentCulture = CultureInfo.InvariantCulture; //0 vital
            CultureInfo.DefaultThreadCurrentUICulture = CultureInfo.InvariantCulture; //0 vital

            //0 setting the culture to invariantculture is vital to ensure that
            //  somedatetimeobject.tostring() will behave as intended in our tests
        }

        [SetUp]
        public void SetUp()
        {
        }

        [TearDown]
        public void TearDown()
        {
        }

        [OneTimeTearDown]
        public void TestFixtureTearDown()
        {
        }

        [Test]
        [Category("Unit.GigTests")]
        [TestCaseSource(nameof(SetCancelStatusCases))]
        public void T009_001_SetCancelStatus_SimpleCall_ShouldCancel(Tuple<string, bool> @case)
        {
            // Arrange
            var gig = new Gig
            {
                Id = 0,
                DateTime = DateTime.MaxValue,
                Artist = new ApplicationUser {Name = Artifacts.PrincipalOfUser1.Identity.Name},
                ArtistId = "1",
                Genre = Artifacts.DummyGenreOfUser1,
                GenreId = 1,
                Venue = "Apollo Theater"
            };
            var attendee = new ApplicationUser { Name = Artifacts.PrincipalOfUser1.Identity.Name };

            gig.Attendances.Add(new Attendance { GigId = 0, AttendeeId = "1", Attendee = attendee, Gig = gig });

            // Act
            var action = new Action(() => gig.SetCancelStatus(cancelNotUncancel: @case.Item2));

            // Assert
            action.Should().NotThrow();
            gig.GotCancelled.Should().Be(@case.Item2);
            attendee.UserNotifications.Count.Should().Be(1);
            attendee.UserNotifications.First().IsRead.Should().Be(false);
            attendee.UserNotifications.First().Notification.Gig.Should().BeEquivalentTo(gig);
            attendee.UserNotifications.First().Notification.DateTime.HasValue.Should().Be(true);
            attendee.UserNotifications.First().Notification.Type.Should().Be(@case.Item2 ? ENotificationType.GigCanceled : ENotificationType.GigUncanceled);

            attendee.UserNotifications.First().Notification.OriginalGenre.Should().BeNull();
            attendee.UserNotifications.First().Notification.OriginalVenue.Should().BeNull();
            attendee.UserNotifications.First().Notification.OriginalDateTime.HasValue.Should().Be(false);
        }

        [Test]
        [TestCaseSource(nameof(TryUpdateCases))]
        [Category("Unit.ClassBeingTestedHere")]
        public void T009_002_TryUpdate_AllScenarios_ShouldPassWithoutExceptions(Tuple<string, Gig, string, DateTime?, Genre, bool, Tuple<string, DateTime?, Genre>> @case)
        {
            // Arrange
            var gig = @case.Item2;
            var attendee = new ApplicationUser();
            var newVenue = @case.Item3;
            var newGenre = @case.Item5;
            var newDatetime = @case.Item4;
            var expectedResult = @case.Item6;
            var updateSuccessful = false;
            var expectedResultForOriginalVenue = @case.Item7.Item1;
            var expectedResultForOriginalGenre = @case.Item7.Item3;
            var expectedResultForOriginalDateTime = @case.Item7.Item2;

            gig.Attendances.Add(new Attendance { GigId = 0, AttendeeId = "1", Attendee = attendee, Gig = gig });

            // Act
            var action = new Action(() => updateSuccessful = gig.TryUpdate(newVenue, newDatetime, newGenre));

            // Assert
            action.Should().NotThrow();
            updateSuccessful.Should().Be(expectedResult);
            attendee.UserNotifications.Count.Should().Be(expectedResult ? 1 : 0);
            attendee.UserNotifications.FirstOrDefault()?.IsRead.Should().Be(false); //0
            attendee.UserNotifications.FirstOrDefault()?.Notification.Gig.Should().Be(gig);
            attendee.UserNotifications.FirstOrDefault()?.Notification.Type.Should().Be(ENotificationType.GigUpdated);
            attendee.UserNotifications.FirstOrDefault()?.Notification.DateTime.HasValue.Should().Be(true);
            attendee.UserNotifications.FirstOrDefault()?.Notification.OriginalVenue.Should().Be(expectedResultForOriginalVenue);
            attendee.UserNotifications.FirstOrDefault()?.Notification.OriginalDateTime.Should().Be(expectedResultForOriginalDateTime);
            attendee.UserNotifications.FirstOrDefault()?.Notification.OriginalGenre.Should().Be(expectedResultForOriginalGenre);
        }
        //0 this is borderline integration testing but when I realized it I was already almost done with these tests so I left it here
    }
}
