using FluentAssertions;
using GigHub.Core.Foundation.Models;
using GigHub.Core.Persistence.Repositories;
using GigHub.Core.UnitTests.Utilities;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace GigHub.Core.UnitTests.UnitTests.Repositories
{
    [TestFixture]
    public class UserHasReadNotificationsRepositoryTests
    {
        [OneTimeSetUp]
        public void TestFixtureSetUp()
        {
            CultureInfo.DefaultThreadCurrentCulture = CultureInfo.InvariantCulture; //0 vital
            CultureInfo.DefaultThreadCurrentUICulture = CultureInfo.InvariantCulture; //0 vital

            //0 setting the culture to invariantculture is vital to ensure that
            //  somedatetimeobject.tostring() will behave as intended in our tests
        }

        [SetUp]
        public void SetUp()
        {
        }

        [TearDown]
        public void TearDown()
        {
        }

        [OneTimeTearDown]
        public void TestFixtureTearDown()
        {
        }

        [Test]
        [Category("Unit.NotificationsRepositoryTests")]
        public void T012_001_GetUnreadNotifications_JustGetUnreadNotifications_ShouldReturnOnlyUnreadNotifications() // fails for the time being
        {
            // Setup
            var unreadNotifs = (IEnumerable<UserHasReadNotification>) null;
            var listOfUnreadNotifications = new[]
            {
                new UserHasReadNotification(new ApplicationUser(), Notification.ForGigCancelled(Artifacts.DummyGigOfUser1)) {IsRead = false, UserId = "1", NotificationId = 1},
                new UserHasReadNotification(new ApplicationUser(), Notification.ForGigCancelled(Artifacts.DummyGigOfUser2)) {IsRead = false, UserId = "1", NotificationId = 2},
                new UserHasReadNotification(new ApplicationUser(), Notification.ForGigCancelled(Artifacts.DummyGigOfUser2)) {IsRead = true, UserId = "1", NotificationId = 3}
            }.ToList();
            var repository = new UserHasReadNotificationsRepository().SetupUserHasReadNotificationsRepositoryMocks(listOfUnreadNotifications.AsQueryable());

            // Act
            var action = new Func<Task>(async () => unreadNotifs = await repository.GetUnreadNotificationsAsync("1"));

            // Verify
            action.Should().NotThrow();
            unreadNotifs.Should().BeEquivalentTo(listOfUnreadNotifications.Where(x => !x.IsRead));
        }

        [Test]
        [Category("Unit.ClassBeingTestedHere")]
        public void T012_002_GetUnreadNotifications_AskForNotificationsIdsThatDontExist_ReturnNone() // fails for the time being
        {
            // Setup
            var unreadNotifs = (IEnumerable<UserHasReadNotification>) null;
            var listOfUnreadNotifications = new[]
            {
                new UserHasReadNotification(new ApplicationUser(), Notification.ForGigCancelled(Artifacts.DummyGigOfUser1)) {IsRead = false, UserId = "1", NotificationId = 1},
                new UserHasReadNotification(new ApplicationUser(), Notification.ForGigCancelled(Artifacts.DummyGigOfUser2)) {IsRead = false, UserId = "1", NotificationId = 2}
            }.ToList();
            var repository = new UserHasReadNotificationsRepository().SetupUserHasReadNotificationsRepositoryMocks(listOfUnreadNotifications.AsQueryable());

            // Act
            var action = new Func<Task>(async () =>
            {
                unreadNotifs = await repository.GetUnreadNotificationsAsync("1", ids: int.MaxValue.Enumerify());
            });

            // Verify
            action.Should().NotThrow();
            unreadNotifs.Should().BeEmpty();
        }
    }
}