﻿using FluentAssertions;
using GigHub.Core.Foundation.Models;
using GigHub.Core.Persistence.Repositories;
using GigHub.Core.UnitTests.Utilities;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace GigHub.Core.UnitTests.UnitTests.Repositories
{
    [TestFixture]
    public class GigsRepositoryTests
    {
        [OneTimeSetUp]
        public void TestFixtureSetUp()
        {
            CultureInfo.DefaultThreadCurrentCulture = CultureInfo.InvariantCulture; //0 vital
            CultureInfo.DefaultThreadCurrentUICulture = CultureInfo.InvariantCulture; //0 vital

            //0 setting the culture to invariantculture is vital to ensure that
            //  somedatetimeobject.tostring() will behave as intended in our tests
        }

        [SetUp]
        public void SetUp()
        {
        }

        [TearDown]
        public void TearDown()
        {
        }

        [OneTimeTearDown]
        public void TestFixtureTearDown()
        {
        }

        [Test]
        [Category("Unit.GigsRepositoryTests")]
        public void T011_001_GetMultiple_GigIsInThePast_ShouldNotBeReturned() // fails for the time being
        {
            // Setup
            var gigs = (IEnumerable<Gig>) null;
            var oneGig = new Gig {DateTime = DateTime.Now.AddDays(-1), ArtistId = "1"};
            var repository = new GigsRepository().SetupGigsRepositoryMocks(new List<Gig> { oneGig }.AsQueryable());
            
            // Act
            var action = new Func<Task>(async () =>
            {
                gigs = await repository.GetMultipleAsync(artistId: "1", onlyFutureOnesNotAll: true, includeCancelled: true);
            });

            // Verify
            action.Should().NotThrow();
            gigs.Should().BeEmpty();
        }

        [Test]
        [Category("Unit.GigsRepositoryTests")]
        public void T011_002_GetMultiple_GigIsCanceled_ShouldNotBeReturned() // fails for the time being
        {
            // Setup
            var gigs = (IEnumerable<Gig>)null;
            var oneGig = new Gig {DateTime = DateTime.Now.AddDays(1), ArtistId = "1"}.SetCancelStatus(cancelNotUncancel: true);
            var repository = new GigsRepository().SetupGigsRepositoryMocks(new List<Gig> { oneGig }.AsQueryable());

            // Act
            var action = new Func<Task>(async () =>
            {
                gigs = await repository.GetMultipleAsync(artistId: "1", onlyFutureOnesNotAll: true, includeCancelled: false);
            });

            // Verify
            action.Should().NotThrow();
            gigs.Should().BeEmpty();
        }

        [Test]
        [Category("Unit.GigsRepositoryTests")]
        public void T011_003_GetMultiple_GigIsForADifferentArtist_ShouldNotBeReturned() // fails for the time being
        {
            // Setup
            var gigs = (IEnumerable<Gig>) null;
            var oneGig = new Gig { DateTime = DateTime.Now.AddDays(1), ArtistId = "1" };
            var repository = new GigsRepository().SetupGigsRepositoryMocks(new List<Gig> { oneGig }.AsQueryable());

            // Act
            var action = new Func<Task>(async () =>
            {
                gigs = await repository.GetMultipleAsync(artistId: oneGig.ArtistId + "-", onlyFutureOnesNotAll: true, includeCancelled: true);
            });

            // Verify
            action.Should().NotThrow();
            gigs.Should().BeEmpty();
        }

        [Test]
        [Category("Unit.GigsRepositoryTests")]
        public void T011_004_GetMultiple_FutureGigIsForGivenArtist_ShouldBeReturned() // fails for the time being
        {
            // Setup
            var gigs = (IEnumerable<Gig>)null;
            var oneGig = new Gig { DateTime = DateTime.Now.AddDays(1), ArtistId = "1" };
            var repository = new GigsRepository().SetupGigsRepositoryMocks(new List<Gig> { oneGig }.AsQueryable());

            // Act
            var action = new Func<Task>(async () =>
            {
                gigs = await repository.GetMultipleAsync(artistId: oneGig.ArtistId, onlyFutureOnesNotAll: true, includeCancelled: true);
            });

            // Verify
            action.Should().NotThrow();
            gigs.Should().ContainSingle(g => g == oneGig);
        }

        [Test]
        [Category("Unit.GigsRepositoryTests")]
        public void T011_005_GetMultiple_GetEverything_ShouldReturnAllGigs() // fails for the time being
        {
            // Setup
            var gigs = (IEnumerable<Gig>)null;
            var allGigs = new[] {Artifacts.DummyGigOfUser1, Artifacts.DummyGigOfUser2};
            var repository = new GigsRepository().SetupGigsRepositoryMocks(allGigs.AsQueryable());

            // Act
            var action = new Func<Task>(async () =>
            {
                gigs = (await repository.GetMultipleAsync()).ToList();
            });

            // Verify
            action.Should().NotThrow();
            gigs.Should().BeEquivalentTo(allGigs);
        }
    }
}