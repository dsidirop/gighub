﻿using FluentAssertions;
using GigHub.Core.Foundation.Models;
using GigHub.Core.Persistence.Repositories;
using GigHub.Core.UnitTests.Utilities;
using Microsoft.AspNet.Identity;
using MyTested.WebApi.Common.Extensions;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GigHub.Core.UnitTests.UnitTests.Repositories
{
    [TestFixture]
    public class AttendancesRepositoryTests
    {
        [SetUp]
        public void SetUp()
        {
        }

        [TearDown]
        public void TearDown()
        {
        }

        [OneTimeTearDown]
        public void TestFixtureTearDown()
        {
        }

        [Test]
        [Category("Unit.AttendanceRepositoryTests")]
        public void T010_001_GetAllMatching_FilterByVenueName_ReturnOneOfTwoGigs()
        {
            // Setup
            var gigsUserIsAttending = (IEnumerable<Attendance>) null;

            var mockAppUser = new ApplicationUser {Name = Artifacts.PrincipalOfUser1.GetName()};

            var gigForAttendance2 = new Gig {Id = 1, Venue = "Stardom Stadium", ArtistId = Artifacts.PrincipalOfUser1.Identity.GetUserId(), Artist = mockAppUser, DateTime = DateTime.MaxValue, Genre = Artifacts.DummyGenreOfUser1, GenreId = Artifacts.DummyGenreOfUser1.Id};
            var attendance1 = new Attendance {Gig = Artifacts.DummyGigOfUser1, GigId = Artifacts.DummyGigOfUser1.Id, Attendee = mockAppUser, AttendeeId = Artifacts.PrincipalOfUser1.Identity.GetUserId()};
            var attendance2 = new Attendance {Gig = gigForAttendance2, GigId = gigForAttendance2.Id, Attendee = mockAppUser, AttendeeId = Artifacts.PrincipalOfUser1.Identity.GetUserId()};
            var fakeDataSource = new List<Attendance> {attendance1, attendance2}.AsQueryable();
            var attendancesRepository = new AttendancesRepository().SetupAttendancesRepositoryMocks(fakeDataSource);

            // Act
            var action = new Func<Task>(async () => gigsUserIsAttending = await attendancesRepository.GetAllMatchingAsync(userId: Artifacts.PrincipalOfUser1.Identity.GetUserId(), searchTerm: Artifacts.DummyGigOfUser1.Venue)); //1 crucial func

            // Verify
            action.Should().NotThrow();
            gigsUserIsAttending.Should().ContainSingle(g => g == attendance1);
        }
        //1 http://haacked.com/archive/2014/11/11/async-void-methods/  http://theburningmonk.com/2012/10/c-beware-of-async-void-in-your-code/  aka avoid async void methods virtually everywhere across
        //  all your code   had we used new action instead of new func we would be testing async void which would clog the test by making it fail to detect exceptions via action.shouldnotthrow   this
        //  is simply because in csharp we cant wait for the operation to complete when using async void and we thus cant intercept exceptions  aka exceptions in async void are by definition uninterceptable
        //  and cause the entire process to simply crash   this is something that happens by design in csharp

        [Test]
        [Category("Unit.AttendanceRepositoryTests")]
        public void T010_002_GetAllMatching_FilterByGigId_ReturnOneOfTwoGigs() //0 fails for the time being
        {
            // Setup
            var gigsUserIsAttending = (IEnumerable<Attendance>) null;

            var mockAppUser = new ApplicationUser { Name = Artifacts.PrincipalOfUser1.GetName() };

            var gigForAttendance2 = new Gig {Id = 1, Venue = "Stardom Stadium", ArtistId = Artifacts.PrincipalOfUser1.Identity.GetUserId(), Artist = mockAppUser, DateTime = DateTime.MaxValue, Genre = Artifacts.DummyGenreOfUser1, GenreId = Artifacts.DummyGenreOfUser1.Id};
            var attendance1 = new Attendance {Gig = Artifacts.DummyGigOfUser1, GigId = Artifacts.DummyGigOfUser1.Id, Attendee = mockAppUser, AttendeeId = Artifacts.PrincipalOfUser1.Identity.GetUserId()};
            var attendance2 = new Attendance {Gig = gigForAttendance2, GigId = gigForAttendance2.Id, Attendee = mockAppUser, AttendeeId = Artifacts.PrincipalOfUser1.Identity.GetUserId()};
            var fakeDataSource = new List<Attendance> { attendance1, attendance2 }.AsQueryable();
            var attendancesRepository = new AttendancesRepository().SetupAttendancesRepositoryMocks(fakeDataSource);

            // Act
            var action = new Func<Task>(async () => gigsUserIsAttending = await attendancesRepository.GetAllMatchingAsync(userId: Artifacts.PrincipalOfUser1.Identity.GetUserId(), gigIds: Artifacts.DummyGigOfUser1.Id.Enumerify().ToList()));

            // Verify
            action.Should().NotThrow();
            gigsUserIsAttending.Should().ContainSingle(g => g == attendance1);
        }
        //0 note that tolistasync used internally by getallmatchingasync is very troublesome when it comes to unittesting it against inmemory mocks of db and dbsets   such unittests throw an exception
        //  upon invoking tolistasync about the operation being unsupported and stuff   working on a solution on this
    }
}