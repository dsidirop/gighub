﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using FluentAssertions;
using GigHub.Core.Controllers.Api;
using GigHub.Core.Foundation.Dtos;
using GigHub.Core.Foundation.Mapper;
using GigHub.Core.Foundation.Mapper.Mappings;
using GigHub.Core.Foundation.Models;
using GigHub.Core.Services;
using GigHub.Core.UnitTests.Utilities;
using Microsoft.AspNet.Identity;
using MyTested.WebApi;
using NUnit.Framework;

//notetoself   regarding the testing of repositories a pragmatic approach is to test only the important
//notetoself   methods using unit tests   this process is quite costly ofcourse
namespace GigHub.Core.UnitTests.UnitTests.Controllers.WebApi
{
    [TestFixture]
    [Category("Unit.NotificationsControllerTests")]
    public class NotificationsControllerTests
    {
        [OneTimeSetUp]
        public void TestFixtureSetUp()
        {
            CultureInfo.DefaultThreadCurrentCulture = CultureInfo.InvariantCulture; //0 vital
            CultureInfo.DefaultThreadCurrentUICulture = CultureInfo.InvariantCulture; //0 vital

            //0 setting the culture to invariantculture is vital to ensure that
            //  somedatetimeobject.tostring() will behave as intended in our tests
        }

        [SetUp]
        public void SetUp()
        {
        }

        [TearDown]
        public void TearDown()
        {
        }

        [OneTimeTearDown]
        public void TestFixtureTearDown()
        {
        }

        [Test]
        public void T018_001_Get_SimpleInvocation_ShouldReturnOk200()
        {
            //Arrange
            var mapper = new MappingService(new IMapping[]
            {
                new GigToGigDto(),
                new GenreToGenreDto(),
                new ApplicationToApplicationDto(),
                new NotificationToNotificationDto()
            });

            var customUnitOfWork = Artifacts.SpawnUnitOfWork(
                userHasReadNotificationsRepo: Artifacts.SpawnUserHasReadNotificationsRepo(new[]
                {
                    new UserHasReadNotification(
                        new ApplicationUser {Name = Artifacts.PrincipalOfUser1.Identity.Name},
                        Notification.ForGigCancelled(Artifacts.DummyGigOfUser1)
                    )
                    {
                        UserId = Artifacts.PrincipalOfUser1.Identity.GetUserId()
                    }
                })
            );

            var notificationsService = new NotificationsService(customUnitOfWork);

            //Act
            var action = MyWebApi
                .Controller(new NotificationsController(
                    mapper: mapper,
                    notificationsService: notificationsService
                ))
                .WithAuthenticatedUser(
                    user => user
                        .WithUsername("username")
                        .AndAlso()
                        .WithIdentifier("1") //identitygetuserid
                )
                .CallingAsync(c => c.Get());

            //Assert
            action
                .ShouldReturn()
                .Ok()
                .WithResponseModelOfType<IEnumerable<NotificationDto>>()
                .Passing(
                    results => results
                        .Single()
                        .Should()
                        .NotBeNull()
                );
        }

        [Test]
        public void T019_001_MarkAsRead_NotifsIdsParameterEmpty_ShouldReturnOk200()
        {
            //Arrange
            var mapper = new MappingService(new IMapping[]
            {
                new GigToGigDto(),
                new GenreToGenreDto(),
                new ApplicationToApplicationDto(),
                new NotificationToNotificationDto()
            });

            var notificationsService = new NotificationsService(Artifacts.UnitOfWork);

            var markAsReadOptions = new MarkAsReadOptions {Ids = new int[] { }};

            //Act
            var action = MyWebApi
                .Controller(new NotificationsController(
                    mapper: mapper,
                    notificationsService: notificationsService
                ))
                .WithAuthenticatedUser(
                    user => user
                        .WithUsername("username")
                        .AndAlso()
                        .WithIdentifier("1") //identitygetuserid
                )
                .CallingAsync(c => c.MarkAsRead(markAsReadOptions));

            //Assert
            action
                .ShouldReturn()
                .Ok()
                .WithNoResponseModel();
        }

        [Test]
        public void T019_002_MarkAsRead_MarkSingleNotifOfUserAsRead_ShouldReturnOk200()
        {
            //Arrange
            var notification = Notification.ForGigCancelled(Artifacts.DummyGigOfUser1);
            var unitOfWork = Artifacts.SpawnUnitOfWork(userHasReadNotificationsRepo: Artifacts.SpawnUserHasReadNotificationsRepo(new[]
            {
                new UserHasReadNotification(new ApplicationUser {Name = Artifacts.PrincipalOfUser1.Identity.Name}, notification) {UserId = Artifacts.PrincipalOfUser1.Identity.GetUserId(), NotificationId = notification.Id}
            }));

            var markAsReadOptions = new MarkAsReadOptions {Ids = notification.Id.Enumerify()};

            //Act
            var action = MyWebApi
                .Controller(new NotificationsController(
                    new MappingService(new IMapping[]
                    {
                        new GigToGigDto(),
                        new GenreToGenreDto(),
                        new ApplicationToApplicationDto(),
                        new NotificationToNotificationDto()
                    }),
                    new NotificationsService(unitOfWork))
                )
                .WithAuthenticatedUser(
                    user => user
                        .WithUsername("username")
                        .AndAlso()
                        .WithIdentifier("1") //identitygetuserid
                )
                .CallingAsync(
                    c => c.MarkAsRead(markAsReadOptions)
                );

            //Assert
            action
                .ShouldReturn()
                .Ok()
                .WithNoResponseModel();
        }

        [Test]
        public void T019_003_MarkAsRead_TryMarkSingleNotifOfDifferentUserAsRead_ShouldReturnForbidden403()
        {
            //Arrange
            var notification = Notification.ForGigCancelled(Artifacts.DummyGigOfUser1);

            var unitOfWork = Artifacts.SpawnUnitOfWork(
                userHasReadNotificationsRepo: Artifacts.SpawnUserHasReadNotificationsRepo(new[]
                    {
                        new UserHasReadNotification(
                            new ApplicationUser {Name = Artifacts.PrincipalOfUser1.Identity.Name},
                            notification
                        )
                        {
                            UserId = Artifacts.PrincipalOfUser1.Identity.GetUserId(),
                            NotificationId = notification.Id
                        }
                    }
                )
            );

            var options = new MarkAsReadOptions {Ids = notification.Id.Enumerify()};

            //Act
            var action = MyWebApi
                .Controller(new NotificationsController(
                    new MappingService(new IMapping[]
                    {
                        new GigToGigDto(),
                        new GenreToGenreDto(),
                        new ApplicationToApplicationDto(),
                        new NotificationToNotificationDto()
                    }),
                    new NotificationsService(unitOfWork))
                )
                .WithAuthenticatedUser(
                    user => user
                        .WithUsername("username")
                        .AndAlso()
                        .WithIdentifier("2") //identitygetuserid
                )
                .CallingAsync(c => c.MarkAsRead(options));

            //Assert
            action
                .ShouldReturn()
                .StatusCode(HttpStatusCode.Forbidden);
        }
    }
}