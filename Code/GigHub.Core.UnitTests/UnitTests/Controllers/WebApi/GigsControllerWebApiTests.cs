﻿using System.Globalization;
using GigHub.Core.Controllers.Api;
using GigHub.Core.Foundation.Dtos;
using GigHub.Core.UnitTests.Utilities;
using Microsoft.AspNet.Identity;
using MyTested.WebApi;
using NUnit.Framework;
using System.Net;
using GigHub.Core.Services;

namespace GigHub.Core.UnitTests.UnitTests.Controllers.WebApi
{
    [TestFixture]
    public class GigsControllerWebApiTests
    {
        [OneTimeSetUp]
        public void TestFixtureSetUp()
        {
            CultureInfo.DefaultThreadCurrentCulture = CultureInfo.InvariantCulture; //0 vital
            CultureInfo.DefaultThreadCurrentUICulture = CultureInfo.InvariantCulture; //0 vital

            //0 setting the culture to invariantculture is vital to ensure that
            //  somedatetimeobject.tostring() will behave as intended in our tests
        }

        [SetUp]
        public void SetUp()
        {
        }

        [TearDown]
        public void TearDown()
        {
        }

        [OneTimeTearDown]
        public void TestFixtureTearDown()
        {
        }

        [Ignore(@"MyWebApi doesn't activate the action-filters regardless of whether they are global or local on action/controller")]
        [Test]
        [Category("Unit.GigsControllerWebApiTests")]
        public void T017_001_SetCancelStatus_NullDto_ShouldReturnBadRequestMalformedJson400()
            => MyWebApi
                .Controller(() => new GigsController(new GigsService(Artifacts.UnitOfWork)))
                .WithAuthenticatedUser( //0 user=artifacts.principalofuser1  doesnt work
                    user => user
                        .WithUsername(Artifacts.PrincipalOfUser1.Identity.Name)
                        .AndAlso()
                        .WithIdentifier(Artifacts.PrincipalOfUser1.Identity.GetUserId())
                )
                .CallingAsync(c => c.SetCancelStatus(null))
                .ShouldReturn()
                .BadRequest()
                .WithErrorMessage("Malformed json in request body");
        //0 for some weird reason the mywebapi assertions cant work via user=artifacts.principalofuser1   turned out we are forced to use the withauthenticateduser approach

        [Ignore(@"MyWebApi doesn't activate the action-filters regardless of whether they are global or local on action/controller")]
        [Test]
        [Category("Unit.GigsControllerWebApiTests")]
        public void T017_002_SetCancelStatus_InvalidDtoGigIdNegative_ShouldReturnBadRequestOnModel400()
            => MyWebApi
                .Controller(() => new GigsController(new GigsService(Artifacts.UnitOfWork)))
                .WithAuthenticatedUser(
                    user => user
                        .WithUsername(Artifacts.PrincipalOfUser1.Identity.Name)
                        .AndAlso()
                        .WithIdentifier(Artifacts.PrincipalOfUser1.Identity.GetUserId())
                )
                .CallingAsync(c => c.SetCancelStatus(new GigToToggleCancelStateDto {GigId = -1, CancelNotUncancel = true}))
                .ShouldReturn()
                .BadRequest()
                .WithModelStateFor<GigToToggleCancelStateDto>()
                .ContainingModelStateErrorFor(m => m.GigId);

        [Test]
        [Category("Unit.GigsControllerWebApiTests")]
        public void T017_003_SetCancelStatus_InvalidDtoGigIdNotExistant_ShouldReturnNotFound404()
            => MyWebApi
                .Controller(() => new GigsController(new GigsService(Artifacts.UnitOfWork)))
                .WithAuthenticatedUser(
                    user => user
                        .WithUsername(Artifacts.PrincipalOfUser1.Identity.Name)
                        .AndAlso()
                        .WithIdentifier(Artifacts.PrincipalOfUser1.Identity.GetUserId())
                )
                .CallingAsync(c => c.SetCancelStatus(new GigToToggleCancelStateDto {GigId = int.MaxValue, CancelNotUncancel = true}))
                .ShouldReturn()
                .StatusCode(HttpStatusCode.NotFound);

        [Test]
        [Category("Unit.GigsControllerWebApiTests")]
        public void T017_004_SetCancelStatus_AttemptToCancelTheGigOfAnotherArtist_ShouldReturnForbidden403()
            => MyWebApi
                .Controller(() => new GigsController(new GigsService(Artifacts.UnitOfWork)))
                .WithAuthenticatedUser(
                    user => user
                        .WithUsername(Artifacts.PrincipalOfUser1.Identity.Name)
                        .AndAlso()
                        .WithIdentifier(Artifacts.PrincipalOfUser1.Identity.GetUserId())
                )
                .CallingAsync(c => c.SetCancelStatus(new GigToToggleCancelStateDto { GigId = Artifacts.DummyGigOfUser2.Id, CancelNotUncancel = true }))
                .ShouldReturn()
                .StatusCode(HttpStatusCode.Forbidden);

        [Test]
        [Category("Unit.GigsControllerWebApiTests")]
        public void T017_005_SetCancelStatus_AttemptToUncancelGigWhichIsAlreadyUncanceled_ShouldReturnSuccessNoContent204()
            => MyWebApi
                .Controller(() => new GigsController(new GigsService(Artifacts.UnitOfWork)))
                .WithAuthenticatedUser(
                    user => user
                        .WithUsername(Artifacts.PrincipalOfUser1.Identity.Name)
                        .AndAlso()
                        .WithIdentifier(Artifacts.PrincipalOfUser1.Identity.GetUserId())
                )
                .CallingAsync(c => c.SetCancelStatus(new GigToToggleCancelStateDto {GigId = Artifacts.DummyGigOfUser1.Id, CancelNotUncancel = false}))
                .ShouldReturn()
                .StatusCode(HttpStatusCode.NoContent); //aka success
    }
}