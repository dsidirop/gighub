﻿using System;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using FluentAssertions;
using GigHub.Core.Controllers;
using GigHub.Core.Foundation.ViewModels;
using GigHub.Core.Services;
using GigHub.Core.UnitTests.Utilities;
using GigHub.Core.Utilities;
using NUnit.Framework;

// ReSharper disable PossibleNullReferenceException

namespace GigHub.Core.UnitTests.UnitTests.Controllers.Mvc
{
    [TestFixture]
    public class GigsControllerMvcTests
    {
        static private readonly Tuple<string, string>[] QueryCases; //must be static
        static private readonly Tuple<string, Action<GigFormViewModel>>[] TweakGigCases; //must be static
        private GigsController _gigsController;

        static GigsControllerMvcTests() //dont move this into [onetimesetup]
        {
            QueryCases = new[]
            {
                new Tuple<string, string>("000", ""),
                new Tuple<string, string>("001", null),
                new Tuple<string, string>("002", "foobar")
            };

            TweakGigCases = new[]
            {
                new Tuple<string, Action<GigFormViewModel>>("000", g => g.Date = g.GetDateTime().AddDays(1).ToString(UDates.GigDateFormat, CultureInfo.InvariantCulture)),
                new Tuple<string, Action<GigFormViewModel>>("001", g => g.Time = g.GetDateTime().AddHours(1).ToString(UDates.GigTimeOfDayFormat, CultureInfo.InvariantCulture)),
                new Tuple<string, Action<GigFormViewModel>>("002", g => g.Venue = g.Venue + "2"),
                new Tuple<string, Action<GigFormViewModel>>("003", g => g.GenreId++)
            };
        }

        [OneTimeSetUp]
        public void TestFixtureSetUp()
        {
            CultureInfo.DefaultThreadCurrentCulture = CultureInfo.InvariantCulture; //0 vital
            CultureInfo.DefaultThreadCurrentUICulture = CultureInfo.InvariantCulture; //0 vital

            //0 setting the culture to invariantculture is vital to ensure that
            //  somedatetimeobject.tostring() will behave as intended in our tests
        }

        [SetUp]
        public void SetUp()
        {
            _gigsController = new GigsController(
                new GigsService(Artifacts.UnitOfWork),
                new ArtistsService(Artifacts.UnitOfWork)
            ).SetupCtrlerMocks(Artifacts.PrincipalOfUser1);
        }

        [TearDown]
        public void TearDown()
        {
        }

        [OneTimeTearDown]
        public void TestFixtureTearDown()
        {
        }

        [Test]
        [Category("Unit.MvcGigsControllerTests")]
        public void T000_001_SingleGig_InvalidGigId_ShouldReturnNotFound404()
        {
            // Arrange
            var result = (HttpStatusCodeResult) null;


            // Act
            var action = new Func<Task>(async () => result = await _gigsController.SingleGig(gigId: -1) as HttpStatusCodeResult);

            // Assert
            action.Should().NotThrow();
            result?.StatusCode.Should().Be((int) HttpStatusCode.NotFound); //0
        }
        //0 result.Should().BeEquivalentTo(new httpstatuscoderesult(httpstatuscode.notfound, "gig not found"))   this doesnt work for some reason

        [Test]
        [Category("Unit.MvcGigsControllerTests")]
        public void T000_002_SingleGig_ValidGigId_ShouldReturnNotNullView()
        {
            // Arrange
            var result = (ViewResult) null;
            
            // Act
            var action = new Func<Task>(async () => result = await _gigsController.SingleGig(gigId: Artifacts.DummyGigOfUser1.Id) as ViewResult);

            // Assert
            action.Should().NotThrow();
            result.Should().NotBeNull();
        }

        [Test]
        [Category("Unit.MvcGigsControllerTests")]
        public void T001_001_Attending_NullModelParameter_ShouldReturnInternalError500()
        {
            // Arrange
            var result = (HttpStatusCodeResult) null;

            // Act
            var action = new Func<Task>(async () => result = await _gigsController.Attending() as HttpStatusCodeResult);

            // Assert
            action.Should().NotThrow();
            result?.StatusCode.Should().Be((int) HttpStatusCode.BadRequest);
        }

        [Test]
        [TestCaseSource(nameof(QueryCases))]
        [Category("Unit.MvcGigsControllerTests")]
        public void T001_002_Attending_ValidModelParameter_ShouldReturnNonNullView(Tuple<string, string> @case)
        {
            // Arrange
            var query = @case.Item2;
            var result = (ViewResult) null;

            // Act
            var action = new Func<Task>(async () => result = await _gigsController.Attending(new AttendingGigsListOptions {Query = query}) as ViewResult);

            // Assert
            action.Should().NotThrow();
            result.Should().NotBeNull();
        }

        [Test]
        [Category("Unit.MvcGigsControllerTests")]
        public void T002_001_Attending_ValidModelParameter_ShouldReturnNonNullView()
        {
            // Arrange
            var result = (ViewResult) null;

            // Act
            var action = new Func<Task>(async () => result = await _gigsController.Attending() as ViewResult);

            // Assert
            action.Should().NotThrow();
            result.Should().NotBeNull();
        }

        [Test]
        [TestCaseSource(nameof(QueryCases))]
        [Category("Unit.MvcGigsControllerTests")]
        public void T003_001_GigsStagedByMe_SimpleQuery_ReturnViewNonNull(Tuple<string, string> @case)
        {
            // Arrange
            var query = @case.Item2;
            var result = (ViewResult) null;


            // Act
            var action = new Func<Task>(async () => result = await _gigsController.GigsStagedByMe(new GigsStagedByMeOptions {Query = query}) as ViewResult);

            // Assert
            action.Should().NotThrow();
            result.Should().NotBeNull();
        }

        [Test]
        [Category("Unit.MvcGigsControllerTests")]
        public void T004_001_ArtistsFollowedByUser_StraightforwardInvocation_ReturnViewNonNull()
        {
            // Arrange
            var model = (ViewResult) null;


            // Act
            var action = new Func<Task>(async () => model = await _gigsController.ArtistsFollowedByUser() as ViewResult);

            //Assert
            action.Should().NotThrow();
            model.Should().NotBeNull();
        }

        [Test]
        [Category("Unit.MvcGigsControllerTests")]
        public void T005_001_Edit_NonExistantGigId_HttpCodeNotFound404()
        {
            // Arrange
            var result = (HttpStatusCodeResult) null;


            // Act
            var action = new Func<Task>(async () => result = await _gigsController.Edit(gigId: -1) as HttpStatusCodeResult);

            // Assert
            action.Should().NotThrow();
            result?.StatusCode.Should().Be((int) HttpStatusCode.NotFound); //0
        }

        [Test]
        [Category("Unit.MvcGigsControllerTests")]
        public void T005_002_Edit_AttemptToEditGigBelongToAnotherUser_HttpResponseUnauthorized401()
        {
            // Arrange
            var result = (HttpStatusCodeResult) null;


            // Act
            var action = new Func<Task>(async () => result = await _gigsController.Edit(gigId: Artifacts.DummyGigOfUser2.Id) as HttpStatusCodeResult);

            // Assert
            action.Should().NotThrow();
            result?.StatusCode.Should().Be((int) HttpStatusCode.Unauthorized); //0
        }

        [Test]
        [Category("Unit.MvcGigsControllerTests")]
        public void T005_003_Edit_ValidGigId_ResultingViewShouldNotBeNull()
        {
            // Arrange
            var result = (ViewResult) null;


            // Act
            var action = new Func<Task>(async () => result = await _gigsController.Edit(gigId: Artifacts.DummyGigOfUser1.Id) as ViewResult);

            // Assert
            action.Should().NotThrow();
            result.Should().NotBeNull();
        }

        [Test]
        [Category("Unit.MvcGigsControllerTests")]
        public void T006_001_Create_JustInvoke_ReturnViewNonNull()
        {
            // Arrange
            var result = (ViewResult) null;


            // Act
            var action = new Func<Task>(async () => result = await _gigsController.Create() as ViewResult);

            // Assert
            action.Should().NotThrow();
            result.Should().NotBeNull();
        }

        [Test]
        [Category("Unit.MvcGigsControllerTests")]
        public void T007_001_Upsert_AttemptToUpdateGigOfDifferentArtist_ReturnViewNonNull()
        {
            // Arrange
            var result = (ViewResult) null;
            
            // Act
            var action = new Func<Task>(async () => result = await _gigsController.Upsert(model: new GigFormViewModel {Id = Artifacts.DummyGigOfUser2.Id}) as ViewResult);

            // Assert
            action.Should().NotThrow();
            result.Should().NotBeNull();
            // (result.Model as GigFormViewModel).PossibleError.Should().Be("You aren't the owner of the Gig you are trying to update"); //impldetail so dont test it here
        }

        [Test]
        [Category("Unit.MvcGigsControllerTests")]
        public async Task T007_002_Upsert_UpdateGigNoChanges_ReturnRedirectToGigsStagedByMe()
        {
            // Arrange
            var result = (RedirectToRouteResult) null;

            var gigFormViewModel = Artifacts.SpawnValidGigFormViewModelFromGig((await Artifacts.UnitOfWork.GigsRepo.GetMultipleAsync()).FirstOrDefault());

            // Act
            var action = new Func<Task>(async () =>
                result = await _gigsController
                    .ValidateViewModelX(gigFormViewModel, throwExceptionIfInvalid: true) //0 safe
                    .Upsert(model: gigFormViewModel) as RedirectToRouteResult
            );

            // Assert
            action.Should().NotThrow();
            result.Should().NotBeNull();
            result.RouteValues.Should().NotBeNull();
            //result.RouteValues["query"].Should().Be(query); //dont
            result.RouteValues["action"].Should().Be(nameof(GigsController.GigsStagedByMe));
            result.RouteValues["controller"].Should().Be(UMvc.Ctrler<GigsController>());
        }
        //0 the validation of the viewmodel should be checked in a separate test    here we perform the validation simply to make the test failfast when if the model is invalid because
        //  we make the assumption that it is valid to begin with in order to make this test hit its intended spot

        [Test]
        [TestCaseSource(nameof(TweakGigCases))]
        [Category("Unit.MvcGigsControllerTests")]
        public async Task T007_003_Upsert_UpdateGigWithChanges_ReturnRedirectToGigsStagedByMe(Tuple<string, Action<GigFormViewModel>> @case)
        {
            // Arrange
            var result = (RedirectToRouteResult) null;

            var gigFormViewModel = Artifacts.SpawnValidGigFormViewModelFromGig((await Artifacts.UnitOfWork.GigsRepo.GetMultipleAsync()).FirstOrDefault());

            @case.Item2(gigFormViewModel); //tweak one of the properties of the model

            // Act
            var action = new Func<Task>(async () =>
                result = await _gigsController
                    .ValidateViewModelX(gigFormViewModel, throwExceptionIfInvalid: true) //0 safe
                    .Upsert(model: gigFormViewModel) as RedirectToRouteResult
            );

            // Assert
            action.Should().NotThrow();
            result.Should().NotBeNull();
            result.RouteValues.Should().NotBeNull();
            //result.RouteValues["query"].Should().Be(query); //dont
            result.RouteValues["action"].Should().Be(nameof(GigsController.GigsStagedByMe));
            result.RouteValues["controller"].Should().Be(UMvc.Ctrler<GigsController>());
        }
        //0 the validation of the viewmodel should be checked in a separate test    here we perform the validation simply to make the test failfast when if the model is invalid because
        //  we make the assumption that it is valid to begin with in order to make this test hit its intended spot
    }
}