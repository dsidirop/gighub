﻿using FluentAssertions;
using GigHub.Core.Controllers;
using GigHub.Core.UnitTests.Utilities;
using NUnit.Framework;
using System;
using System.Globalization;
using System.Threading.Tasks;
using System.Web.Mvc;
using GigHub.Core.Services;

//reminder#1    unittests exist to test behavior of classes in terms of returned values and exception     unittests should not be testing internal implementation
//reminder#1    details like for example whether the save method got called or whether the results of an operation got saved on the disk or database  always keep
//reminder#1    this detail in mind because it is the difference that makes a difference between good and bad unittests
//reminder#2    at the time of this writing various free continuous integration services dont support out of the box the latest and greatest nunit libs   one has
//reminder#2    to employ workarounds to get things working with said ci services    be careful to take this fact into account in corporate environments that need
//reminder#2    ci up and running because fiddling with workarounds can prove timeconsuming and counterproductive   also note that the unittesting suites of from ms
//reminder#2    seem to enjoy the best support among ci services   this holds for both the old 1.x variants and the new 2.x variants
namespace GigHub.Core.UnitTests.UnitTests.Controllers.Mvc
{
    [TestFixture]
    public class HomeControllerMvcTests
    {
        private HomeController _homeController;

        [OneTimeSetUp]
        public void TestFixtureSetUp()
        {
            CultureInfo.DefaultThreadCurrentCulture = CultureInfo.InvariantCulture; //0 vital
            CultureInfo.DefaultThreadCurrentUICulture = CultureInfo.InvariantCulture; //0 vital

            //0 setting the culture to invariantculture is vital to ensure that
            //  somedatetimeobject.tostring() will behave as intended in our tests
        }

        [SetUp]
        public void SetUp()
        {
            _homeController = new HomeController(new GigsService(Artifacts.UnitOfWork), new ArtistsService(Artifacts.UnitOfWork)).SetupCtrlerMocks(Artifacts.PrincipalOfUser1);
        }

        [TearDown]
        public void TearDown()
        {
        }

        [OneTimeTearDown]
        public void TestFixtureTearDown()
        {
        }

        [Test]
        [Category("Unit.HomeControllerTests")]
        public void T008_001_Index_JustGetTheView_ShouldNotThrowResultMustNotBeNull()
        {
            // Arrange
            var result = (ViewResult) null;
            
            // Act
            var action = new Func<Task>(async () => result = await _homeController.GigsList() as ViewResult);

            // Assert
            action.Should().NotThrow();
            result.Should().NotBeNull();
        }
        //0 should be equivalent to the oldschool controller.user=principalofuser1
    }
}

