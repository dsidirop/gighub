﻿using System;
using System.Globalization;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using FluentAssertions;
using GigHub.Core.Utilities;
using NUnit.Framework;

// ReSharper disable PossibleNullReferenceException

namespace GigHub.Core.UnitTests.UnitTests.Helpers.Reflection
{
    [Ignore("Work in progress")]
    [TestFixture]
    [Category("Unit.TryGrabAttributeFromLiveInstanceUsingBaseTypeExpression")]
    public class TryGrabAttributeFromLiveInstanceUsingBaseTypeExpressionTests
    {
        static TryGrabAttributeFromLiveInstanceUsingBaseTypeExpressionTests() //dont move this into [onetimesetup]
        {
        }

        [OneTimeSetUp]
        public void TestFixtureSetUp()
        {
            CultureInfo.DefaultThreadCurrentCulture = CultureInfo.InvariantCulture; //0 vital
            CultureInfo.DefaultThreadCurrentUICulture = CultureInfo.InvariantCulture; //0 vital

            //0 setting the culture to invariantculture is vital to ensure that
            //  somedatetimeobject.tostring() will behave as intended in our tests
        }

        [SetUp]
        public void SetUp()
        {
        }

        [TearDown]
        public void TearDown()
        {
        }

        [OneTimeTearDown]
        public void TestFixtureTearDown()
        {
        }

        [Test]
        public void T000_001_TryGrabAttributeFromLiveInstanceUsingBaseTypeExpression_GivenModelWithPropertyDecoratedWithAttribute_ShouldReturnSaidAttributeInstance()
        {
            // Arrange
            var fooModel = new Foo();

            // Act
            var action = new Func<PingAttribute>(
                () => fooModel.TryGrabAttributeFromLiveInstanceUsingBaseTypeExpression<Foo, IFoo, IBar, PingAttribute>(
                    x => x.BarProperty
                )
            );

            // Assert
            action
                .Should()
                .NotThrow()
                .Subject
                .Should()
                .NotBeNull($"it's expected to be a '{nameof(PingAttribute)}' instance")
                .And
                .Subject
                .Should()
                .BeOfType<PingAttribute>();
        }

        public interface IFoo
        {
            IBar BarProperty { get; set; }
        }

        public interface IBar
        {

        }

        public class PingAttribute : Attribute
        {
        }

        public class Foo : IFoo
        {
            [Ping] public IBar BarProperty { get; set; } = new Bar();
        }

        public class Bar : IBar
        {
        }
    }
}