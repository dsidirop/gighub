﻿using GigHub.Core.Foundation.Models;
using GigHub.Core.Foundation.Persistence;
using GigHub.Core.Foundation.ViewModels;
using GigHub.Core.UnitTests.Utilities.TestDbAsync;
using GigHub.Core.Utilities;
using Microsoft.AspNet.Identity;
using Moq;
using Ninject.Infrastructure.Language;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace GigHub.Core.UnitTests.Utilities
{
    static public class Artifacts
    {
        static private IPrincipal _principalOfUser1;
        static public IPrincipal PrincipalOfUser1 => _principalOfUser1 = _principalOfUser1 ?? SpawnUserPrincipal(userid: 1);

        static private IPrincipal _principalOfUser2;
        static public IPrincipal PrincipalOfUser2 => _principalOfUser2 = _principalOfUser2 ?? SpawnUserPrincipal(userid: 2);

        static private IPrincipal SpawnUserPrincipal(int userid)
        {
            var useridentity = new GenericIdentity(name: $"user{userid}@domain.com");
            useridentity.AddClaim(new Claim(@"http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name", useridentity.Name));
            useridentity.AddClaim(new Claim(@"http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier", userid.ToString()));
            return new GenericPrincipal(useridentity, roles: null);
        }

        static private Genre _dummyGenreOfUser1;
        static public Genre DummyGenreOfUser1 => _dummyGenreOfUser1 = _dummyGenreOfUser1 ?? new Genre
        {
            Id = 1,
            Name = "Blues"
        };

        static private Genre _dummyGenreOfUser2;
        static public Genre DummyGenreOfUser2 => _dummyGenreOfUser2 = _dummyGenreOfUser2 ?? new Genre
        {
            Id = 2,
            Name = "Jazz"
        };

        static private Gig _dummyGigOfUser1;
        static public Gig DummyGigOfUser1 => _dummyGigOfUser1 = _dummyGigOfUser1 ?? new Gig
        {
            Id = 1,
            Venue = "Apollo Theater",
            Artist = new ApplicationUser {Name = PrincipalOfUser1.Identity.Name},
            ArtistId = PrincipalOfUser1.Identity.GetUserId(),
            DateTime = DateTime.MaxValue.AddYears(-10),
            Genre = DummyGenreOfUser1,
            GenreId = DummyGenreOfUser1.Id
        };

        static private Gig _dummyGigOfUser2;
        static public Gig DummyGigOfUser2 => _dummyGigOfUser2 = _dummyGigOfUser2 ?? new Gig
        {
            Id = 2,
            Venue = "Stellar Stadium",
            ArtistId = PrincipalOfUser2.Identity.GetUserId(),
            Artist = new ApplicationUser {Name = PrincipalOfUser2.Identity.Name},
            DateTime = DateTime.MaxValue.AddYears(-10),
            Genre = DummyGenreOfUser2,
            GenreId = DummyGenreOfUser2.Id
        };

        static public Gig SpawnGig(IPrincipal user)
        {
            return new Gig
            {
                Id = 1,
                Venue = "Apollo Theater",
                ArtistId = user.Identity.GetUserId(),
                Artist = new ApplicationUser(),
                DateTime = DateTime.MaxValue.AddYears(-10),
                Genre = DummyGenreOfUser1,
                GenreId = DummyGenreOfUser1.Id
            };
        }

        static public GigFormViewModel SpawnValidGigFormViewModelFromGig(Gig gig) => new GigFormViewModel
        {
            Id = gig.Id,
            Date = gig.DateTime.ToString(UDates.GigDateFormat, CultureInfo.InvariantCulture),
            Time = gig.DateTime.ToString(UDates.GigTimeOfDayFormat, CultureInfo.InvariantCulture),
            Venue = gig.Venue,
            Genres = new[] {gig.Genre},
            GenreId = gig.GenreId
        };

        static private IGigsRepository _gigsRepo;
        static public IGigsRepository GigsRepo
        {
            get
            {
                if (_gigsRepo != null) return _gigsRepo;

                var gigsRepo = new Mock<IGigsRepository>();
                gigsRepo.Setup(uow => uow.GetMultipleAsync( /*userId:*/ It.IsAny<string>(), /*searchTerm:*/ It.IsAny<string>(), /*onlyFutureOnesNotAll:*/ It.IsAny<bool>(), /*includeCancelled:*/ It.IsAny<bool>()))
                    .Returns(Task.FromResult(new[] {DummyGigOfUser1, DummyGigOfUser2}.ToEnumerable()));

                gigsRepo.Setup(uow => uow.GetSingleAsync( /*gigId:*/ It.IsAny<int>(), /*includeGenre:*/ It.IsAny<bool>(), /*includeAttendances:*/ It.IsAny<bool>(), /*includeAttendancesAttendees:*/ It.IsAny<bool>(), /*includeArtist:*/ It.IsAny<bool>(), /*includeArtistsFollowers:*/ It.IsAny<bool>()))
                    .Returns<int, bool, bool, bool, bool, bool>((gigId, includeGenre, includeAttendances, includeAttendancesAttendees, includeArtist, includeArtistFollowers) =>
                    {
                        if (gigId == 1) return Task.FromResult(DummyGigOfUser1);
                        if (gigId == 2) return Task.FromResult(DummyGigOfUser2);
                        return Task.FromResult<Gig>(null);
                    });
                return _gigsRepo = gigsRepo.Object;
            }
        }


        static public IGenresRepository _genresRepo;
        static public IGenresRepository GenresRepo
        {
            get
            {
                if (_genresRepo != null) return _genresRepo;

                var gigsRepo = new Mock<IGenresRepository>();
                gigsRepo.Setup(uow => uow.GetAsync( /*genreId:*/ It.IsAny<byte>())).Returns<byte>(b =>
                {
                    if (b == DummyGenreOfUser1.Id) return Task.FromResult(DummyGenreOfUser1);
                    if (b == DummyGenreOfUser2.Id) return Task.FromResult(DummyGenreOfUser2);

                    return Task.FromResult<Genre>(null);
                });
                gigsRepo.Setup(uow => uow.GetAllAsync()).Returns(Task.FromResult(new[] { DummyGenreOfUser1, DummyGenreOfUser2 }.AsEnumerable()));
                return _genresRepo = gigsRepo.Object;
            }
        }

        static private IAttendancesRepository _attendancesRepo;
        static public IAttendancesRepository AttendancesRepo
        {
            get
            {
                if (_attendancesRepo != null) return _attendancesRepo;

                var attendancesRepository = new Mock<IAttendancesRepository>();

                attendancesRepository
                    .Setup(uow => uow.GetAllMatchingAsync( /*userId:*/ It.IsAny<string>(), /*searchTerm:*/ It.IsAny<string>(), /*gigIds:*/ It.IsAny<IEnumerable<int>>()))
                    .Returns(Task.FromResult<IEnumerable<Attendance>>(new List<Attendance>())); //0

                return _attendancesRepo = attendancesRepository.Object;
            }
        }

        static private IArtistsFollowingsRepository _artistsFollowingsRepository;
        static public IArtistsFollowingsRepository ArtistsFollowingsRepo
        {
            get
            {
                if (_artistsFollowingsRepository != null) return _artistsFollowingsRepository;

                var artistsFollowingsRepository = new Mock<IArtistsFollowingsRepository>();

                artistsFollowingsRepository
                    .Setup(uow => uow.GetMultipleAsync( /*fanId:*/ It.IsAny<string>(), /*artistsId:*/ It.IsAny<IEnumerable<string>>()))
                    .Returns(Task.FromResult<IEnumerable<FanFollowingArtist>>(new List<FanFollowingArtist>())); //0

                return _artistsFollowingsRepository = artistsFollowingsRepository.Object;
            }
        }
        //0 never use the newtask constructor in asynchronous code  instead use task.fromresult as suggested per http://stackoverflow.com/a/19365805/863651
        //      Task.FromResult<IEnumerable<FanFollowingArtist>>(new List<FanFollowingArtist>())  <-- correct   wont deadlock
        //      new Task<IEnumerable<FanFollowingArtist>>(() => new FanFollowingArtist[] {})      <--   wrong   will deadlock

        static private IUserHasReadNotificationsRepository _userHasReadNotificationsRepo;
        static public IUserHasReadNotificationsRepository UserHasReadNotificationsRepo => _userHasReadNotificationsRepo ?? SpawnUserHasReadNotificationsRepo();

        static public IUserHasReadNotificationsRepository SpawnUserHasReadNotificationsRepo(UserHasReadNotification[] notifications = null)
        {
            var userHasReadNotificationsRepo = new Mock<IUserHasReadNotificationsRepository>();

            userHasReadNotificationsRepo.Setup(uow => uow.GetUnreadNotificationsAsync(
                /*userId*/ It.IsAny<string>(),
                /*ids:*/ It.IsAny<IEnumerable<int>>())
            ).Returns(Task.FromResult(
                (notifications ?? new UserHasReadNotification[] { }).AsEnumerable()
            ));

            return _userHasReadNotificationsRepo = userHasReadNotificationsRepo.Object;
        }
        

        static private IUnitOfWork _unitOfWork;
        static public IUnitOfWork UnitOfWork => _unitOfWork = _unitOfWork ?? SpawnUnitOfWork(GigsRepo, GenresRepo, AttendancesRepo, ArtistsFollowingsRepo, UserHasReadNotificationsRepo);

        static public IUnitOfWork SpawnUnitOfWork(
            IGigsRepository gigsRepo = null,
            IGenresRepository genresRepo = null,
            IAttendancesRepository attendancesRepo = null,
            IArtistsFollowingsRepository artistsFollowingsRepo = null,
            IUserHasReadNotificationsRepository userHasReadNotificationsRepo = null)
        {
            var unitOfWork = new Mock<IUnitOfWork>();
            unitOfWork.Setup(uow => uow.GigsRepo).Returns(gigsRepo ?? GigsRepo);
            unitOfWork.Setup(uow => uow.GenresRepo).Returns(genresRepo ?? GenresRepo);
            unitOfWork.Setup(uow => uow.AttendancesRepo).Returns(attendancesRepo ?? AttendancesRepo);
            unitOfWork.Setup(uow => uow.ArtistsFollowingsRepo).Returns(artistsFollowingsRepo ?? ArtistsFollowingsRepo);
            unitOfWork.Setup(uow => uow.UserHasReadNotificationsRepo).Returns(userHasReadNotificationsRepo ?? UserHasReadNotificationsRepo);
            return _unitOfWork = unitOfWork.Object;
        }

        static public T SetupCtrlerMocks<T>(this T controller, IPrincipal user) where T : Controller
        {
            var routes = new RouteCollection(); //todo   contemplate on whether this is right not in the sense
            //RouteConfig.RegisterRoutes(routes); //todo   that its a step away from unit testing philosophy

            var serverMock = new Mock<HttpServerUtilityBase>();
            var sessionMock = new Mock<HttpSessionStateBase>();
            var requestMock = new Mock<HttpRequestBase>();
            var contextMock = new Mock<HttpContextBase>();
            var responseMock = new Mock<HttpResponseBase>();
            var controllerContextMock = new Mock<ControllerContext>();

            contextMock.SetupGet(x => x.User).Returns(user);
            contextMock.SetupGet(x => x.Server).Returns(serverMock.Object);
            contextMock.SetupGet(x => x.Session).Returns(sessionMock.Object);
            contextMock.SetupGet(x => x.Request).Returns(requestMock.Object);
            contextMock.SetupGet(x => x.Response).Returns(responseMock.Object);

            requestMock.SetupGet(x => x.Url).Returns(new Uri(@"http://localhost/a", UriKind.Absolute));
            requestMock.SetupGet(x => x.ApplicationPath).Returns(@"/");
            requestMock.SetupGet(x => x.ServerVariables).Returns(new NameValueCollection());

            responseMock.Setup(x => x.ApplyAppPathModifier(It.IsAny<string>())).Returns<string>(x => x);

            controllerContextMock.SetupGet(x => x.RouteData).Returns(new RouteData());
            controllerContextMock.SetupGet(x => x.Controller).Returns(controller);
            controllerContextMock.SetupGet(x => x.HttpContext).Returns(contextMock.Object);

            controller.Url = new UrlHelper(new RequestContext(contextMock.Object, new RouteData()), routes);
            controller.ControllerContext = controllerContextMock.Object;

            return controller;
        }

        static public IGigsRepository SetupGigsRepositoryMocks(this IGigsRepository repository, IQueryable<Gig> fakeData)
        {
            var mockAppDbContext = new Mock<IApplicationDbContext>();
            mockAppDbContext.SetupGet(c => c.Gigs).Returns(SpawnMockDbSetForFakeData(fakeData).Object);

            repository.Context = mockAppDbContext.Object;

            return repository;
        }

        static public IAttendancesRepository SetupAttendancesRepositoryMocks(this IAttendancesRepository repository, IQueryable<Attendance> fakeData)
        {
            var mockAppDbContext = new Mock<IApplicationDbContext>();
            mockAppDbContext.SetupGet(c => c.Attendances).Returns(SpawnMockDbSetForFakeData(fakeData).Object);

            repository.Context = mockAppDbContext.Object;

            return repository;
        }

        static public IUserHasReadNotificationsRepository SetupUserHasReadNotificationsRepositoryMocks(this IUserHasReadNotificationsRepository repository, IQueryable<UserHasReadNotification> fakeData)
        {
            var mockAppDbContext = new Mock<IApplicationDbContext>();
            mockAppDbContext.SetupGet(c => c.UserHasReadNotifications).Returns(SpawnMockDbSetForFakeData(fakeData).Object);

            repository.Context = mockAppDbContext.Object;

            return repository;
        }

        static private Mock<DbSet<T>> SpawnMockDbSetForFakeData<T>(IQueryable<T> fakeData) where T : class
        {
            var mockAttendancesDbSet = new Mock<DbSet<T>>();
            mockAttendancesDbSet.As<IDbAsyncEnumerable<T>>().Setup(m => m.GetAsyncEnumerator()).Returns(new TestDbAsyncEnumerator<T>(fakeData.GetEnumerator())); //0
            mockAttendancesDbSet.As<IQueryable<T>>().Setup(m => m.Provider).Returns(new TestDbAsyncQueryProvider<T>(fakeData.Provider));
            mockAttendancesDbSet.As<IQueryable<T>>().Setup(m => m.Expression).Returns(fakeData.Expression);
            mockAttendancesDbSet.As<IQueryable<T>>().Setup(m => m.ElementType).Returns(fakeData.ElementType);
            mockAttendancesDbSet.As<IQueryable<T>>().Setup(m => m.GetEnumerator()).Returns(fakeData.GetEnumerator());

            return mockAttendancesDbSet;
        }
        //0 notice the use of idbasyncenumerable   mocking the dbset in this manner is vital in order to ensure that the invocations of tolistasync in the repositories
        //  work as intended within the context of the unit testing mechanism   had we used a simple casual mock here we would end up with runtime exceptions
    }
}
