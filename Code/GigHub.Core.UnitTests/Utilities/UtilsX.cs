using Moq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web.Mvc;

namespace GigHub.Core.UnitTests.Utilities
{
    static public class UtilsX
    {
        static public IEnumerable<T> Enumerify<T>(this T item)
        {
            yield return item;
        }

        static public void SetSource<T>(this Mock<DbSet<T>> mockSet, IList<T> source) where T : class
        {
            var data = source.AsQueryable();

            mockSet.As<IQueryable<T>>().Setup(m => m.Provider).Returns(data.Provider);
            mockSet.As<IQueryable<T>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<T>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<T>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator);
        }

        static public TController ValidateViewModelX<TViewModel, TController>(this TController controller, TViewModel viewModelToValidate, bool throwExceptionIfInvalid = false)
            where TController : Controller
        {
            var validationResults = new List<ValidationResult>(4);

            Validator.TryValidateObject(viewModelToValidate, new ValidationContext(viewModelToValidate, serviceProvider: null, items: null), validationResults, validateAllProperties: true);
            validationResults.ForEach(x => controller.ModelState.AddModelError(x.MemberNames.FirstOrDefault() ?? "", x.ErrorMessage));

            if (throwExceptionIfInvalid && validationResults.Any()) throw new ModelValidationException(@"Model is invalid");

            return controller;
        }
        //http://blog.johnnyreilly.com/2013/03/unit-testing-modelstate.html
    }
}