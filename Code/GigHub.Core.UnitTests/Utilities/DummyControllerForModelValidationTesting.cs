using Moq;
using System.Web.Mvc;

namespace GigHub.Core.UnitTests.Utilities
{
    public class DummyControllerForModelValidationTesting : Controller
    {
        public DummyControllerForModelValidationTesting()
        {
            ControllerContext = new Mock<ControllerContext>().Object;
        }

        public bool PerformModelValidation(object model) => TryValidateModel(model);
    }
}