﻿using FluentAssertions;
using GigHub.Core.Controllers;
using GigHub.Core.Foundation.Models;
using GigHub.Core.Foundation.ViewModels;
using GigHub.Core.IntegrationTests.Utilities;
using GigHub.Core.Persistence;
using GigHub.Core.Persistence.Contexts;
using GigHub.Core.Services;
using GigHub.Core.Utilities;
using NUnit.Framework;
using System;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

//forwardwarning    When it comes to integration testing the common best practice is to have each and every test reset the
//forwardwarning    db back to vanilla state right after each test completes regardless of whether the test succeeded or not
//forwardwarning    Have a look at the [isolated] attributeclass
namespace GigHub.Core.IntegrationTests.IntegrationTests.Mvc
{
    [TestFixture]
    public class GigsControllerMvcTests
    {
        private UnitOfWork _unitOfWork;
        private GigsController _controller;
        private ApplicationDbContext _context;

        [OneTimeSetUp]
        public void TestFixtureSetUp()
        {
            CultureInfo.DefaultThreadCurrentCulture = CultureInfo.InvariantCulture; //0 vital
            CultureInfo.DefaultThreadCurrentUICulture = CultureInfo.InvariantCulture; //0 vital

            //0 setting the culture to invariantculture is vital to ensure that
            //  somedatetimeobject.tostring() will behave as intended in our tests

            //bare in mind that the Setup class is always getting called first
            //in order for it to ensure that the db has been seeded with users properly
        }

        [SetUp]
        public void SetUp()
        {
            _context = new ApplicationDbContext(); //order
            _unitOfWork = new UnitOfWork(_context); //order
            _controller = new GigsController(new GigsService(_unitOfWork), new ArtistsService(_unitOfWork)); //0
        }
        //0 notice that here we do not mock the unitofwork of appdbcontext simply because we are performing integration testing

        [TearDown]
        public void TearDown()
        {
            _context.Dispose(); //crucial
        }

        [OneTimeTearDown]
        public void TestFixtureTearDown()
        {
        }

        [Test]
        [IsolatedWithAsyncSupport] //0 crucial
        [Category("Integration.GigsControllerMvcTests")]
        public void GigsStagedByMe_WhenCalled_ShouldReturnAllUpcomingGigsStagedByMe()
        {
            // Arrange
            var users = _context.Users.ToList();
            var user1 = users[0];
            var user2 = users[1];
            var result = (ViewResult) null;

            _controller.SetCurrentUserToMockup(user1.Id, user1.Name);

            var gig1 = new Gig {Artist = user1, DateTime = DateTime.Now.AddDays(1), Genre = _context.Genres.First(), Venue = "-"};
            var gig2 = new Gig {Artist = user2, DateTime = DateTime.Now.AddDays(1), Genre = _context.Genres.First(), Venue = "-"};

            _context.Gigs.Add(gig1);
            _context.Gigs.Add(gig2);
            _context.SaveChanges();

            // Act
            var action = new Func<Task>(async () =>
            {
                result = await _controller.GigsStagedByMe() as ViewResult;
            });

            // Assert
            action.Should().NotThrow();
            result.Should().NotBeNull();
            (result?.ViewData.Model as GigsStagedByMeViewModel)?.Gigs.Should().BeEquivalentTo(gig1.Enumerify());
        }
        //0 the isolated attribute makes sure to revert the database to vanilla state after each test is done   we absolutely need this attribute

        [Test]
        [IsolatedWithAsyncSupport] //0
        [Category("Integration.GigsControllerMvcTests")]
        public void Upsert_WhenUpdatingVenueOfAlreadyExistingGig_ShouldReturnRedirectionToRouteAndTheGigMustHaveBeenUpdated() //0
        {
            // Arrange
            var user = _context.Users.First();
            var genres = _context.Genres.ToList();
            var genre1 = genres[0];
            var genre2 = genres[1];
            var result = (RedirectToRouteResult) null;

            _controller.SetCurrentUserToMockup(user.Id, user.Name);

            var now = DateTime.Now;
            now = new DateTime(now.Year, now.Month, now.Day, now.Hour, now.Minute, 0, now.Kind);

            var gig = new Gig
            {
                //Id = 1, //dont
                Venue = "Apollo Theater",
                Genre = genre1,
                Artist = user,
                GenreId = genre1.Id,
                ArtistId = user.Id,
                DateTime = now.AddDays(1)
            };

            _context.Gigs.Add(gig); //order
            _context.SaveChanges(); //order
            _context.Entry(gig).Reload(); //1 crucial   this far better than gig=context.gigs.first()

            var updatedGig = new Gig
            {
                Id = gig.Id, //1 vital
                Venue = $"{gig.Venue}-",
                Genre = genre2,
                Artist = gig.Artist,
                GenreId = genre2.Id,
                ArtistId = user.Id,
                DateTime = gig.DateTime.AddDays(1)
            };

            // Act
            var action = new Func<Task>(async () =>
            {
                result = await _controller.Upsert(new GigFormViewModel
                {
                    Id = updatedGig.Id,
                    Date = updatedGig.DateTime.ToString(UDates.GigDateFormat, CultureInfo.InvariantCulture),
                    Time = updatedGig.DateTime.ToString(UDates.GigTimeOfDayFormat, CultureInfo.InvariantCulture),
                    Venue = updatedGig.Venue,
                    Genres = new Genre[] {}, //indifferent
                    GenreId = updatedGig.GenreId,
                    PossibleError = "" //indifferent
                }) as RedirectToRouteResult;
            });

            // Assert
            action.Should().NotThrow();
            result.Should().NotBeNull(); // ReSharper disable once PossibleNullReferenceException
            result.RouteValues.Should().NotBeNull();
            //result.RouteValues["query"].Should().Be(query); //dont
            result.RouteValues["action"].Should().Be(nameof(GigsController.GigsStagedByMe));
            result.RouteValues["controller"].Should().Be(UMvc.Ctrler<GigsController>());
            _context.Gigs.First().Should().BeEquivalentTo(updatedGig);
        }
        //0 this test performs writes to the database and in order for it not to hang its vital to have the transaction scope inside isolatedwithasyncsupport
        //  set up in such a way that it supports async write operations   http://stackoverflow.com/a/37485875/863651
        //1 the gigid is getting automatically set by the db due to autoincrement being used  this is why we need to first insert the
        //  gig and then regrab it from the db so as to ensure that the gigid will be the appropriate one
        //
        //  one strange thing I noticed with the way entity framework works is that  context.entry(gig).reload()  has some subtle differences
        //  compared to  context.gigs.first()   If we follow the later then although datetime2 column has its precision set to 0 we still get
        //  gigs with datetime having precision which includes milliseconds    this shouldnt be happening but it does with the later approach
        //  probably because the dbcontext caches the entries we write   its kinda weird really    http://stackoverflow.com/a/39476370/863651
    }
}