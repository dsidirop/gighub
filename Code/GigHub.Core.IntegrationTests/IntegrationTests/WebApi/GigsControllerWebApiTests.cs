﻿using System;
using System.Globalization;
using System.Linq;
using System.Net;
using FluentAssertions;
using GigHub.Core.Controllers.Api;
using GigHub.Core.Foundation.Dtos;
using GigHub.Core.Foundation.Models;
using GigHub.Core.IntegrationTests.Utilities;
using GigHub.Core.Persistence;
using GigHub.Core.Persistence.Contexts;
using GigHub.Core.Services;
using MyTested.WebApi;
using NUnit.Framework;

namespace GigHub.Core.IntegrationTests.IntegrationTests.WebApi
{
    [TestFixture]
    public class GigsControllerWebApiTests
    {
        private GigsController _controller;
        private ApplicationDbContext _context;
        static private readonly Tuple<string, GigToToggleCancelStateDto>[] Cases; //must be static

        static GigsControllerWebApiTests() //dont move this into [onetimesetup]
        {
            Cases = new[]
            {
                new Tuple<string, GigToToggleCancelStateDto>("000", new GigToToggleCancelStateDto { GigId = -1, CancelNotUncancel = true }),
                new Tuple<string, GigToToggleCancelStateDto>("001", new GigToToggleCancelStateDto { GigId = 0, CancelNotUncancel = true })
            };
        }

        [OneTimeSetUp]
        public void TestFixtureSetUp()
        {
            CultureInfo.DefaultThreadCurrentCulture = CultureInfo.InvariantCulture; //0 vital
            CultureInfo.DefaultThreadCurrentUICulture = CultureInfo.InvariantCulture; //0 vital

            //0 setting the culture to invariantculture is vital to ensure that
            //  somedatetimeobject.tostring() will behave as intended in our tests

            //bare in mind that the Setup class is always getting called first
            //in order for it to ensure that the db has been seeded with users properly
        }

        [SetUp]
        public void SetUp()
        {
            _context = new ApplicationDbContext();
            _controller = new GigsController(new GigsService(new UnitOfWork(_context))); //0
        }
        //0 notice that here we do not mock the unitofwork of appdbcontext simply because we are performing integration testing

        [TearDown]
        public void TearDown()
        {
            _context.Dispose(); //crucial
        }

        [OneTimeTearDown]
        public void TestFixtureTearDown()
        {
        }

        [Ignore(@"MyWebApi doesn't activate the action-filters regardless of whether they are global or local on action/controller")]
        [Test]
        [IsolatedWithAsyncSupport] //0 crucial
        [Category("Integration.GigsControllerWebApiTests")]
        public void SetCancelStatus_InvalidDtoJson_ShouldReturnBadRequestInvalidJsonInRequestBody400()
        {
            //Arrange
            var firstUser = _context.Users.First();

            //Act
            var action = MyWebApi
                .Controller(() => _controller)
                .WithAuthenticatedUser(user => user.WithUsername(firstUser.Name).AndAlso().WithIdentifier(firstUser.Id)) //onlyway
                .CallingAsync(c => c.SetCancelStatus(/*dto:*/ null));

            //Assert
            action
                .ShouldReturn()
                .BadRequest()
                .WithErrorMessage("Malformed json in request body");

            //0 the isolated attribute makes sure to revert the database to vanilla state after each test is done   we absolutely need this attribute
        }

        [Ignore(@"MyWebApi doesn't activate the action-filters regardless of whether they are global or local on action/controller")]
        [Test]
        [IsolatedWithAsyncSupport]
        [TestCaseSource(nameof(Cases))]
        [Category("Integration.GigsControllerWebApiTests")]
        public void SetCancelStatus_InvalidDtoGigIdOutOfRange_ShouldReturnBadRequestWithProperModelError400(Tuple<string, GigToToggleCancelStateDto> @case)
        {
            var dto = @case.Item2;

            var firstUser = _context.Users.First();
            MyWebApi
                 .Controller(() => _controller)
                 .WithAuthenticatedUser(user => user.WithUsername(firstUser.Name).AndAlso().WithIdentifier(firstUser.Id)) //onlyway
                 .CallingAsync(c => c.SetCancelStatus(dto))
                 .ShouldReturn()
                 .BadRequest()
                 .WithModelStateFor<GigToToggleCancelStateDto>()
                 .ContainingModelStateErrorFor(m => m.GigId);
        }

        [Test]
        [IsolatedWithAsyncSupport]
        [Category("Integration.GigsControllerWebApiTests")]
        public void SetCancelStatus_PassGigIdNonExistant_ShouldReturnNotFound404()
        {
            var firstUser = _context.Users.First();
            MyWebApi
                .Controller(() => _controller)
                .WithAuthenticatedUser(user => user.WithUsername(firstUser.Name).AndAlso().WithIdentifier(firstUser.Id)) //onlyway
                .CallingAsync(c => c.SetCancelStatus(new GigToToggleCancelStateDto {GigId = 1, CancelNotUncancel = true}))
                .ShouldReturn()
                .StatusCode(HttpStatusCode.NotFound);
        }

        [Test]
        [IsolatedWithAsyncSupport]
        [Category("Integration.GigsControllerWebApiTests")]
        public void SetCancelStatus_AttemptToCancelGigOfDifferentArtist_ShouldReturnUnauthorized401()
        {
            var users = _context.Users.ToList();
            var user1 = users[0];
            var user2 = users[1];

            var gig = new Gig {Artist = user2, DateTime = DateTime.Now.AddDays(1), Genre = _context.Genres.First(), Venue = "Apollo Theater"};
            _context.Gigs.Add(gig); //order
            _context.SaveChanges(); //order
            _context.Entry(gig).Reload(); //ordervital

            MyWebApi
                .Controller(() => _controller)
                .WithAuthenticatedUser(user => user.WithUsername(user1.Name).AndAlso().WithIdentifier(user1.Id)) //onlyway
                .CallingAsync(c => c.SetCancelStatus(new GigToToggleCancelStateDto {GigId = gig.Id, CancelNotUncancel = true}))
                .ShouldReturn()
                .StatusCode(HttpStatusCode.Forbidden)
                .AndProvideTheHttpRequestMessage()
                .Content.Should().Be(null);
        }

        [Test]
        [IsolatedWithAsyncSupport] //0
        [Category("Integration.GigsControllerWebApiTests")]
        public void SetCancelStatus_ValidCall_ShouldSucceedWithResponseNoContent201() //0
        {
            var firstUser = _context.Users.First();

            var gig = new Gig { Artist = firstUser, DateTime = DateTime.Now.AddDays(1), Genre = _context.Genres.First(), Venue = "Apollo Theater" };
            _context.Gigs.Add(gig); //order
            _context.SaveChanges(); //order
            _context.Entry(gig).Reload(); //ordervital

            MyWebApi
                .Controller(() => _controller)
                .WithAuthenticatedUser(user => user.WithUsername(firstUser.Name).AndAlso().WithIdentifier(firstUser.Id)) //onlyway
                .CallingAsync(c => c.SetCancelStatus(new GigToToggleCancelStateDto {GigId = gig.Id, CancelNotUncancel = true}))
                .ShouldReturn()
                .StatusCode(HttpStatusCode.NoContent);
        }
        //0 this test performs writes to the database and in order for it not to hang its vital to have the transaction scope inside isolatedwithasyncsupport
        //  set up in such a way that it supports async write operations   http://stackoverflow.com/a/37485875/863651
    }
}