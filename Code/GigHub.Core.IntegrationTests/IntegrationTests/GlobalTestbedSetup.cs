﻿using System;
using System.Data.Entity.Migrations;
using System.Globalization;
using System.IO;
using System.Linq;
using GigHub.Core.Foundation.Models;
using GigHub.Core.Persistence.Contexts;
using GigHub.Core.Persistence.Migrations;
using GigHub.Core.Utilities;
using NUnit.Framework;

//[SetUpFixture] is a feature readily available in nunit which is not available in the 1.x flavor of the ms unit testing suite
//it would be interesting to see if the guys in redmond have included such a feature in the 2.x version
namespace GigHub.Core.IntegrationTests.IntegrationTests
{
    [SetUpFixture]
    public class GlobalTestbedSetup
    {
        [OneTimeSetUp] //0
        public void SetUp()
        {
            CultureInfo.DefaultThreadCurrentCulture = CultureInfo.InvariantCulture; //1 vital
            CultureInfo.DefaultThreadCurrentUICulture = CultureInfo.InvariantCulture; //1 vital

            EnsureDataDirectoryIsSet(); //order
            EnsureDataDirectoryPointsToTestDataFolder(); //order
            DeletePreexistingDbFile(); //order

            EnsureDbSetup(); //order
            EnsureDbIsSeeded(); //order deadlast

            //0 Keep in mind that in older versions of nunit we would use [setup] instead of [setupfixture] here
            //1 setting the culture to invariantculture is vital to ensure that somedatetimeobject.tostring() will behave as intended in our tests
        }

        static private void EnsureDbSetup()
        {
            var applicationDbContextConfiguration = new ApplicationDbContextConfiguration(); //1

            new DbMigrator(applicationDbContextConfiguration).Update(); //2 connectionstring

            //1 The configuration class can be found under gighubcore persistence migrations
            //2 The connection string can be found inside the webconfig file   it has been tweaked to point to a dummy database different than the one in the core project
            //  Notice that the connectionstring we are using points to (localdb)\mssqllocaldb    if this project is run on a different machine which doesnt have sqlserver2016
            //  you might have to tweak the connection string   for instance if only sqlserver2012 is available the connection string needs to be (localdb)\v.11
        }

        static private void EnsureDbIsSeeded()
        {
            var context = new ApplicationDbContext();
            if (context.Users.Any()) return; //0

            context.Users.Add(new ApplicationUser {UserName = "user1", Name = "user1", Email = "-", PasswordHash = "-"});
            context.Users.Add(new ApplicationUser {UserName = "user2", Name = "user2", Email = "-", PasswordHash = "-"});
            context.SaveChanges();

            //0 we need the seed method to run only once  that is very first time the database gets created and is thus devoid of users
        }

        static private void EnsureDataDirectoryIsSet()
        {
            var dataDirectory = AppDomain.CurrentDomain.GetData(DataDirectory) as string;
            if (!string.IsNullOrWhiteSpace(dataDirectory)) //0
                return;

            var binPath =
                UAssemblies
                    .GetExecutingDirectory() //we try this first just in case   when running tests in vs it points to  C:\Windows\Microsoft.NET\Framework\v4.0.30319
                    .GetAllParentDirectories()
                    .FirstOrDefault(x => (Path.GetFileName(x) ?? "").Equals("bin", StringComparison.InvariantCultureIgnoreCase))
                ??
                UAssemblies
                    .DeduceOriginFolderPathForAssemblyOf<GlobalTestbedSetup>()
                    .GetAllParentDirectories()
                    .FirstOrDefault(x => (Path.GetFileName(x) ?? "").Equals("bin", StringComparison.InvariantCultureIgnoreCase));
            if (binPath == null)
                throw new Exception(@"[GTS.EDDIS01] [BUG] Failed to spot the bin directory while tests are being running from within the IDE");

            var directoryContainingMdf = Directory.GetParent(binPath);

            AppDomain.CurrentDomain.SetData(DataDirectory, directoryContainingMdf.FullName);

            //0 when running tests through appvoyer and the like for some reason |datadirectory| is set correctly  when running
            //  tests through visualstudio this datavariable is not set and are thus forced to take matters into our hands
        }

        static private void EnsureDataDirectoryPointsToTestDataFolder()
        {
            var dataDirectory = AppDomain.CurrentDomain.GetData(DataDirectory) as string;
            if (string.IsNullOrWhiteSpace(dataDirectory))
                throw new Exception($@"[GTS.EDDPTTDF01] [BUG] |DataDirectory| is not set (how did this happen?)");

            var dataDirectoryOnTestData = Path.Combine(dataDirectory, @"Test_Data");
            AppDomain.CurrentDomain.SetData(DataDirectory, dataDirectoryOnTestData);
        }

        static private void DeletePreexistingDbFile()
        {
            var dataDirectory = AppDomain.CurrentDomain.GetData(DataDirectory) as string;
            if (string.IsNullOrWhiteSpace(dataDirectory)) //0
                throw new Exception($@"[GTS.DPDF01] [BUG] |DataDirectory| is not set (how did this happen?)");

            File.Delete(Path.Combine(dataDirectory, $@"{DatabaseName}.mdf")); //0
            File.Delete(Path.Combine(dataDirectory, $@"{DatabaseName}_log.ldf")); //0

            //todo              optimization to speed up our continuous integration
            //todo
            //todo  to speed tests up its worth contemplating placing a prefabricated db in here
            //todo  so that we wont have to go through the pains of constantly rebuilding the db
            //todo  using db migrations  in order to achieve this we would need an artifacts server
            //todo  to fetch the db from   even if its dropbox or something

            //0 if the file doesnt exist no exception is thrown so we are fine on appvoayer
        }

        private const string DatabaseName = @"GigHubTestbed";
        private const string DataDirectory = @"DataDirectory";
    }
}

