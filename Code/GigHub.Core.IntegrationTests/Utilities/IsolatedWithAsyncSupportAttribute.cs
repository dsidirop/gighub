using System;
using System.Transactions;
using NUnit.Framework;
using NUnit.Framework.Interfaces;

namespace GigHub.Core.IntegrationTests.Utilities
{
    sealed public class IsolatedWithAsyncSupportAttribute : Attribute, ITestAction
    {
        private TransactionScope _transactionScope;

        public ActionTargets Targets => ActionTargets.Test; // = can be applied only to individual tests    not on entire test suites

        public void BeforeTest(ITest test)
        {
            _transactionScope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled); //0 crucial

            //0 using transactionscopeasyncflowoption.enabled is a must have in order to have integration tests work as intended when it comes to
            //  actually writing stuff to the database   readonly tests work even without this tweak though    http://stackoverflow.com/a/37485875/863651
        }

        public void AfterTest(ITest test)
        {
            _transactionScope.Dispose();
        }
    }
}
