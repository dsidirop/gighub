using Moq;
using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;

namespace GigHub.Core.IntegrationTests.Utilities
{
    static public class UtilsX
    {
        static public IEnumerable<T> Enumerify<T>(this T item)
        {
            yield return item;
        }

        static public void SetCurrentUserToMockup(this Controller controller, string userId, string username)
        {
            var useridentity = new GenericIdentity(username);

            useridentity.AddClaim(new Claim(@"http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name", username));
            useridentity.AddClaim(new Claim(@"http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier", userId));

            var principal = new GenericPrincipal(useridentity, null);

            controller.ControllerContext = Mock.Of<ControllerContext>(
                ctx => ctx.HttpContext == Mock.Of<HttpContextBase>(
                           http => http.User == principal
                       )
            );
        }
        //0 One would expect that casting mockhttpcontext to httpcontextbase would be as simple as   mockhttpcontext.object as httpcontextbase  Right? Wrong
        //  We have to resort to using httpcontextwrapper to achieve this effect
        //1 roughly equivalent to
        //
        //  var mockHttpContext = new Mock<HttpContext>();
        //  mockHttpContext.SetupGet(c => c.User).Returns(principal);
        //  var mockControllerContext = new Mock<ControllerContext>();
        //  mockControllerContext.SetupGet(c => c.HttpContext).Returns(new HttpContextWrapper(mockHttpContext.Object)); //0 httpcontextwrapper
        //
        //  controller.ControllerContext = mockHttpContext;
    }
}