NUGET SURVIVAL GUIDE:

    Never use the the graphical tool for nuget to restore/install/update packages.

    Any and all package installations/restorations/updates must be performed manually via command line and by editing packages.config for each and every project separately. Nuget crash-course:

    http://blog.davidebbo.com/2011/01/installing-nuget-packages-directly-from.html

    Use the graphical tool for nuget to only INSPECT the versions of the dependencies and check out possible updates.
    After installing a package via command line be sure to double-check that the project affected is referencing the dlls from the LOCAL PACKAGE folder residing INSIDE the project's folder.
    Also double-check that you've placed the new package in the packages.config of the project.

    For newly created projects also double-check in the pre-build events of the .csproj file that this section exists:

    <Target Name="BeforeBuild"> <Exec Command="..nuget\nuget.exe restore .\packages.config -PackagesDirectory ..\packages" /> </Target>

    Last but not least, when UPGRADING a package in one project always make sure to perform the same upgrade to any and all other sibling projects (Quick tip: You can scan the solution for other projects that use the same nuget package by performing a solution-wide text search targeting packages.config files across the board).

Omitting this step can lead to very weird runtime behaviour in the sense that the output directory for the solution might end up having the old version of the package-dll depending on which order the projects of the solution got built.